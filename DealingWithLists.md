# Lists

## Requirements
+ I have the case where I want to save a list in a db.
+ I want the diffability that maps give me (with hashes and stuff)
+ I want a single adressing space
- Do I want sparse lists?

I need functions like

prepend pos [values]
append pos [values] 

## Implementation

There is a NumericalIndex typeclass. This allows a key in the database to
be a number. Trees in the db can be indexed by keys that implement this interface
and in this case lists are possible. There are specific functions supported
in the db to manipulate such lists (ex: append some values at the end of the
list, automatically generating the keys)
