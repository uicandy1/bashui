# ![uicandy](doc/uicandy.png) uicandy: user interface for shell applications

The goal of this project is to create a GUI application from a set
of command line programs in a declarative way.

- the ui itself is designed with a visual editor (e.g. glade for gtk)
- a view model determines the runtime content of the ui 
- declarative bindings tell the ui how to use the view model
- application code consists of one or more short lived command line programs that
implement the business logic and one more more command line programs that
update the view model. 
- a normal shell script ties everything together (functions and ui)

## Documentation 

- [Samples](/doc/samples.md)
- [Command Refrence](doc/detail.md).

## Walkthrough: Hangman 

![hangman](doc/hm.gif)

> completesource code [samples/hangman/hangman.sh](samples/hangman/hangman.sh)

Let's say we want to create a small application for solving hangman and
crossword problems: we know zero or more letters in a word and the number
of letters in the word and we want to find all matching words:

```
_ A N G _ _ N
```

As long as we have grep and a 
dictionary (two things that are very common in a linux distrubution),
we can solve the problem on the command line by doing:

```bash
grep -ie "^.ANG..N$" /usr/share/dict/words
```

which gives us the following list of results:

```
Rangoon
hangman
hangmen
```

It's great, we already have a fully working command line solution (that
is even better than a GUI based solution for command line users). It would
be nice to be able to stick a GUI on top of that without much effort
so that more casual users can benefit from the solution
(we don't want to rewrite grep for that but preferably to use it).

### Create the GUI in glade

Creating a GUI (placing widgets, determining what user interface mechanism
should be used) is a visual thing, and is better done with visual tools
by people who understand visual things (designers). Fortunately we can use
glade with the gtk toolkit as a visual tool to create a gui.

For this hangman application we need a main window, an entry widget (to enter the
word to be found) and a treeview to display the list of solutions:

![Glade](doc/Glade.png)

[ source ](samples/hangman/hangman.glade)

### Design the view model

The design of the GUI is not enough: the application needs to be able to interact
with it. This interaction happens in two steps:

a) the application updates a view model (that fully describes the current state
of the ui)

b) the ui itself is regenerated from the view model (a list of bindings connect
data items from the view model to widgets in the gui, something like use 
the `potato` data item as the label of the `ID_POTATO` button).

Using json notation for clarity, the view model that we use in this sample consists of 
a list of items that contain a "word" field. These words are the solutions that
need to be displayed in the treeview widget:

```json
{
    "solutions": [ 
        { "word": "hangman" },
        { "word": "hangmen" },
        { "word": "rangoon" }
    ]
}
```

This view model is the only UI thing that our application code understands.
So that updating the ui only consists in running queries that change this model
(and has nothing to do with knowing gtk related details).

### Connect the view model to the glade UI with declarative bindings

We need to tell the UI two things:
a) what to do when the text in the entry widget changes
b) what to do with the "solutions" in the data model

Sadly this information cannot be entered directly in glade, so we need to
extend our glade file with some "bindings". 

Whenever the entry (text input) changes in the ui, we want to execute
a "changed" program (what this changed program does will be explained later):

```haskell
            EntryChanged "ID_ENTRY" (Literal (StringValue "changed")),
```

We want the tree view to be populated by the `word` field of the
`solutions` data member of the view model (this creates a "Word" column
in the treeview that is populated with the `word` field of the `solutions` list
items):

```haskell
            TreeViewListItems "ID_TREE" (Reference ["solutions"]) 
                [
                    Column "Word" (Reference ["word"]) Text
                ]
```

### Implement the "changed" program:

The EntryChanged binding will invoke the `changed` and provide
the content of the entry field as stdin. The `changed` program
needs to call grep to get the results and then update the solutions
list in the data model. The full code is here:

```bash
changed () {
    local content=$(cat) 
    local pattern="^$(echo "$content" | sed "s/[ -.?*_]/./g")\$"
    uidb "searching $content"<<EOF
lines solutions word ____
$(grep -ie "$pattern" "/usr/share/dict/words" || echo -n '""')
____
EOF
}
export -f "changed"
```

Breaking it down, we first read stdin to a string:
```bash
    local content=$(cat) 
```

This receives the full content of the entry widget. We need to convert this string
to a regular expression for grep (ex: `h-----n` to `^h.....n$`). This line
does this:
```bash
    local pattern="^$(echo "$content" | sed "s/[ -.?*_]/./g")\$"
```

From that point we need to run grep and feed the results of grep back to
the application. To do this we pipe a ui query to `uidb`. As mentioned
earlier, our ui process works as a database: if we want to change the
view model, we need to execute a query. `lines solutions word ___` 
tells the ui server to update the solutions list with the following line
that contain the word field. Then we populate the lines with the result
of running grep on the query. Then we add the list terminator `___` (note
that the `<<<EOF` thing is a common thing in shell scripts called here
documents):

```bash

    uidb "searching $content"<<EOF
lines solutions word ____
$(grep -ie "$pattern" "/usr/share/dict/words" || echo -n '""')
____
EOF

```

Finally we export our changed script to subprocesses:

```bash
export -f "changed"
```

### Launching the ui server

The last step in our shell script consists in launching the ui server:

```bash
runui
```

This launches the ui server (called ui). This
program listens on a unix domain socket waiting for connections
that will run queries on the internal viewmodel. 

### REPL and debugging

While the hangman application is running, it is possible to connect to the 
viewmodel database using a command line tool, to inspect or modify values. The database
is immutable (each new version of the database is kept so it is possible to
inspect the history of changes). Transactions are atomic (they may consist
of multiple changes that need to either succeed or fail as a whole).

![REPL](doc/Repl.png)
