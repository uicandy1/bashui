{-|
Module      : AppTypes
Description : Types used in the ui server
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Type definitions for the ui server
-}
module AppTypes
    ( 
        AppSettings(..),
        Binding(..),
        BuildOptions(..),
        Column(..),
        DataReference(..),
        DbKey,
        DbPath(..),
        DbQuery(..),
        DbResult(..),
        DbValue(..),
        Format(..),
        GladePath,
        MenuItemLabelCmd(..),
        Settings(..),
        UiDb(..),
        WidgetId
    ) where

import Server.Types
import Data.IORef(IORef(..))
import Graphics.UI.Gtk(EventM(..), EButton)
import System.Log.Logger(Priority)

-- | Id of a gtk widget
type WidgetId = String

-- | Path of a glade file (FIXME)
type GladePath = String

-- | References to variables in the model (used by bindings)
data DataReference = 
    Literal DbValue | 
    Reference DbPath
        deriving (Read, Show)

-- | How to display a column
data Format = Text | Icon 
    deriving (Read, Show)

-- | TreeViews being quite sophisticated, this is helpful
data Column = Column String DataReference Format
    deriving (Read, Show)

-- | Build options
data BuildOptions = BuildOptions {
    -- we want to block circular event firing
    -- allowEvents :: IORef Bool,
    -- we need a way to notify the startup of the applicaton
    fireStartup :: IORef (IO ()),
    -- we need a way to update the db when "editors" (ex: text editor)
    -- change their content (so that the db stays in sync with it)
    dbIORef :: DBIORef,
    -- run (no allowEvents)
    runWithStdinN :: String -> String -> IO (),
    -- run (no allowEvents)
    runN :: String -> IO (),
    -- run with stdin (allow events)
    runWithStdin :: String -> String -> IO (),
    -- run (with allowEvents)
    run :: String -> IO (),
    -- run (with allowEvents) -- we could get rid of this using stdin
    runMouseEvent :: String -> EventM EButton Bool
}

-- | Text action
data MenuItemLabelCmd = MenuItemLabelCmd DataReference DataReference 
    deriving (Read, Show)

-- | These connect a widget (WidgetId) to a variable in the model
data Binding = 
    AppWindow WidgetId |
    ApplicationReady DataReference |
    ButtonActivated WidgetId DataReference |
    ButtonText WidgetId DataReference |
    ComboBoxModel WidgetId DataReference [Column] |
    ComboBoxSelectionChanged WidgetId DataReference DataReference DataReference |
    ComboBoxSelectionCleared WidgetId DataReference |
    ComboBoxSelection WidgetId DataReference |
    ContainerItems WidgetId DataReference GladePath WidgetId [Binding] | 
    ContextMenu WidgetId DataReference MenuItemLabelCmd | 
    DialogOnChange WidgetId DataReference |
    EntryActivated WidgetId DataReference |
    EntryChanged WidgetId DataReference |
    EntryText WidgetId DataReference |
    Group GladePath [Binding] |
    HideOnDelete WidgetId |
    IconViewListItems WidgetId DataReference [Column] |
    ImageBase64 WidgetId DataReference |
    ImageSvg WidgetId DataReference |
    LabelText WidgetId DataReference | 
    ListBoxSelect WidgetId DataReference DataReference |
    MenuItemActivated WidgetId DataReference |
    MenuItemLabel WidgetId DataReference |
    MouseButtonPress WidgetId DataReference |
    MouseButtonRelease WidgetId DataReference |
    MouseCapture WidgetId DataReference |
    RangeMinMax WidgetId DataReference DataReference |
    RangeValue WidgetId DataReference |
    RangeValueChange WidgetId DataReference |
    RowActivated WidgetId DataReference DataReference |
    SpinButtonIncrements WidgetId DataReference DataReference |
    SpinButtonMinMax WidgetId DataReference DataReference |
    SpinButtonSpinned WidgetId DataReference |
    SwitchActive WidgetId DataReference |
    SwitchActivate WidgetId DataReference |
    TextViewText WidgetId DataReference |
    ToggleButtonActive WidgetId DataReference |
    ToggleButtonToggled WidgetId DataReference |
    ToolButtonActivated WidgetId DataReference |
    TreeViewListItems WidgetId DataReference [Column] |
    TreeViewSelectionChanged WidgetId DataReference DataReference |
    TreeViewSelectionCleared WidgetId DataReference |
    TreeViewSingleSelection WidgetId DataReference |
    WidgetHide WidgetId DataReference |
    WidgetMap WidgetId DataReference |
    WidgetRealize WidgetId DataReference |
    WidgetSensitive WidgetId DataReference |
    WidgetShow WidgetId DataReference |
    WidgetUnRealize WidgetId DataReference |
    WidgetUnmap WidgetId DataReference 
        deriving (Read, Show)

-- | The settings for the ui application
-- (file based settings)
data Settings = Settings {
        root :: Binding,
        db :: UiDb,
        socketPath :: String
    }
    deriving (Read, Show)

-- | App settings
-- (command line args)
data AppSettings = AppSettings
  { 
    logLevel :: Priority,
    settingsPath :: String,
    forkEvents :: Bool
  }
  deriving Show

