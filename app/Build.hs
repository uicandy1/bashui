{-|
Module      : Build
Description : Binding building mechanism
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Instanciate the various bindings
-}
module Build
    (
    build
    ) where

import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString.Char8 (pack)
import Data.IORef(writeIORef)
import Data.List as L
import Data.Map.Strict as M
import Data.Maybe (isJust, isNothing, fromJust, fromMaybe)
import GHC.Float(float2Double)
import Graphics.UI.Gtk
import LoadImages
import MDb 
import MouseCapture(runMouseCapture)
import Server.DbIO(syncValueToDb)
import System.Glib.UTFString (stringToGlib)
import System.Log.Logger
import AppTypes
import qualified Bindings as B

-- | Adds a value to synchronize before the next transaction
syncValue :: BuildOptions -> [DbKey] -> DbValue -> IO ()
syncValue bopt key value = do
    syncValueToDb (dbIORef bopt) key value $ "internal update " ++ (show key) ++ " -> " ++ (show value)

-- | Automatically synchronizes a DataReference to a db value (synchronization are
-- done in a transaction just before the next external transaction)
autoSync :: DataReference -> BuildOptions  -> DbValue -> IO ()
-- nothing to do for a literal: it cannot be updated
autoSync (Literal _) _ _ = return ()
autoSync (Reference dbPath) bopts value = 
    syncValue bopts dbPath value 

-- | Sacrilege, type casting. Some bindings expect a specific data type
-- the data will be casted.
asBool :: DbValue -> Bool
asBool (StringValue s) = s == "True" || s == "true"
asBool (IntValue i) = i /= 0
asBool (FloatValue f) = f /= 0.0 

-- | Cast as int
asInt :: DbValue -> Int
asInt (StringValue s) = 0
asInt (IntValue i) = i
asInt (FloatValue f) = 0

-- | cast as float
asFloat :: DbValue -> Float
asFloat (StringValue s) = 0.0
asFloat (IntValue i) = 0.0
asFloat (FloatValue v) = v

-- | cast as float
asDouble :: DbValue -> Double
asDouble (StringValue s) = 0.0
asDouble (IntValue i) = 0.0
asDouble (FloatValue v) = float2Double v

-- | Cast as string
asString :: DbValue -> String
asString (StringValue s) = s
asString (IntValue i) = "int"
asString (FloatValue v) = "float"

-- | Cast as a list of ui dbs
asListOfUiDb :: DbValue -> [UiDb]
asListOfUiDb v =
    [ dbLeaf v ]

-- | Lookup at a key and cast to Bool
lookupAlwaysBool :: DataReference -> UiDb -> Bool
lookupAlwaysBool (Reference k) db =
    maybe False asBool (deepLookupValue k db)
lookupAlwaysBool (Literal v) _ =
    asBool v

-- | Lookup at a key and cast to Int
lookupAlwaysInt :: DataReference -> UiDb -> Int
lookupAlwaysInt (Reference k) db =
    maybe 0 asInt (deepLookupValue k db)
lookupAlwaysInt (Literal v) _ =
    asInt v

-- | Lookup at a key and cast to Float
lookupAlwaysFloat :: DataReference -> UiDb -> Float
lookupAlwaysFloat (Reference k) db =
    maybe 0.0 asFloat (deepLookupValue k db)
lookupAlwaysFloat (Literal v) _ =
    asFloat v

-- | Lookup at a key and cast to Double
lookupAlwaysDouble :: DataReference -> UiDb -> Double
lookupAlwaysDouble (Reference k) db =
    maybe 0.0 asDouble (deepLookupValue k db)
lookupAlwaysDouble (Literal v) _ =
    asDouble v

-- | Loogup as a range (two floats)
lookupAlwaysRange :: DataReference -> DataReference -> UiDb -> (Double, Double)
lookupAlwaysRange r1 r2 db = (lookupAlwaysDouble r1 db, lookupAlwaysDouble r2 db)

-- | Lookup at a key and cast to String
lookupAlwaysString :: DataReference -> UiDb -> String
lookupAlwaysString (Reference k) db =
    maybe "" asString (deepLookupValue k db)
lookupAlwaysString (Literal v) _ =
    asString v
    
-- | Lookup at a key and cast to Pixbuf
lookupAlwaysIcon :: DataReference -> UiDb -> IO Pixbuf
lookupAlwaysIcon ref db = do
    theme <- iconThemeGetDefault
    maybeIcon <- iconThemeLoadIcon theme (lookupAlwaysString ref db) 64 IconLookupGenericFallback 
    maybe (pixbufNew ColorspaceRgb False 8 1 1) return maybeIcon

-- | Lookup at a key and cast to list of dbs
lookupAlwaysListOfUiDb :: DataReference -> UiDb -> [UiDb]
lookupAlwaysListOfUiDb (Reference k) db =
    maybe [] dbAsDbList (deepLookup k db)
lookupAlwaysListOfUiDb (Literal v) _ =
    asListOfUiDb v

-- | Lookup at a key and cast to db
lookupAlwaysUiDb :: DataReference -> UiDb -> UiDb
lookupAlwaysUiDb (Reference k) db =
    fromMaybe MDb.empty (deepLookup k db)
lookupAlwaysUiDb (Literal v) _ =
    dbLeaf v 

-- | Lookup at a key and cast to list of (k, db)
lookupAlwaysListOfKeyUiDb :: DataReference -> UiDb -> [(DbKey, UiDb)]
lookupAlwaysListOfKeyUiDb (Reference k) db =
    fromMaybe [] $ deepLookup k db >>= dbAsKeyDbList
lookupAlwaysListOfKeyUiDb (Literal v) _ = []
 
-- | Checks if a DataReference is a literal
isLiteral :: DataReference -> Bool
isLiteral (Literal _ ) = True
isLiteral _ = False

-- | Check if a binding is a static binding
staticBinding :: Binding -> Bool
staticBinding (AppWindow _) = True
staticBinding (ApplicationReady (Literal _)) = True
staticBinding (ButtonActivated _ (Literal _)) = True
staticBinding (ButtonText _ (Literal _)) = True
staticBinding (ComboBoxModel _ (Literal _) _) = True
staticBinding (ComboBoxSelectionChanged _ (Literal _) (Literal _) (Literal _)) = True
staticBinding (ComboBoxSelectionCleared _ (Literal _)) = True 
staticBinding (ComboBoxSelection _ (Literal _)) = True
staticBinding (ContextMenu _ (Literal _) (MenuItemLabelCmd (Literal _) (Literal _)) ) = True
staticBinding (DialogOnChange _ (Literal _)) = True
staticBinding (EntryActivated _ (Literal _)) = True
staticBinding (EntryChanged _ (Literal _)) = True
staticBinding (EntryText _ (Literal _)) = True
staticBinding (HideOnDelete _) = True
staticBinding (ImageBase64 _ (Literal _)) = True
staticBinding (ImageSvg _ (Literal _)) = True
staticBinding (LabelText _ (Literal _) ) = True
staticBinding (ListBoxSelect _ (Literal _) (Literal _)) = True
staticBinding (MenuItemActivated _ (Literal _)) = True
staticBinding (MenuItemLabel _ (Literal _)) = True
staticBinding (MouseButtonPress _ (Literal _)) = True
staticBinding (MouseButtonRelease _ (Literal _)) = True
staticBinding (MouseCapture _ (Literal _)) = True
staticBinding (RangeMinMax _ (Literal _) (Literal _)) = True
staticBinding (RangeValue _ (Literal _)) = True
staticBinding (RangeValueChange _ (Literal _)) = True
staticBinding (RowActivated _ (Literal _) (Literal _)) = True
staticBinding (SpinButtonIncrements _ (Literal _) (Literal _)) = True
staticBinding (SpinButtonMinMax _ (Literal _) (Literal _)) = True
staticBinding (SpinButtonSpinned _ (Literal _)) = True
staticBinding (SwitchActive _ (Literal _)) = True
staticBinding (SwitchActivate _ (Literal _)) = True
staticBinding (TextViewText _ (Literal _)) = True
staticBinding (ToggleButtonActive _ (Literal _)) = True
staticBinding (ToggleButtonToggled _ (Literal _)) = True
staticBinding (ToolButtonActivated _ (Literal _)) = True
staticBinding (TreeViewSelectionChanged _ (Literal _) (Literal _)) = True
staticBinding (TreeViewSelectionCleared _ (Literal _)) = True
staticBinding (TreeViewSingleSelection _ (Literal _)) = True
staticBinding (WidgetHide _ (Literal _)) = True
staticBinding (WidgetMap _ (Literal _)) = True
staticBinding (WidgetRealize _ (Literal _)) = True
staticBinding (WidgetSensitive _ (Literal _)) = True
staticBinding (WidgetShow _ (Literal _)) = True
staticBinding (WidgetUnRealize _ (Literal _)) = True
staticBinding (WidgetUnmap _ (Literal _)) = True
staticBinding _ = False

-- | Find the path of a DataReferencce
dataReferencePath :: DataReference -> Maybe DbPath
dataReferencePath (Literal _) = Nothing
dataReferencePath (Reference dbp) = Just dbp

-- | Path of a Binding
bindingKey:: Binding -> Maybe DbPath
bindingKey (ButtonActivated _ dr) = dataReferencePath dr
bindingKey (ButtonText _ dr) = dataReferencePath dr
bindingKey (ComboBoxModel _ dr _) = dataReferencePath dr
-- bindingKey (ComboBoxSelectionChanged _ dr _ _) = dataReferencePath dr
bindingKey (ComboBoxSelectionCleared _ dr) = dataReferencePath dr 
bindingKey (ComboBoxSelection _ dr) = dataReferencePath dr
bindingKey (ContainerItems _ dr _ _ _) = dataReferencePath dr
bindingKey (ContextMenu _ dr _) = dataReferencePath dr
bindingKey (DialogOnChange _ dr) = dataReferencePath dr
bindingKey (EntryActivated _ dr) = dataReferencePath dr
bindingKey (EntryChanged _ dr) = dataReferencePath dr
bindingKey (EntryText _ dr) = dataReferencePath dr
bindingKey (IconViewListItems _ dr _) = dataReferencePath dr
bindingKey (ImageBase64 _ dr) = dataReferencePath dr
bindingKey (ImageSvg _ dr) = dataReferencePath dr
bindingKey (LabelText _ dr) = dataReferencePath dr
--bindingKey (ListBoxSelect _ dr dr) = dataReferencePath dr
bindingKey (MenuItemActivated _ dr) = dataReferencePath dr
bindingKey (MenuItemLabel _ dr) = dataReferencePath dr
bindingKey (MouseButtonPress _ dr) = dataReferencePath dr
bindingKey (MouseButtonRelease _ dr) = dataReferencePath dr
bindingKey (MouseCapture _ dr) = dataReferencePath dr
--bindingKey (RangeMinMax _ dr dr) = dataReferencePath dr
bindingKey (RangeValue _ dr) = dataReferencePath dr
bindingKey (RangeValueChange _ dr) = dataReferencePath dr
--bindingKey (RowActivated _ dr dr) = dataReferencePath dr
--bindingKey (SpinButtonIncrements _ dr dr) = dataReferencePath dr
--bindingKey (SpinButtonMinMax _ dr dr) = dataReferencePath dr
bindingKey (SpinButtonSpinned _ dr) = dataReferencePath dr
bindingKey (SwitchActive _ dr) = dataReferencePath dr 
bindingKey (SwitchActivate _ dr) = dataReferencePath dr
bindingKey (TextViewText _ dr) = dataReferencePath dr
bindingKey (ToggleButtonActive _ dr) = dataReferencePath dr
bindingKey (ToggleButtonToggled _ dr) = dataReferencePath dr
bindingKey (ToolButtonActivated _ dr) = dataReferencePath dr
-- FIXME: this one should be on... breaks the filemanager... to be investigated
--bindingKey (TreeViewListItems _ dr _) = dataReferencePath dr
--bindingKey (TreeViewSelectionChanged _ dr dr) = dataReferencePath dr
bindingKey (TreeViewSelectionCleared _ dr) = dataReferencePath dr
bindingKey (TreeViewSingleSelection _ dr) = dataReferencePath dr
bindingKey (WidgetHide _ dr) = dataReferencePath dr
bindingKey (WidgetMap _ dr) = dataReferencePath dr
bindingKey (WidgetRealize _ dr) = dataReferencePath dr
bindingKey (WidgetSensitive _ dr) = dataReferencePath dr
bindingKey (WidgetShow _ dr) = dataReferencePath dr
bindingKey (WidgetUnRealize _ dr) = dataReferencePath dr
bindingKey (WidgetUnmap _ dr) = dataReferencePath dr
bindingKey _ = Nothing

-- | Paths that have differences between two dbs
diffDb :: UiDb -> UiDb -> [DbPath]
diffDb db1 db2 = fst <$> deepDiff db1 db2

-- | Special build for groups
buildGroup :: BuildOptions -> Builder -> [Binding] -> IO (B.Binding UiDb)
buildGroup bopts builder bindings = do
    let (static, dynamic) = L.partition staticBinding bindings
    builtStatic <- mapM (build bopts builder) static
    let staticGroup = B.Binding $ B.groupedBinding builtStatic
    let onceStaticGroup = B.Binding $ B.onceBinding (Just staticGroup)
    -- create a mapped binding for the others
    let (keyable, notkeyable) = L.partition (isJust . bindingKey) dynamic
    builtKeyable <- mapM (build bopts builder) keyable
    builtNotkeyable <- mapM (build bopts builder) notkeyable
    let keyed = zip ((fromJust . bindingKey) <$> keyable) builtKeyable
    let mappedKeyed = B.Binding $ B.mappedBinding (M.fromList keyed) diffDb MDb.empty 
    let all = B.groupedBinding (onceStaticGroup : mappedKeyed : builtNotkeyable)
    return $ B.Binding all

-- Build runtime bindings from binding definitions 
-- (convert the stuff that we read to the stuff that we use)
build :: BuildOptions -> Builder -> Binding -> IO (B.Binding UiDb)
build bopts builder (AppWindow wid) = do
    window <- builderGetObject builder castToWindow wid
    -- window <- windowNew
    window `on` deleteEvent $ do -- handler to run on window destruction
        liftIO mainQuit
        return False
    widgetShowAll window
    return $ B.Binding n
    where
        n :: UiDb -> IO (B.Binding UiDb)
        n = B.nullBinding

build bopts builder (ApplicationReady dr) = 
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.ioBinding readyFunc)
    where
        readyFunc :: String -> IO ()
        readyFunc cmd =
            writeIORef (fireStartup bopts) $ (run bopts) cmd

build bopts builder (ButtonActivated wid dr) = do
    button <- builderGetObject builder castToButton wid
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing button buttonActivated (run bopts))

build bopts builder (ButtonText wid dr) = do
    button <- builderGetObject builder castToButton wid
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.buttonStringBinding "" button)

build bopts builder (ComboBoxModel wid dr columns) = do 
    cbox <- builderGetObject builder castToComboBox wid
    ls <- listStoreNew []
    mapM_ (buildColumn cbox ls) columns
    comboBoxSetModel cbox $ Just ls
    return $ B.adaptBinding (lookupAlwaysUiDb dr) $ B.Binding $ B.listStoreBinding ls MDb.empty 
    where
        buildColumn :: ComboBox -> ListStore UiDb -> Column -> IO () 
        buildColumn cbox ls (Column _ dr format) = do
            case format of
                Text -> do
                    cell <- cellRendererTextNew
                    cellLayoutPackStart cbox cell True
                    cellLayoutSetAttributes cbox cell ls $ \row -> [ cellText := lookupAlwaysString dr row ]
                AppTypes.Icon -> do
                    cell <- cellRendererPixbufNew
                    cellLayoutPackStart cbox cell True
                    cellLayoutSetAttributes cbox cell ls $ \row -> [ cellPixbufIconName := lookupAlwaysString dr row ]
            return () 

build bopts builder (ComboBoxSelectionChanged wid dr drCommand drItem) = do
    cbox <- builderGetObject builder castToComboBox wid
    return $ B.adaptBinding (lookupAlwaysListOfKeyUiDb dr) $ 
        B.Binding (B.objectOnBinding Nothing cbox changed getHandler)
    where
        getHandler :: [(DbKey, UiDb)] -> IO ()
        getHandler kdbs = do
            cbox <- builderGetObject builder castToComboBox wid
            index <- comboBoxGetActive cbox
            when (index >= 0) $ do 
                let (_, uidb) = kdbs !! index
                let cmd = lookupAlwaysString drCommand uidb
                let payload = lookupAlwaysString drItem uidb
                (runWithStdin bopts) cmd payload

build bopts builder (ComboBoxSelectionCleared wid drCommand) = do
    cbox <- builderGetObject builder castToComboBox wid
    return $ B.adaptBinding (lookupAlwaysString drCommand) $ 
        B.Binding (B.objectOnBinding Nothing cbox changed getHandler)
    where
        getHandler :: String -> IO ()
        getHandler cmd = do
            cbox <- builderGetObject builder castToComboBox wid
            index <- comboBoxGetActive cbox
            when (index < 0) $ do 
                (run bopts) cmd 

build bopts builder (ComboBoxSelection wid dr) = do
    cbox <- builderGetObject builder castToComboBox wid
    return $ B.adaptBinding (lookupAlwaysInt dr) $ 
        B.Binding (B.comboBoxActiveBinding cbox) 

build bopts builder (ContainerItems wid dr gladepath swid bindings) = do
    container <- builderGetObject builder castToContainer wid
    let cntList = B.containerListBinding [] createBoundWidget container 
    return $ B.adaptBinding (lookupAlwaysListOfUiDb dr) $ B.Binding cntList
    where 
        createBoundWidget :: UiDb -> IO (Widget, B.Binding UiDb) 
        createBoundWidget db = do
            -- the builder used by the group
            itemBuilder <- builderNew 
            builderAddFromFile itemBuilder gladepath
            built <- buildGroup bopts itemBuilder bindings 
            --B.Binding b <- buildGroup itemBuilder bindings 
            --built <- b db
            widget <- builderGetObject itemBuilder castToWidget swid
            return (widget, built)

build bopts builder (ContextMenu wid dr mi) = do
    widget <- builderGetObject builder castToWidget wid
    return $ B.adaptBinding (lookupAlwaysListOfUiDb dr) $ 
        B.Binding (B.objectOnBinding Nothing widget buttonPressEvent (runCMEvent mi))
        
    where
        -- | Run context menu event
        runCMEvent :: MenuItemLabelCmd -> [UiDb] -> EventM EButton Bool
        runCMEvent mi dbs = do
            but <- eventButton
            ts <- eventTime
            case but of
                RightButton -> liftIO $ do
                    m <- menuNew
                    let ms = toMenuShell m
                    mapM (createAppendItem ms mi) dbs
                    menuPopup m $ Just (but, ts)
                    return True
                _ -> return False

        createAppendItem :: MenuShell -> MenuItemLabelCmd -> UiDb -> IO ()
        createAppendItem ms (MenuItemLabelCmd drLabel drCommand) db = do
            mi <- menuItemNew
            menuItemSetLabel mi (lookupAlwaysString drLabel db) 
            mi `on` menuItemActivated $ (runN bopts) (lookupAlwaysString drCommand db)
            widgetShow $ castToWidget mi
            menuShellAppend ms mi

build bopts builder (DialogOnChange wid dr) = do
    return $ B.adaptBinding (lookupAlwaysInt dr) $ B.Binding (B.valueChangeBinding (popDialog builder wid) 0)
    where 
        popDialog :: Builder -> WidgetId -> IO ()
        popDialog builder wid = do
            dlg <- builderGetObject builder castToDialog wid
            --dialogRun dlg
            widgetShow $ castToWidget dlg
            return ()

build bopts builder (EntryActivated wid drCommand) = do
    entry <- builderGetObject builder castToEntry wid
    return $ B.adaptBinding 
        (lookupAlwaysString drCommand) $ 
        B.Binding (B.objectOnBinding Nothing entry entryActivated (getHandler entry))
    where
        getHandler :: Entry -> String -> IO ()
        getHandler entry cmd = do
            text <- entryGetText entry
            (runWithStdin bopts) cmd text

build bopts builder (EntryChanged wid drCommand) = do
    entry <- builderGetObject builder castToEntry wid
    let editable = castToEditable entry
    return $ B.adaptBinding 
        (lookupAlwaysString drCommand) $ 
        B.Binding (B.objectOnBinding Nothing editable editableChanged (getHandler entry))
    where
        getHandler :: Entry -> String -> IO ()
        getHandler entry cmd = do
            text <- entryGetText entry
            (runWithStdin bopts) cmd text

{- 
build bopts builder (EntryText wid dr) = do
    entry <- builderGetObject builder castToEntry wid
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.entryStringBinding entry)
-}
{-build bopts builder (EntryText wid dr) = do
    entry <- builderGetObject builder castToEntry wid
    -- we want to update the db with the new value when it changes
    return $ B.Binding $ B.groupedBinding [
        B.Binding 
            (B.objectOnBinding Nothing Nothing (castToEditable entry)  editableChanged 
                (autoSync dr bopts "auto update entry")),
        B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.entryStringBinding entry) ]
-}
build bopts builder (EntryText wid dr) = do
    entry <- builderGetObject builder castToEntry wid
    editable <- builderGetObject builder castToEditable wid
    -- this will force a sync
    on editable editableChanged (do
        text <- entryGetText entry
        autoSync dr bopts $ StringValue text)
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.entryStringBinding entry)

build bopts _ (Group gladepath bindings) = do
    -- separate the static bindings from the dynamic bindings
    builder <- builderNew
    builderAddFromFile builder gladepath
    buildGroup bopts builder bindings

build bopts builder (HideOnDelete wid) = do
    widget <- builderGetObject builder castToWidget wid
    return $ 
        B.Binding (B.objectOnBinding Nothing widget deleteEvent (runDeleteEvent widget))
    where
        runDeleteEvent :: Widget -> UiDb -> EventM EAny Bool
        runDeleteEvent widget _ = do
            liftIO $ widgetHide widget
            return True
     
build bopts builder (IconViewListItems wid dr columns) = do
    iconView <- builderGetObject builder castToIconView wid
    ls <- listStoreNew []
    iconViewSetModel iconView $ Just ls
    let textCid = makeColumnIdString 0 :: ColumnId row String
    iconViewSetTextColumn iconView textCid
    let pixbufCid = makeColumnIdPixbuf 1 :: ColumnId row Pixbuf
    iconViewSetPixbufColumn iconView pixbufCid
    -- My listStore is a listStore of Db
    -- SO the build Column thing equivalent is needed (in a way!)
    -- be cause there's no way that anything useful will get extracted from that magically
    -- NOTE: it's at the MODEL level that we need to do this for this to work,
    -- because we have no cell renderer here...
    -- So... it's our list STORE that is not really good.
    -- Too complicated for now.

    -- create the binding
    return $ B.adaptBinding (lookupAlwaysUiDb dr) $ B.Binding $ B.listStoreBinding ls MDb.empty 

build bopts builder (ImageBase64 wid dr) = do
    image <- builderGetObject builder castToImage wid
    return $ B.adaptIOBinding 
        (toPixbuf . lookupAlwaysString dr) $ 
        B.Binding (B.imagePixbufBinding image)
    where
        toPixbuf :: String -> IO Pixbuf
        toPixbuf s = do
            eitherPixbuf <- base64ToPixbuf bs
            either (\err -> do
                liftIO $ errorM "buildImageBase64" $ "ERROR converting base64 string to pixbuf " ++ err 
                emptyPixbuf) return eitherPixbuf
            where
                bs = pack s
                emptyPixbuf :: IO Pixbuf
                emptyPixbuf = pixbufNew ColorspaceRgb True 8 32 32 

build bopts builder (ImageSvg wid dr) = do
    image <- builderGetObject builder castToImage wid
    return $ B.adaptIOBinding 
        (toPixbuf . lookupAlwaysString dr) $ 
        B.Binding (B.imagePixbufBinding image)
    where
        toPixbuf :: String -> IO Pixbuf
        toPixbuf s = do
            eitherPixbuf <- svgToPixbuf "." bs
            either (\err -> do
                liftIO $ errorM "buildImageSvg" $ "ERROR converting svg string to pixbuf " ++ err 
                emptyPixbuf) return eitherPixbuf
            where
                bs = pack s
                emptyPixbuf :: IO Pixbuf
                emptyPixbuf = pixbufNew ColorspaceRgb True 8 32 32 

build bopts builder (LabelText wid dr) = do
    label <- builderGetObject builder castToLabel wid
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.labelStringBinding label)

-- missing: ListBoxSelect

build bopts builder (MenuItemActivated wid dr) = do
    item <- builderGetObject builder castToMenuItem wid
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing item menuItemActivated (run bopts))

build bopts builder (MenuItemLabel wid dr) = do
    item <- builderGetObject builder castToMenuItem wid
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.menuItemLabelBinding item)

build bopts builder (MouseButtonPress wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [ButtonPressMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget buttonPressEvent (runMouseEvent bopts))

build bopts builder (MouseButtonRelease wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [ButtonReleaseMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget buttonReleaseEvent (runMouseEvent bopts))

build bopts builder (MouseCapture wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [ButtonPressMask, ButtonReleaseMask, ButtonMotionMask, PointerMotionMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget buttonPressEvent (runMouseCapture widget))

build bopts builder (RangeMinMax wid drMin drMax) = do
    range <- builderGetObject builder castToRange wid
    return $ B.adaptBinding (lookupAlwaysRange drMin drMax) $ B.Binding (B.rangeMinMaxBinding (0, 0) range)

build bopts builder (RangeValue wid dr) = do
    range <- builderGetObject builder castToRange wid
    return $ B.adaptBinding (lookupAlwaysDouble dr) $ B.Binding (B.rangeValueBinding range)

build bopts builder (RangeValueChange wid dr) = do
    widget <- builderGetObject builder castToRange wid
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget valueChanged (getHandler widget))
    where
        getHandler :: Range -> String -> IO ()
        getHandler r cmd = do
            v <- rangeGetValue r
            (runWithStdin bopts) cmd (show v)

build bopts builder (RowActivated wid drList dr) = do
    treeView <- builderGetObject builder castToTreeView wid
    return $ B.adaptBinding (lookupAlwaysListOfUiDb drList) $ 
        B.Binding (B.objectOnBinding Nothing treeView rowActivated getHandler)
    where
        -- THIS IS HARD TO UNDERSTAND, REALLY
        -- Note, because we have :
        -- treeViewRowActivated :: TreeViewClass self => self -> TreePath -> TreeViewColumn -> IO ()
        -- and:
        -- objectOnBinding :: (Eq a, GObjectClass o) => 
        --        Maybe a -> Maybe (ConnectId o) -> o -> Signal o c -> (a -> c) -> a -> IO (Binding a)
        -- we have a :: [UiDb]
        --    snd  c :: TreePath -> TreeViewColumn -> IO ()
        -- And the adaptBinding
        -- lookupAlwaysListOfUiDb :: DataReference -> UiDb -> [UiDb]
        -- Is what converts our binding from a UiDb binding to a [UiDb] binding
        getHandler :: [UiDb] -> TreePath -> TreeViewColumn -> IO ()
        getHandler dbs (path:_) column = do
            let cmd = lookupAlwaysString dr (dbs !! path)
            (run bopts) cmd

build bopts builder (SpinButtonIncrements wid dr1 dr2) = do
    spin <- builderGetObject builder castToSpinButton wid
    return $ B.adaptBinding (lookupAlwaysRange dr1 dr2) $ B.Binding (B.spinButtonIncrementsBinding  (0, 0) spin)

build bopts builder (SpinButtonMinMax wid drMin drMax) = do
    spin <- builderGetObject builder castToSpinButton wid
    return $ B.adaptBinding (lookupAlwaysRange drMin drMax) $ B.Binding (B.spinButtonMinMaxBinding  (0, 0) spin)

build bopts builder (SpinButtonSpinned wid dr) = do
    spin <- builderGetObject builder castToSpinButton wid
    return $ B.adaptBinding 
        (runAction spin) $ 
        B.Binding (B.spinButtonOnValueSpinnedBinding Nothing spin)
    where
        runAction :: SpinButton -> UiDb -> IO ()
        runAction spin db = do
            v <- spinButtonGetValue spin
            let action = lookupAlwaysString dr db
            (runWithStdin bopts) action (show v)

build bopts builder (SwitchActive wid dr) = do
    switch <- builderGetObject builder castToSwitch wid
    return $ B.adaptBinding (lookupAlwaysBool dr) $ B.Binding (B.switchActiveBinding switch)

build bopts builder (SwitchActivate wid dr) = do
    switch <- builderGetObject builder castToSwitch wid
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing switch (notifyProperty switchActive) (getHandler switch))
    where
        -- FIXME: this does not work right now
        getHandler :: Switch -> String -> IO ()
        getHandler tb cmd = do
            active <- switchGetActive tb
            runWithStdin bopts cmd (show active)

build bopts builder (TextViewText wid dr) = do
    tv <- builderGetObject builder castToTextView wid
    return $ B.adaptBinding (lookupAlwaysString dr) $ B.Binding (B.textViewStringBinding tv)

build bopts builder (ToggleButtonActive wid dr) = do
    tb <- builderGetObject builder castToToggleButton wid
    return $ B.adaptBinding (lookupAlwaysBool dr) $ B.Binding (B.toggleButtonActiveBinding tb)

build bopts builder (ToggleButtonToggled wid dr) = do
    tb <- builderGetObject builder castToToggleButton wid
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing tb toggled (getHandler tb))
    where
        getHandler :: ToggleButton -> String -> IO ()
        getHandler tb cmd = do
            active <- toggleButtonGetActive tb
            runWithStdin bopts cmd (show active)

build bopts builder (ToolButtonActivated wid dr) = do
    button <- builderGetObject builder castToToolButton wid
    return $ B.adaptBinding 
        (run bopts . lookupAlwaysString dr) $ 
        B.Binding (B.toolButtonClickedBinding Nothing button)

build bopts builder (TreeViewListItems wid dr columns) = do
    treeView <- builderGetObject builder castToTreeView wid
    -- create the list store
    ls <- listStoreNew []
    mapM_ (buildColumn treeView ls) columns
    -- assign it to the treeview
    treeViewSetModel treeView $ Just ls
    -- create the binding
    return $ B.adaptBinding (lookupAlwaysUiDb dr) $ B.Binding $ B.listStoreBinding ls MDb.empty 
    where
        -- adaptIOBinding :: (b -> IO a) -> Binding a -> Binding b
        buildColumn :: TreeView -> ListStore UiDb -> Column -> IO Int
        buildColumn treeView ls (Column title dr format) = do
            column <- treeViewColumnNew
            -- TODO use format here
            case format of
                Text -> do
                    cell <- cellRendererTextNew
                    cellLayoutPackStart column cell True
                    cellLayoutSetAttributes column cell ls $ \row -> [ cellText := lookupAlwaysString dr row ]
                AppTypes.Icon -> do
                    cell <- cellRendererPixbufNew
                    cellLayoutPackStart column cell True
                    cellLayoutSetAttributes column cell ls $ \row -> [ cellPixbufIconName := lookupAlwaysString dr row ]
            treeViewColumnSetTitle column title
            treeViewAppendColumn treeView column

build bopts builder (TreeViewSelectionChanged wid drList dr) = do
    treeView <- builderGetObject builder castToTreeView wid
    selection <- treeViewGetSelection treeView
    return $ B.adaptBinding (lookupAlwaysListOfKeyUiDb drList) $ 
        B.Binding (B.objectOnBinding Nothing selection treeSelectionSelectionChanged getHandler)
    where
        getHandler :: [(DbKey, UiDb)] -> IO ()
        getHandler kdbs = do
            treeView <- builderGetObject builder castToTreeView wid
            selection <- treeViewGetSelection treeView
            maybeTreeItem <- treeSelectionGetSelected selection
            maybeModel <- treeViewGetModel treeView
            let maybeStuff = do
                    treeItem <- maybeTreeItem
                    model <- maybeModel
                    return (treeItem, model)
            maybe runSkip (uncurry runTreeItem) maybeStuff
            where
                runSkip :: IO ()
                runSkip = do
                    infoM "notification" $ "skip " 
                runTreeItem :: TreeIter -> TreeModel -> IO ()
                runTreeItem treeItem model = do
                    (path:_) <- treeModelGetPath model treeItem
                    let (key, uidb) = kdbs !! path
                    --print (key, uidb)
                    let cmd = lookupAlwaysString dr uidb
                    run bopts (cmd ++ " " ++ "\"" ++ show key ++ "\"")

build bopts builder (TreeViewSelectionCleared wid drCommand) = do
    treeView <- builderGetObject builder castToTreeView wid
    selection <- treeViewGetSelection treeView
    return $ B.adaptBinding 
        (lookupAlwaysString drCommand) $ 
        B.Binding (B.objectOnBinding Nothing selection treeSelectionSelectionChanged getHandler)
    where
        getHandler :: String -> IO ()
        getHandler cmd = do
            treeView <- builderGetObject builder castToTreeView wid
            selection <- treeViewGetSelection treeView
            maybeTreeItem <- treeSelectionGetSelected selection
            when (isNothing maybeTreeItem) $ runN bopts cmd 

build bopts builder (TreeViewSingleSelection wid drSelection) = do
    treeView <- builderGetObject builder castToTreeView wid
    return $ B.adaptBinding 
        (stringToTreePath . stringToGlib . lookupAlwaysString drSelection) $ 
            B.Binding (B.treeViewSingleSelectionBinding treeView)

build bopts builder (WidgetHide wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget hideSignal (run bopts))

build bopts builder (WidgetMap wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget mapSignal (run bopts))

build bopts builder (WidgetRealize wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget realize (run bopts))

build bopts builder (WidgetSensitive wid dr) = do
    entry <- builderGetObject builder castToWidget wid
    return $ B.adaptBinding (lookupAlwaysBool dr) $ B.Binding (B.widgetSensitiveBinding entry)
    
build bopts builder (WidgetShow wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget showSignal (run bopts))

build bopts builder (WidgetUnRealize wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget unrealize (run bopts))

build bopts builder (WidgetUnmap wid dr) = do
    widget <- builderGetObject builder castToWidget wid
    widgetAddEvents widget [AllEventsMask]
    return $ B.adaptBinding 
        (lookupAlwaysString dr) $ 
        B.Binding (B.objectOnBinding Nothing widget unmapSignal (run bopts))
    
build bopts _ b = do
    liftIO $ errorM "build" $ "Missing builder for binding " ++ show b ++ " using null binding instead"
    return $ B.Binding B.nullBinding
