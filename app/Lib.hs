{-|
Module      : Lib
Description : Run the main loop of the GTK ui application
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Run the main loop of the GTK ui application
-}
module Lib
    ( 
    runGui,
    loadSettings,
    AppTypes.Settings(..)
    ) where
import Bindings
import Build
import Control.Concurrent(forkIO)
import Control.Monad(join)
import Data.IORef(newIORef, readIORef, writeIORef, IORef)
import Graphics.UI.Gtk
import Run
import Server.DbIO(server, createJournalRef)
import System.Log.Logger
import Text.Read(readMaybe)
import AppTypes

-- | Read settings from a file
loadSettings :: FilePath -> IO (Maybe AppTypes.Settings)
loadSettings path = do
    text <- readFile path
    return $ readMaybe text

-- | Runs the gui based on some settings
runGui :: AppTypes.AppSettings -> AppTypes.Settings -> IO ()
runGui options someSettings = do
    infoM logContext "initGUI"
    initGUI
    infoM logContext "builder"
    builder <- builderNew
    allowEvents <- newIORef True
    syncQueries <- newIORef []
    fireS <- newIORef (putStrLn "") 
    dbioref <- createJournalRef (db someSettings) "creation"
    
    let bo = BuildOptions {
        --allowEvents = allowEvents,
        fireStartup = fireS,
        dbIORef = dbioref,
        -- run (no allowEvents)
        AppTypes.runWithStdinN = Run.runWithStdinN fIO,
        -- run (no allowEvents)
        AppTypes.runN = Run.runN fIO,
        -- run with stdin (allow events)
        AppTypes.runWithStdin = Run.runWithStdin fIO allowEvents,
        -- run (with allowEvents)
        AppTypes.run = Run.run fIO allowEvents,
        -- run (with allowEvents) -- we could get rid of this using stdin
        AppTypes.runMouseEvent = Run.runMouseEvent fIO allowEvents

    }
    Binding binding <- build bo builder (root someSettings)
    infoM logContext "binding"
    nb <- binding (db someSettings)

    infoM logContext "launch server"
    ioRef <- newIORef (db someSettings, nb)
    threadId <- forkIO (server (socketPath someSettings) dbioref (process ioRef allowEvents))

    join $ readIORef fireS

    infoM logContext "run mainGUI"
    mainGUI
    infoM logContext "exit"

    where
        fIO = if forkEvents options then 
            (\o -> do 
                infoM logContext "forkIO"
                forkIO o 
                return ()) 
            else (\o -> do
                    infoM logContext "noFork"
                    o)
        logContext = "runGui"
        process :: IORef (UiDb, Bindings.Binding UiDb) -> IORef Bool -> UiDb -> UiDb -> IO ()
        process ioRef allowEvents _ db = do
            idleAdd (updateBindings ioRef allowEvents db) priorityDefaultIdle
            return ()

        updateBindings :: IORef (UiDb, Bindings.Binding UiDb) -> IORef Bool -> UiDb -> IO Bool
        updateBindings ioRef allowEvents ndb = do
            (db, Binding bindings) <- readIORef ioRef
            -- we don't want events to be triggered when WE change things
            -- so we disable events
            writeIORef allowEvents False
            newBindings <- bindings ndb
            -- now, the update is complete so we re enable them
            writeIORef allowEvents True
            writeIORef ioRef (ndb, newBindings)
            return False
         
