{-|
Module      : LoadImages
Description : Load images
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Load and convert images
-}
module LoadImages(
    base64ToPixbuf,
    svgToPixbuf 
    ) where

import Codec.Picture(
    Image(..), 
    DynamicImage(..),    
    pixelAt,
    PixelRGB8(..),
    Pixel(..),
    decodeImage,
    convertRGB8)
import Graphics.UI.Gtk.Gdk.Pixbuf(
    Pixbuf(..), 
    PixbufData(..), 
    pixbufNew, 
    Colorspace(..), 
    pixbufGetPixels,
    pixbufGetRowstride)
import Graphics.Svg(
    parseSvgFile,
    Document(..)
    )
import Graphics.Rasterific.Svg(
    renderSvgDocument
    )
import Graphics.Text.TrueType(
    FontCache(..),
    emptyFontCache
    )
import Data.Array.MArray
import Data.Word (Word8)
import Data.ByteString.Base64(decodeLenient)
import Data.ByteString(ByteString(..))
import Control.Monad (sequence_)

-- | Convert an image to a Pixbuf. Quite pathetic.
-- There should be a good way.
imageToPixbuf :: Image PixelRGB8 -> IO Pixbuf
imageToPixbuf img = do
    let imgD = imageData img
    let w = imageWidth img
    let h = imageHeight img
    pixb <- pixbufNew ColorspaceRgb True 8 w h
    stride <- pixbufGetRowstride pixb
    pix <- pixbufGetPixels pixb :: IO (PixbufData Int Word8)
    sequence_ [ 
        do
            let (PixelRGB8 r g b) = pixelAt img x y
            let pos = y * stride + x * 4
            writeArray pix pos r
            writeArray pix (pos + 1) g
            writeArray pix (pos + 2) b
            writeArray pix (pos + 3) 255
            return ()
        | y <- [0..(h-1)], x <- [0..(w-1)]]
    return pixb
    
-- | Convert raw image data to an Image
base64ToImage :: ByteString -> Either String (Image PixelRGB8)
base64ToImage bytes = do
    let imgData = decodeLenient bytes
    decoded <- decodeImage imgData
    return $ convertRGB8 decoded

-- | Convert raw image data to a Pixbuf
base64ToPixbuf :: ByteString -> IO (Either String Pixbuf)
base64ToPixbuf bytes = 
    case img of 
        (Left b) -> return $ Left b
        (Right img) -> sequence (Right $ imageToPixbuf img)
    where
        img = base64ToImage bytes     

-- | Convert an svg document to a Pixbuf
documentToImage :: FontCache -> Document -> IO Pixbuf
documentToImage cache document = do
    (img, _ ) <-renderSvgDocument cache Nothing 96 document
    imageToPixbuf $ convertRGB8 $ ImageRGBA8 img

-- | Convert ns svg to a Pixbuf
svgToPixbuf :: FilePath -> ByteString -> IO (Either String Pixbuf)
svgToPixbuf sourcePath bytes = 
    maybe 
        (return $ Left "Could not parse svg") 
        (\doc -> do
            pb <- documentToImage emptyFontCache doc
            return $ Right pb)
        $ parseSvgFile sourcePath bytes
