{-|
Module      : Main
Description : The main ui app (ui server)
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The main ui application runs the user interface
and waits for queries from external processes
(to update the view model)
-}
module Main where

import Lib
import Options.Applicative
import Options.Applicative.Help.Pretty
import System.Exit(ExitCode(..), exitWith)
import Server.Logger
import System.Log.Logger
import AppTypes(AppSettings(..))

-- | Exit code
invalidSettings = ExitFailure 1

-- | Generate an option parser that returns AppSettings
optParser :: Parser AppSettings
optParser = AppSettings
    <$> option auto ( 
        ( long "loglevel"
        <> short 'l'
        <> value WARNING
        <> help "Log level (DEBUG, INFO, NOTICE, WARNING, ERROR, CRITICAL, ALERT, EMERGENCY" ))
    <*> argument str (metavar "SETTINGSFILE")       
    <*> switch
          ( long "forkevents"
         <> short 'f'
         <> help "Whether to fork event handlers" )

-- | Parse the options
opts :: ParserInfo AppSettings
opts = info (optParser <**> helper)
  ( fullDesc
  <> progDesc "Start the ui server"
  <> header "ui - user interface server" 
  <> (footerDoc . Just) 
        (text "The ui server shows the window of an application. The content of the \n\
        \widgets is taken from an in memory database. Declarative bindings tells ui \n\
        \how to use the data (for example, it is possible for many buttons to use \n\
        \the same label, and in this case if the text changes at one place in the database \n\
        \changes are observed at many places in the ui). \n\
        \The ui server waits for connections on a unix domain socket. Each connection acts as \n\
        \an atomic transaction (all queries performed during this connection \n\
        \will either succeed or fail as whole). The database uses immutable data: \n\
        \the full history of changes is kept and every change creates a new version \n\
        \of the data. The version history can be retrieved. Previous versions can be \n\
        \inspected.\n\n" </> 
        text "The candy program can be used to send request to the state program." </> text "" </>
        text "Settings File\n" </>
            (text "The settings file has the following structure:\n\
            \Settings {\n\
            \    root = see below,\n\
            \    db = see below,\n\
            \    socketPath = \"/path/to/unixdomainsocket\"\n\
            \}\n" </>
            text "root should be a binding (the topmost binding is typically a group).\n\
            \the possible bindings are:\n\
            \AppWindow WidgetId\n\
            \    ex: AppWindow \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ApplicationReady DataReference\n\
            \    ex: ApplicationReady (Literal (StringValue \"notify-send hello\"))\n\
            \    A program to execute when the application is ready (the ui server is ready to receive requests)\n\
            \ButtonActivated WidgetId DataReference \n\
            \    ex: ButtonActivated \"ID_BUTTON\" (Literal (StringValue \"notify-send button-activated\"))\n\
            \    Make 'notify-send button-activated' be run when the ID_BUTTON button is clicked.\n\
            \ButtonText WidgetId DataReference\n\
            \    ex: ButtonText \"ID_BUTTON1\" (Reference [\"label\"])\n\
            \    Use the string with the 'label' key the database as the button label.\n\
            \ComboBoxModel WidgetId DataReference [Column] \n\
            \    ex: ComboBoxModel \"ID_COMBO1\" (Reference [\"comboitems\"]) [Column \"Title\" (Reference [\"thefield\"]) Text]\n\
            \    Populate the ID_COMBO1 combo box with the combiitems list, using the thefiled field as a text source\n\
            \ComboBoxSelectionChanged WidgetId DataReference DataReference DataReference \n\
            \    ex: ComboBoxSelectionChanged \"ID_COMBO\" (Reference [\"comboitems\"]) (Literal (StringValue \"notify 'combo: '\")) (Reference [\"thestring\"])\n\
            \    Call 'notify combo: ' when selection changes\n\
            \ComboBoxSelectionCleared WidgetId DataReference\n\
            \    ex: ComboBoxSelectionCleared \"ID_COMBO\" (Reference [\"command\"]) \n\
            \    Run program referenced by command when the selection from ID_COMBO is cleared\n\
            \ ... to be continued ...\n"
{-            \ComboBoxSelection WidgetId DataReference\n\
            \    ex: ComboBoxSelection \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ContainerItems WidgetId DataReference GladePath WidgetId [Binding]  \n\
            \    ex: ContainerItems \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ContextMenu\n\
            \    ex: ContextMenu\"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \DialogOnChange WidgetId DataReference\n\
            \    ex: DialogOnChange \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \EntryActivated WidgetId DataReference\n\
            \    ex: EntryActivated \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \EntryChanged WidgetId DataReference\n\
            \    ex: EntryChanged \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \EntryText WidgetId DataReference\n\
            \    ex: EntryText \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \Group GladePath [Binding] \n\
            \    ex: Group \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \HideOnDelete\n\
            \    ex: HideOnDelete\"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \IconViewListItems WidgetId DataReference [Column] \n\
            \    ex: IconViewListItems\"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ImageBase64\n\
            \    ex: mageBase64\"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ImageSvg WidgetId DataReference\n\
            \    ex: ImageSvg \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \LabelText WidgetId DataReference\n\
            \    ex: LabelText \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ListBoxSelect WidgetId DataReference DataReference\n\
            \    ex: ListBoxSelect \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \MenuItemActivated WidgetId DataReference\n\
            \    ex: MenuItemActivated \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \MenuItemLabel WidgetId DataReference\n\
            \    ex: MenuItemLabel \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \MouseButtonPress WidgetId DataReference\n\
            \    ex: MouseButtonPress \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \MouseButtonRelease WidgetId DataReference\n\
            \    ex: MouseButtonRelease \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \MouseCapture WidgetId DataReference\n\
            \    ex: MouseCapture \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \RangeMinMax WidgetId DataReference DataReference\n\
            \    ex: RangeMinMax \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \RangeValue WidgetId DataReference\n\
            \    ex: RangeValue \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \RangeValueChange WidgetId DataReference\n\
            \    ex: RangeValueChange \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \RowActivated WidgetId DataReference DataReference\n\
            \    ex: RowActivated \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \SpinButtonIncrements WidgetId DataReference DataReference\n\
            \    ex: SpinButtonIncrements \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \SpinButtonMinMax WidgetId DataReference DataReference\n\
            \    ex: SpinButtonMinMax \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \SpinButtonSpinned WidgetId DataReference\n\
            \    ex: SpinButtonSpinned \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \SwitchActive WidgetId DataReference\n\
            \    ex: SwitchActive \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \SwitchActivate WidgetId DataReference\n\
            \    ex: SwitchActivate \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \TextViewText WidgetId DataReference\n\
            \    ex: TextViewText \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ToggleButtonActive WidgetId DataReference\n\
            \    ex: ToggleButtonActive \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ToggleButtonToggled WidgetId DataReference\n\
            \    ex: ToggleButtonToggled \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \ToolButtonActivated WidgetId DataReference\n\
            \    ex: ToolButtonActivated \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \TreeViewListItems WidgetId DataReference [Column] \n\
            \    ex: TreeViewListItems \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \TreeViewSelectionChanged\n\
            \    ex: TreeViewSelectionChanged\"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \TreeViewSelectionCleared WidgetId DataReference\n\
            \    ex: TreeViewSelectionCleared \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \TreeViewSingleSelection WidgetId DataReference\n\
            \    ex: TreeViewSingleSelection \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetHide WidgetId DataReference\n\
            \    ex: WidgetHide \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetMap WidgetId DataReference\n\
            \    ex: WidgetMap \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetRealize WidgetId DataReference\n\
            \    ex: WidgetRealize \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetSensitive WidgetId DataReference\n\
            \    ex: WidgetSensitive \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetShow WidgetId DataReference\n\
            \    ex: WidgetShow \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetUnRealize WidgetId DataReference\n\
            \    ex: WidgetUnRealize \"ID_MAIN\"\n\
            \    Set the widget id of the main window of the application\n\
            \WidgetUnmap WidgetId DataReference \n" -}
            ) </>
        text "See also: candy")
    )
            

-- | Run the ui server
main :: IO ()
main = do
    options <- execParser opts
    setupLogger "candy" $ logLevel options
    infoM logContext $ "options = " ++ show options
    maybeSettings <- loadSettings $ settingsPath options
    maybe 
        (do
            errorM logContext $ "could not parse settings file " ++ settingsPath options
            exitWith invalidSettings
            )
        (runGui options)
        maybeSettings
    where
        logContext = "main"
