{-|
Module      : MouseCapture
Description : An experiment for processing mouse in a single process
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The goal of this module is to process event streams in a single
process (continuously).
-}
module MouseCapture 
    (
    runMouseCapture 
    ) where

import Control.Exception(catch, IOException(..))
import Control.Monad.IO.Class(liftIO)
import Data.IORef
import Graphics.UI.Gtk
import System.Process
import GHC.IO.Handle

runMouseCapture :: Widget -> String -> EventM EButton Bool
runMouseCapture widget cmd = do
    (x, y) <- eventCoordinates
    liftIO $ do 
        (Just hin, _ {-Just hout-}, _, _) <-
            liftIO $ createProcess (proc "bash" ["-c", cmd]) { 
                --std_out = CreatePipe,
                std_in = CreatePipe }
        showXY hin "down" x y
        idMove <- widget `on` motionNotifyEvent $ handleMove hin
        idRefs <- newIORef (idMove, Nothing) :: IO (IORef (ConnectId Widget, Maybe (ConnectId Widget)))
        idUp <- widget `on` buttonReleaseEvent $ handleUp hin idRefs
        writeIORef idRefs (idMove, Just idUp)
        putStrLn ("down " ++ show x ++ " " ++ show y)
    return True
    -- we should create a process to read hout and process what comes from it
    where
        handleMove :: Handle -> EventM EMotion Bool
        handleMove hout = do
            (x, y) <- eventCoordinates
            liftIO $ do
                putStrLn "move"
                showXY hout "move" x y
            return True

        handleUp :: Handle -> IORef (ConnectId Widget, Maybe (ConnectId Widget))  -> EventM EButton Bool
        handleUp hout idRefs = do
            (x, y) <- eventCoordinates
            liftIO $ do
                putStrLn "up"
                showXY hout "up" x y
                (idMove, Just idUp) <- readIORef idRefs
                -- unhook handlers
                signalDisconnect idMove
                signalDisconnect idUp
                -- close output handle
                hClose hout
            return True
        showXY :: (Show a) => Handle -> String -> a -> a -> IO ()
        showXY hout prefix x y =
            catch (do
                hPutStr hout (prefix ++ " " ++ show x ++ " " ++ show y ++ "\n")
                hFlush hout
                )
                (\e -> do
                    let err = e :: IOException
                    putStrLn $ "exception"
                    return ())
