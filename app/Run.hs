{-|
Module      : Build
Description : Binding building mechanism
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Instanciate the various bindings
-}
module Run
    (
    runWithStdinN,
    runN, 
    runWithStdin, 
    run, 
    runMouseEvent
    ) where

import Control.Monad
import Control.Monad.IO.Class
import Data.IORef(IORef(..), readIORef)
import Graphics.UI.Gtk
import System.Exit(ExitCode(..))
import System.Log.Logger
import System.Process (readProcessWithExitCode)

-- | Call a bash command (an action associated with a binding)
callBashCommand :: String -> String -> IO String
callBashCommand cmd stdin = do
    (exit, stderr, stdout) <- readProcessWithExitCode "bash" [ "-c", cmd ] stdin
    --print exit
    case exit of
        ExitFailure n -> do
            errorM logContext $ "callBashCommand failed, exit code = " ++ show n 
            warningM logContext $ "stdout = " ++ stdout 
            warningM logContext $ "stderr = " ++ stderr 
            return stdout
        ExitSuccess -> do
            infoM logContext "callBashCommand succeded"
            infoM logContext $ "stdout = " ++ stdout 
            infoM logContext $ "stderr = " ++ stderr 
            return stdout
    where logContext = "callBashCommand"

type ForkIO = IO () -> IO ()
-- | Run with stdin without the ref based blocking thing
runWithStdinN :: ForkIO -> String -> String -> IO ()
runWithStdinN fIO cmd stdin = do
    fIO $ do
        callBashCommand cmd stdin
        return ()
    return ()

-- | Run wihout the ref based blocking thing
runN :: ForkIO -> String -> IO ()
runN fIO cmd = runWithStdinN fIO cmd ""

-- | Run providing an input as stdin
runWithStdin :: ForkIO -> IORef Bool -> String -> String -> IO ()
runWithStdin fIO ref cmd stdin = do
    b <- readIORef  ref
    when b $ do 
        fIO $ do
            callBashCommand cmd stdin
            return ()
        return ()

-- | Run without stdin and stdout
run :: ForkIO -> IORef Bool -> String -> IO ()
run fIO ref cmd = runWithStdin fIO ref cmd ""

-- | Run a mouse event (providing coordinates)
runMouseEvent :: ForkIO -> IORef Bool -> String -> EventM EButton Bool
runMouseEvent fIO ref cmd = do
    b <- liftIO $ readIORef ref
    (x, y) <- eventCoordinates
    when b $ 
        liftIO $ do
            fIO $ do
                callBashCommand cmd (show x ++ " " ++ show y)
                return ()
            return ()
    return True
