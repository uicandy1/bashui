#! /usr/bin/env bash
script="$(readlink -f "$(dirname $0)")"
tmpdir="$(mktemp -d)"
cd "$tmpdir"
cp "$script/Dockerfile" .
git clone git@gitlab.com:uicandy1/bashui.git 
docker build . 
cd "$script"
rm "$tmpdir"
