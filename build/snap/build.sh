#! /usr/bin/env bash
script="$(readlink -f "$(dirname $0)")"
tmpdir="$(mktemp -d)"
cd "$tmpdir"
mkdir snap
echo "building in $tmpdir"
cp "$script/snapcraft.yaml" snap
snapcraft clean
SNAPCRAFT_BUILD_ENVIRONMENT_MEMORY=4G snapcraft
