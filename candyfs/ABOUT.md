# Rationale

Shell (sh, bash, etc.) is the language and user interface of unix: when
you use unix, you talk to it in shell language to get things done. You do
this either interactively (from the terminal) or by writing scripts.

The three building blocks that make this possible are the process and the file
system and the file descriptor. 

A process runs a program in isolation from other
proccesses. It receives a file decriptor for its input, a file descriptor 
for its output and a file descriptor for its errors and logs. Ideally
the program that it runs sticks to only dealing with stdin and stdout 
making it essentially a filter, the most useful and elegant form of
program in this os. But it also has access to the file system.

The file system is a tree shaped naming mechanism that gives access to
persisting state (and stateful devices) in the unix os. From a nested
name, you can access files and devices. Without the file system (and file
system access), unix programs would almost be unable to produce any useful
effect.

The last building block is the file descriptor. The file system allows 
to get access to a file descriptor through a path (ex: by opening the path).
There are also system calls (thr fourth building block that I don't really cover
here) that allow to get access to file descriptors among other things.


-------
