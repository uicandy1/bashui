{-|
Module      : DbDir
Description : Listing of transactions
Copyright   : (c) Hugo Windisch, 2020
License     : BSD3
Stability   : experimental
-}
{-# LANGUAGE OverloadedStrings #-}

module DbDir (
    DbFileHandle (..),
    dbGetFileStat,
    dbOpen,
    dbOpenDirectory,
    dbRead,     
    dbReadDirectory,
    dbStat,
    --ldb,
    toString
    ) where
--import Data.IORef(readIORef)
import Data.List(take, drop)
import Data.Map.Strict as Map
import Data.Maybe(maybe)
import Foreign.C.Error
import MDb
--import Options.Applicative
--import Options.Applicative.Help.Pretty
--import Server.DbIO
import Server.KeyType
import Server.Types(UiDb, DbKey(..))
--import System.Environment(getProgName)
import System.Fuse
import System.IO(FilePath)
import System.Posix.Files
import System.Posix.Types
--import Text.Read(readMaybe)
import qualified Data.ByteString.Char8 as B

-- | What we use as a file handle
data DbFileHandle = DbFileHandle UiDb

toString :: DbKey -> String
toString (Key s) = s
tostring (Index i) = "#" ++ (show i)


-- | Get the file stat of a db
dbStat :: UiDb -> FuseContext -> FileStat
dbStat (Leaf _ _) ctx = FileStat { 
        statEntryType = RegularFile
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }

dbStat (Tree _ m) ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }

{-
-- | latest db from a DBIORef
ldb :: DBIORef -> IO UiDb
ldb dbio = do
    (UndoableJournal j1 j2) <- readIORef dbio
    return $ latestDb j1

-- | split a path into a bunch of keys
splitPath :: FilePath -> [DbKey]
splitPath path =
    (Key . B.unpack) <$> (Data.List.filter (/= "") (B.split '/' (B.pack path)))
-}

dbGetFileStat :: UiDb -> [String] -> IO (Either Errno FileStat)
dbGetFileStat db keys = do
    ctx <- getFuseContext
    let found = deepLookup (Key <$> keys) db --  Db k v -> Maybe (Db k v)
    return $ maybe (Left eNOENT) (\f -> Right $ dbStat f ctx) found

dbOpen :: UiDb -> [String] -> OpenMode -> OpenFileFlags -> IO (Either Errno DbFileHandle)
dbOpen db path mode flags = do
    let keys = Key <$> path
    let found = deepLookup keys db 
    return $ maybe 
                (Left eACCES)
                (Right . DbFileHandle)
                found

-- | Open a directory
dbOpenDirectory :: UiDb -> [String] -> IO Errno
dbOpenDirectory db keys = do
    return $ maybe eNOENT (const eOK) $ deepLookup (Key <$> keys) db

-- | Read a db as a directory
crd :: UiDb -> IO (Either Errno [(FilePath, FileStat)])
crd db = do
    ctx <- getFuseContext
    return $ listFiles ctx db
    where
        listFiles :: FuseContext -> UiDb -> Either Errno [(FilePath, FileStat)]
        listFiles ctx (Leaf _ _) = Left eNOENT -- FIXME: proper error?
        listFiles ctx (Tree _ m) =
            Right $ uncurry (listKV ctx) <$> toAscList m
        -- list a given key value
        listKV :: FuseContext -> DbKey -> UiDb -> (FilePath, FileStat)
        listKV ctx k db = (toString k, dbStat db ctx) 

-- | Read a directory
dbReadDirectory :: UiDb -> [String] -> IO (Either Errno [(FilePath, FileStat)])
dbReadDirectory db path = do
    let keys = Key <$> path
    ctx <- getFuseContext
    let found = deepLookup keys db 
    maybe (return $ Left eNOENT) crd found

-- Open a file (at a fixed point in history)
{-dbOpen :: DBIORef -> FilePath -> OpenMode -> OpenFileFlags -> IO (Either Errno DbFileHandle)
dbOpen dbio path mode flags = do
    let keys = splitPath path
    db <- ldb dbio
    ctx <- getFuseContext
    let found = deepLookup keys db 
    return $ maybe 
                (Left eACCES)
                (Right . DbFileHandle)
                found
-}
-- Read from a file
dbRead :: UiDb -> [String] -> DbFileHandle -> ByteCount -> FileOffset -> IO (Either Errno B.ByteString)
dbRead odb path (DbFileHandle db) byteCount offset =
    return $ Right . B.pack $ (Data.List.take (fromIntegral byteCount) . Data.List.drop (fromIntegral offset)) dbstr
    where
        keya = Key <$> path
        dbstr = show db 

