{-|
Module      : Main
Description : Expose the db as a file system (easier to consume
              by shell script. But is it doable at all, when everything
              is considered?
Copyright   : (c) Hugo Windisch, 2020
License     : BSD3
Stability   : experimental

State db server
-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.List(filter)
import Data.Maybe(maybe)
--import DbDir
import Foreign.C.Error
import MDb
import Options.Applicative
import Options.Applicative.Help.Pretty
import RevisionDir
import Server.DbIO
import System.Environment(getProgName)
import System.Fuse
import System.IO(FilePath)
import System.Posix.Files
import System.Posix.Types
import Text.Read(readMaybe)
import TransactionDir
import Types
import qualified Data.ByteString.Char8 as B

-- | Server configuraion
data FSSettings = FSSettings
  { 
    database :: String,
    mountingPoint :: String
  }
  deriving Show

-- | Generate an option parser
optParser :: Parser FSSettings
optParser = FSSettings
      <$> strOption
          ( long "db"
         <> short 'd'
         <> value ""
         <> help "Initial DB path" )
      <*> strOption
          ( long "mp"
         <> short 'p'
         <> value ""
         <> help "Mounting point" )

-- | Parse the options
opts :: ParserInfo FSSettings
opts = info (optParser <**> helper)
  ( fullDesc
  <> header "candyfs - an in memory database exposed as a file system through fuse"
  <> progDesc "Start the file system"
  <> (footerDoc . Just) 
        (text "Exposes an in memory db as a file system\n" </>
        text "The candy program can be used to send request to the state program." </> text "" </>
        text "See also: candy")
     )

main :: IO ()
main = do
    options <- execParser opts
    let dbpath = database options 
    let mpt = mountingPoint options
    --appendFile "/home/hugo/blah.log" "fdsfsdfs"
    if dbpath /= "" then do
            text <- readFile dbpath
            maybe (do
                    --errorM "main" $ "could not read db file" ++ dbpath
                    --exitWith invalidSettings
                    return () )
                (\db -> do
                    dbioref <- createJournalRef db "initial db"
                    runFileSystem dbioref mpt)
                (readMaybe text)
        else do
            --infoM "main" $ "no dbpath, using empty db"
            dbioref <- createJournalRef MDb.empty "initial db"
            runFileSystem dbioref mpt

-- | split a path into a bunch of keys
splitPath :: FilePath -> [String]
splitPath path =
    B.unpack <$> (Data.List.filter (/= "") (B.split '/' (B.pack path)))

runFileSystem :: DBIORef -> String -> IO ()
runFileSystem dbio mountPoint = do 
    prog <- getProgName
    fuseRun prog [mountPoint] (candyFSOps dbio) defaultExceptionHandler

prependPaths :: [a] -> [([a], b)] -> [([a], b)]
prependPaths pre paths =
    pp pre <$> paths
    where
        pp :: [a] -> ([a], b) -> ([a], b)
        pp aa (bb, cc) = (aa ++ bb, cc)

-- Note: not too sure 
candyFSOps :: DBIORef -> FuseOperations CandyFileHandle
candyFSOps dbio = defaultFuseOps { fuseGetFileStat = candyGetFileStat dbio
                            , fuseOpen        = candyOpen dbio
                            , fuseRead        = candyRead dbio
                            , fuseOpenDirectory = candyOpenDirectory dbio
                            , fuseReadDirectory = candyReadDirectory dbio
                            , fuseGetFileSystemStats = candyGetFileSystemStats dbio
                            }

revPrefix = "rev"
trPrefix = "tr"

candyGetFileStat :: DBIORef -> FilePath -> IO (Either Errno FileStat)
candyGetFileStat dbio fp 
    | sp == [] = do
        ctx <- getFuseContext
        return $ Right (dirStat ctx)
    | hsp == revPrefix = revGetFileStat dbio tsp
    | hsp == trPrefix = trGetFileStat dbio tsp
    | otherwise = return (Left eNOENT)
    where
        sp = splitPath fp
        hsp = head sp
        tsp = tail sp

-- | Open a directory
candyOpenDirectory :: DBIORef -> FilePath -> IO Errno
candyOpenDirectory dbio fp
    | sp == [] = return eOK
    | hsp == revPrefix = revOpenDirectory dbio tsp
    | hsp == trPrefix = trOpenDirectory dbio tsp
    | otherwise = return eNOENT
    where
        sp = splitPath fp
        hsp = head sp
        tsp = tail sp

-- | Read a directory
candyReadDirectory :: DBIORef -> FilePath -> IO (Either Errno [(FilePath, FileStat)])
candyReadDirectory dbio fp
    | sp == [] = do
            ctx <- getFuseContext
            return $ Right [
                ("rev", dirStat ctx),
                ("tr", dirStat ctx)
                ]
    | hsp == revPrefix = do
            revs <- revReadDirectory dbio tsp
            --return (prependPaths revPrefix <$> revs)
            return revs
    | hsp == trPrefix = do
            tr <- trReadDirectory dbio tsp
            --return (prependPaths trPrefix <$> tr)
            return tr
    | otherwise = do
        return (Left eNOENT)
    where
        sp = splitPath fp
        hsp = head sp
        tsp = tail sp

-- Open a file (at a fixed point in history)
candyOpen :: DBIORef -> FilePath -> OpenMode -> OpenFileFlags -> IO (Either Errno CandyFileHandle)
candyOpen dbio fp omode oflags
    | first == revPrefix = revOpen dbio other omode oflags
    | first == trPrefix = trOpen dbio other omode oflags
    | otherwise = return (Left eNOENT)
    where
        sp@(first:other) = splitPath fp

-- Read from a file
candyRead :: DBIORef -> FilePath -> CandyFileHandle -> ByteCount -> FileOffset -> IO (Either Errno B.ByteString)
candyRead dbio fp fhandle count offset
    | first == revPrefix = revRead dbio other fhandle count offset 
    | first == trPrefix = trRead dbio other fhandle count offset
    | otherwise = return (Left eNOENT)
    where
        sp@(first:other) = splitPath fp

-- Get stats from the file system
candyGetFileSystemStats :: DBIORef -> String -> IO (Either Errno FileSystemStats)
candyGetFileSystemStats dbio str =
  return $ Right  FileSystemStats
    { fsStatBlockSize = 512
    , fsStatBlockCount = 1
    , fsStatBlocksFree = 1
    , fsStatBlocksAvailable = 1
    , fsStatFileCount = 5
    , fsStatFilesFree = 10
    , fsStatMaxNameLength = 255
    }

dirStat ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }
