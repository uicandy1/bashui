# candyfs
(ideas)

The whole idea of this is to have a simple way
of splitting an interactive program into multiple
commands (shell commands) while maintaining its
state in memory. The state is exposed as a file system.
The underlying db is immutable and supports transacted
operations.

## File system structure

- You can access the data of all revisions
```
/rev
    latest/
    ...
    ...
    ...
/tr
    .tr
    .trlock
    .commit
    .rollback
    /pending
        -- you can write in this to
        -- query things (?)
        .query
 
```

## Transactions 

- You can create a transaction by reading from .tr or .trlock
- You can commit it by writing (its name)) to commit
- You can roll it back by writing its name to rollback
- While it exists, you can access the data by name under tr

## Queries

- .query would be another +/- magic thing that would let 
you run complex queries

## Notifications
- the file system can fire notifications through dbus
or any existing notification mechanism

## Raw Ideas
- you can probably make the caller block to create
a blocking transaction (like there will only be
one of these at the same time, to the creation
will fail if there is already one and potentially
even block for a while)

- could use dbus or whatever to signal changes
so that no special communication mechanism is needed.

## Why
- addressing space means file system addressing (i.e. path)
- atomic operations are inherently supported
- will be a more natural interface for shell scripts

