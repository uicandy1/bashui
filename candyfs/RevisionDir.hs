{-|
Module      : RevisionDir
Description : Listing of transactions
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The state db triggers notifications (when the db
changes)
-}
module RevisionDir (
    revGetFileStat,
    revOpenDirectory,
    revReadDirectory, 
    revOpen,
    revRead 
    ) where
import Data.ByteString.Base64(encode)
import Data.IORef(readIORef)
import Data.List(find)
import DbDir
import MDb(dbHash)
import System.Fuse
import System.Posix.Files
import System.Posix.Types
import Server.Types(UiDb, DBIORef, UndoableJournal(..), DbKey(..), DbValue(..))
import Transaction.Transaction(JournalEntry(..), Journal(..), latestDb)
import Types
import qualified Data.ByteString.Char8 as B

-- | JournalEntry stat : a journal entry is essentially a db
journalEntryStat (JournalEntry db description) ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }
-- | latest db from a DBIORef
ldb :: DBIORef -> IO UiDb
ldb dbio = do
    (UndoableJournal j1 j2) <- readIORef dbio
    return $ latestDb j1

dirStat ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }

-- | Journal entry to file name
journalEntryToFilePath :: JournalEntry k v -> FilePath
journalEntryToFilePath (JournalEntry db comment) = (\b -> if b=='/' then '_' else b) <$> (B.unpack $ encode (dbHash db))


-- | Find a journal entry based on its stringified hash 
findJournalEntry :: DBIORef -> String -> IO (Either Errno (JournalEntry DbKey DbValue))
findJournalEntry dbio hash = do
    (UndoableJournal (Journal entries) _) <- readIORef dbio
    let maybeJE = find (\je -> journalEntryToFilePath je == hash) entries
    return $ maybe (Left eNOENT) Right maybeJE

-- | Reads the transaction directory
--   Note: the path that we return from here needs to be relative to here
readRevisionDirectory :: DBIORef -> IO (Either Errno [(FilePath, FileStat)])
readRevisionDirectory dbio = do
    ctx <- getFuseContext
    (UndoableJournal (Journal entries) _) <- readIORef dbio
    return $ Right ((\e-> (journalEntryToFilePath e, journalEntryStat e ctx)) <$> entries)

revGetFileStat :: DBIORef -> [String] -> IO (Either Errno FileStat)
revGetFileStat _ [] = do
    ctx <- getFuseContext
    return $ Right (dirStat ctx)
revGetFileStat dbio [rev] = do
    ctx <- getFuseContext
    journalEntry <- findJournalEntry dbio rev
    return ((const $ dirStat ctx) <$> journalEntry)
revGetFileStat dbio (rev:other) = do
    journalEntry <- findJournalEntry dbio rev
    either
        (return . Left)
        (\(JournalEntry db _) ->  dbGetFileStat db other)
        journalEntry
revGetFileStat _ _ = return (Left eNOENT)

revOpenDirectory :: DBIORef -> [String] -> IO Errno
revOpenDirectory _ [] = return eOK
revOpenDirectory dbio [rev] = do 
    journalEntry <- findJournalEntry dbio rev
    return $ either (const eACCES) (const eOK) journalEntry 
revOpenDirectory dbio (rev:other) = do 
    journalEntry <- findJournalEntry dbio rev
    either
        (const $ return eACCES)
        (\(JournalEntry db _) -> dbOpenDirectory db other)
        journalEntry
revOpenDirectory _ _ = return eNOENT

revReadDirectory :: DBIORef -> [String] -> IO (Either Errno [(FilePath, FileStat)])
revReadDirectory dbio [] = readRevisionDirectory dbio 
revReadDirectory dbio (rev:other) = do
    journalEntry <- findJournalEntry dbio rev
    either
        (return . Left)
        (\(JournalEntry db _) -> dbReadDirectory db other)
        journalEntry

revReadDirectory _ _ = return (Left eNOENT)

revOpen :: DBIORef -> [String] -> OpenMode -> OpenFileFlags -> IO (Either Errno CandyFileHandle)
revOpen dbio (rev:other) omode oflags = do
    journalEntry <- findJournalEntry dbio rev
    either
        (return . Left)
        (\(JournalEntry db _) -> do 
            fh <- dbOpen db other omode oflags
            return (DbFH <$> fh)
            )
        journalEntry
revOpen _ _ _ _ = return (Left eNOENT)

revRead :: DBIORef -> [String] -> CandyFileHandle -> ByteCount -> FileOffset -> IO (Either Errno B.ByteString)
revRead dbio (rev: other) (DbFH handle) count offset = do
    journalEntry <- findJournalEntry dbio rev
    either
        (return . Left)
        (\(JournalEntry db _) -> dbRead db other handle count offset)
        journalEntry
revRead _ _ _ _ _ = return (Left eNOENT)

