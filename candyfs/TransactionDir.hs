{-|
Module      : TransactionDir
Description : Listing of transactions
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The state db triggers notifications (when the db
changes)
-}
module TransactionDir (
    trGetFileStat,
    trOpenDirectory,
    trReadDirectory, 
    trOpen,
    trRead 
    ) where
import Data.IORef(readIORef)
import Data.Map.Strict as M
import System.Fuse
import System.Posix.Files
import System.Posix.Types
import Server.Types(UiDb, DBIORef, UndoableJournal(..))
import Transaction.Transaction(JournalEntry(..), latestDb)
import Types
import qualified Data.ByteString.Char8 as B

-- | JournalEntry stat : a journal entry is essentially a db
journalEntryStat (JournalEntry db description) ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }
-- | latest db from a DBIORef
ldb :: DBIORef -> IO UiDb
ldb dbio = do
    (UndoableJournal j1 j2) <- readIORef dbio
    return $ latestDb j1

-- | Journal entry to file name
journalEntryToFilePath :: JournalEntry k v -> FilePath
journalEntryToFilePath entry = "blah"

-- | Reads the transaction directory
--   Note: the path that we return from here needs to be relative to here
{- readTransactionDirectory :: DBIORef -> FilePath -> IO (Either Errno [(FilePath, FileStat)])
readTransactionDirectory dbio _ = do
    ctx <- getFuseContext
    (UndoableJournal (Journal entries) _) <- readIORef dbio
    return $ Right (\e-> (journalEntryToFilePath e, journalEntryStat e ctx)) <$> entries
-}

dirStat ctx = FileStat { 
        statEntryType = Directory
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }
fileStat ctx = FileStat { 
        statEntryType = RegularFile
        , statFileMode = foldr1 unionFileModes
              [ ownerReadMode
              , ownerExecuteMode
              , groupReadMode
              , groupExecuteMode
              , otherReadMode
              , otherExecuteMode
              ]
        , statLinkCount = 2
        , statFileOwner = fuseCtxUserID ctx
        , statFileGroup = fuseCtxGroupID ctx
        , statSpecialDeviceID = 0
        , statFileSize = 4096 -- FIXME
        , statBlocks = 1
        , statAccessTime = 0
        , statModificationTime = 0
        , statStatusChangeTime = 0
    }

data SpecialFile =
    SpecialFile String

specialFiles = [
    SpecialFile "tr",
    SpecialFile "trlock",
    SpecialFile "commit",
    SpecialFile "rollback" ]

specialFilesMap = M.fromList ((\sf@(SpecialFile name) -> (name, sf)) <$> specialFiles)

-- | 
trGetFileStat :: DBIORef -> [String] -> IO (Either Errno FileStat)
trGetFileStat _ [] = do
    ctx <- getFuseContext
    return $ Right (dirStat ctx)
trGetFileStat _ [f] = do
    ctx <- getFuseContext
    let maybeF = M.lookup f specialFilesMap
    return $ maybe (Left eACCES) (\f -> Right $ fileStat ctx) maybeF
trGetFileStat _ _ = return (Left eNOENT)

trOpenDirectory :: DBIORef -> [String] -> IO Errno
trOpenDirectory _ [] = return eOK
trOpenDirectory _ _ = return eNOENT

trReadDirectory :: DBIORef -> [String] -> IO (Either Errno [(FilePath, FileStat)])
trReadDirectory _ [] = do
    ctx <- getFuseContext
    return $ Right ((\(SpecialFile name) -> (name, fileStat ctx)) <$> specialFiles)
trReadDirectory _ _ = return (Left eNOENT)

trOpen :: DBIORef -> [String] -> OpenMode -> OpenFileFlags -> IO (Either Errno CandyFileHandle)
trOpen _ _ _ _ = return (Left eNOENT)

trRead :: DBIORef -> [String] -> CandyFileHandle -> ByteCount -> FileOffset -> IO (Either Errno B.ByteString)
trRead _ _ _ _ _ = return (Left eNOENT)

--openTransaction :: DBIORef -> CandyFileHandle 
