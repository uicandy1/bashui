{-|
Module      : Types
Description : Define types for candyfs
Copyright   : (c) Hugo Windisch, 2020
License     : BSD3
Stability   : experimental

Defines the types that we use for the candy file system
-}
module Types (
    CandyFileHandle(..)
    ) where

import Data.Map.Strict as M
import DbDir
import Transaction.Transaction(TransactionState, Journal(..))
import Server.Types(DbKey, DbValue)

{-
Hour file handles can be different things depending on the
type of file that is opened.
-}
data CandyFileHandle = 
    -- File handle for reading db files, maybe this
    -- should be removed for a StringFH
    DbFH DbFileHandle |
    -- String file handle. When the response is read only we can
    -- just put it there and that's it (computed when opening the file,
    -- than can be read... lazyness of this...)
    StringFH String 

-- | Maps a transaction name (like a filename) with a transaction state
data TransactionMap = TransactionMap (M.Map String (TransactionState DbKey DbValue))

data CandyIORef = CandyIORef (Journal DbKey DbValue) TransactionMap

