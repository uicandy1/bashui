
# candy command documentation

```
candy - a connection to a ui or state database

Usage: candy (-s|--socketPath SOCKET) [-t|--transactionSocket TSOCKET]
             [-n|--update ARG] [-l|--loglevel ARG]
             [-c|--transactionComment COMMENT] [-x|--transacted]
  execute queries on ui or state database in interactive (REPL) or non
  iteractive mode

Available options:
  -s,--socketPath SOCKET   Unix domain socket path to SOCKET
  -t,--transactionSocket TSOCKET
                           Unix domain socket path for transactions to TSOCKET
  -n,--update ARG          Command to run on update
  -l,--loglevel ARG        Log level (DEBUG, INFO, NOTICE, WARNING, ERROR,
                           CRITICAL, ALERT, EMERGENCY
  -c,--transactionComment COMMENT
                           Transaction comment as COMMENT
  -x,--transacted          transacted
  -h,--help                Show this help text

To connect interactively (REPL mode) to a ui server and interactively inspect the history and state of the ui:

    candy -s /tmp/tmp.wlhbvCTDPr/ui.ipc 


To pipe a request to a ui server (non interactive request): 

    echo 'getj .' | candy -s /tmp/tmp.wlhbvCTDPr/ui.ipc -- 


Supported Queries:

appendlines  path cols sep ...   append the provided lines at path
at           partialhash comment start a transaction at a partialhash
commit                           commit the current transaction
count        path                count the children at path
del          path                remove the selected key from the database at path
get          path                return the value of a given key as a string
geth         path                return the value of a given key as Haskell read format
getj         path                return the value of a given key as json
help                             show help information
history                          display the change history of the db
lines        path columns sep    set lines at path
patch        path sep ...        sync under path with the provided changes
prependlines path columns sep    add some lines before the existing line at path
quit                             quit the application
redo                             redo (the whole history) one step
rollback                         rollback the current transaction
select       fields from path    select fields from path
transact     comment             start a transaction at the tip
undo                             undo (the whole history) one step

```
# Setting a string

`string key value` can be used to assign a value to a key in the db. Key can contain
dots to access a nested value. Value can be quoted.

### code
```
string abc 123
```

# Reading as a string

`get key` can be used to read the value of a key. Key dan contain dots to access
a nested value.

### code
```
get abc
```

### result

```
123
```

# History of changes

`history` will display a list of changes in the current db. The purpose of this is
that it is possible to look at any value in the db at any point in the history.

### code
```
history
```

### result

```
9DhEyQjvGeKaoBAmX+KpD6gTI1k= unnamed
Bf5AV1MWbxJVWefJrFWGVPEHx+k= initial db

```

# Setting multiple lines

It is possible to have ordered lists of items in the database. These will use a special
key scheme: #1, #2, .. #n.
The `lines` command can be used to initialize a sequencial list of items.

### code
```
lines thelines thetext ___
"first line"
"second line"
"third line"
"fourth \" line"
___
```

# Appending lines to a list

It is possible to append lines to a list of items.

### code
```
appendlines thelines thetext ___
"one more"
"and the last one"
___
```

# Prepending lines to a list

It is possible to prepend lines to a list of items.

### code
```
prependlines thelines thetext ___
"real first one"
"real second one"
___
```

# Counting the lines in a list

The `count` query returns the number of lines in a list

### code
```
count thelines
```

### result

```
[[IntValue 8]]
```

# Multiple fields per line

All line commands (`lines`, `appendlines`, `prependlines`) allow the insertion
of multiple fields per line.

### code
```
lines thecolumns firstfield,secondfield  ___
"line1 field 1" "line1 field 2"
"line2 field 1" "line2 field 2"
___
```

Now if we try `get thecolumns` we will get:

### result

```
line1 field 1 line1 field 2
line2 field 1 line2 field 2
```

For structured content get is no very convenient but `geth` and
`getj` can be used. 

`getj thecolumns` returns structured data formatted as json

### result

```
[
  {
    "firstfield": "line1 field 1",
    "secondfield": "line1 field 2"
  },
  {
    "firstfield": "line2 field 1",
    "secondfield": "line2 field 2"
  }
]
```

`geth thecolumns` returns structured data formatted with the Haskell read format

### result

```
ResultDb (Just (Tree (fromList [(#0,Tree (fromList [("firstfield",Leaf (StringValue "line1 field 1")),("secondfield",Leaf (StringValue "line1 field 2"))])),(#1,Tree (fromList [("firstfield",Leaf (StringValue "line2 field 1")),("secondfield",Leaf (StringValue "line2 field 2"))]))])))
```

