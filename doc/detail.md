# Command Reference

uicandy essentially consists of 3 commands: `ui`, `state` and `candy`

## ui: the user interface server

#### one ui for all applications

`ui` is the program that shows the user interface. 
Whenever a gui application is started, an
instance of ui is launched to show its user interface.

#### uses the gui toolkit of the plaform

ui uses the gui toolkit of the platform to show the user interface but
at this time only gtk is supported.

#### acts as an in-memory database

To change what is displayed in ui, applications perform queries
on the database. Queries are executed in transactions that
atomically pass or fail. The database is immutable, meaning that all
previous states are kept but the history can be limited to n steps.
A REPL allows to inspect past and present data in the database.

#### uses declarative bindings to connect ui elements to the data

UI layout is created with a UI editor (currently only gtk is supported
so this tool is glade). The ui widgets are populated using data from
the database, and the way the data is used is determined by a set
of static bindings. For example, bindings can be used to populate
a list view saying use this list from the database, using this field
of list elements for the first column, this other field for the second
column and this other field as the command to run when a line is
double clicked. The list view is automatically updated whenever
the list from the database changes.

Complete documentation of the `ui` program can be found [here](/doc/ui.md).

## state: the state server

`state` keeps program state. Its use is optional,
It exposes the same transacted immutable database as `ui` letting
application use it to store their internal state (the state could
be stored in the file system, or in another database, or by
the app itself if it runs as a server,
but `state` allows to write the appliation as stateless
command line programs, while benefitting from a transacted database,
immutable data, the same shell friendly queries that are used by
the ui part, and data inspection tools).

Complete documentation of the `state` program can be found [here](/doc/state.md).

### candy: the client

`candy` allows a program to communicate with `ui` and `state`. It
can act as a communication mechanism or as an interactive read
eval print loop.

Complete documentation of the `candy` program can be found [here](/doc/candy.md).

