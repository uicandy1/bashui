Threads are multiple lines of thought operating at the same time in the
same set of memories, multiple little voices speaking at the same time
in the same brain. Through hard work and care for detail, programmers manage to
make this stuff work. But in real life, a situation where multiple voices constantly
talk inside the same head is called severe mental illness.

In contrast processes are like independent little dudes. Each one has his
own set of memories and thoughts, his own goal and his own view of the world. 
Each one can accomplish his own little thing in his own little corner,
or alternately, communicate with friends to cooperate on bigger projects. 
Sometimes they communicate in
their specialized jargon, but they really prefer plain
text: it makes things so much simpler for everyone. In geek talk, this
arrangement is called the UNIX philosophy. In real life talk it is called team work.

Of these two arrangements, GUI applications follow the first. No matter what
plaform they target and what GUI library they use, they are big programs
that do everything in a single process, as a single unsplittable thing with a single 
set of memories shared by everything. And when they're 
able to do two things at the same time, or multiple things in the background, 
it's generally because they use threads. And if they allow any form
of scripting (using the program features in a programmatic way independently
of the GUI) it's
because they integrate a scripting language (adding even
more code to their monolithic structure, making things bigger and 
more complex).

The other model is used mainly by command line interfaces and some cloud architectures (
stateless REST services
share a lot of ideas with the unix philosophy, and are widely used in the cloud). Multiple
isolated and independent programs work as a team, coordinating their work
by communicating in plain text. This model works very well for non interactive
administrative tasks, and in so called "back end" programs. It is used abundantly 
in all sorts of scenarios (installation, background processing, machine startup etc) 
but practically never for interactive GUI work.

The goal of this project is to enable the use of the "multiple little dudes"
model for the development of interactive GUI applications. It is nice
that casual users don't have to learn command line interfaces and
shell scripting to do their work. It is nice that they get a visual
and easy to learn interface to the various functions of a system. But
if this system is composed of independently usable commands, that can be
scripted by the tools provided by the os (shells), that share information
by communicating, that externalize any state to the file system, that can
individually fail without jeopardizing the integrity of the whole, that
naturally benefit from the access rights mechanisms provided by the os, that run in
mulitple processes concurrently, naturally and transparently using available cores and
even available hosts, it's even better. Can GUI programs operate this
way? Can they be easy to write and debug and can they perform well enough
when implemented like this?

We want a convenient way to break a GUI program into a composition of small 
independent programs.  As a side quest, we also want user interfaces to be designed visually
(with an interface designer application) and loosely coupled to the
implementation. It should be easy for designers to provide a mock
implementation of the program, and for programmers to work work on the
real implementation without caring too much about the graphical interface. 

Programmers own the program, designers own the user interface, the binding between the 
two is expressed in shell scripting (the text user interface of the os) and 
remains under the control of the user.  The casual user does not see any difference. 
The advanced user owns the composition of interactive programs.

The scope of the project is experimentation: finding a way to assemble GUI
programs with shell scripting, improving
it until it becomes convenient, and finding the limits of this strategy
(what kinds of program work well like this and what other kinds are better done
differently). Providing a production ready solution is not an immediate concern. 
