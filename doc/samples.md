# Samples

Here is a quick description of the various samples.

## Mainstream

These samples are good illustrations of what uicandy is meant
to be,

### hangman

![Hangman](hangman.png)

> source directory: [/samples/hangman](/samples/hangman)

Hangman is a hangman solver. The main application (the backend) is grep. 
The application consists in tacking a ui on top
of grep to turn it into an easy to understand single purpose tool. 
This is the quintessential 
uicandy application: an easy to create ui on top of a non interactive
command line program.


### notes

![Notes](notes.png)

> source directory: [/samples/notes](/samples/notes)

Notes is a small note taking application (the hello world of gui frameworks).
A state db keeps the notes, and the ui db is updated in reaction to changes in
the state db.


### fm2

![Filemanager](fileman.png)

> source directory: [/samples/fm2](/samples/fm2)

As a filemanager fm2 is still a bit simple and limited. 
As an experiment, it explores the following question:
Why would someone implement the core functions of a file manager  
when `ls`, `find`, `cd`, `file` etc. are already available? 
Why not use these already available building blocks?

The typical "single executable" design of a file manager, that 
could easily be seen from a user's perspective as a small
program that does one thing well, is drastically monolithic from
a traditional UNIX perspective: you need a big arsenal of shell commands
like `ls` and `find` to cover all its functions.

## Experimental

The examples in this section are either testing tools or
explorations of uses cases that are challenging (potentially
not that much of a good fit).

### widgets

![Widgets](widgets.png)

> source directory: [/samples/widgets](/samples/widgets)

We need a small laboratory to quickly test various ui bindings,
and this is whay widgets does. It's not a useful application but
rather a quick way to test bindings.

### capture

> source directory: [/samples/capture](/samples/capture)

Capture is a work in progress on the idea of handling mouse moves
by streaming mouse positions to a single process (instead of
launching a new process for every new position).

When the mouse is clicked a process is started, and all mouse
moves are streamed to that process until the mouse is released.

### visedit

![visedit](/doc/visedit.png)

> source directory: [/samples/visedit](/samples/visedit)

Visedit is a quick experimentation on the idea of a visual editor (something
like a paint program).
Could a visual editor be implemented with uicandy? This is the question
that visedit may eventually answer.

## Other

This section contains samples that are either old or not really
useful at this moment.

### fileman

> source directory: [/samples/fileman](/samples/fileman)

Fileman is an older version of fm2 that stores in state in the filesystem
instead of a db. Ideally, all persistent things should be accessed through
the file system. Not there yet.

### ui2

> source directory: [/samples/ui2](/samples/ui2)

Ui2 is a future thing.
