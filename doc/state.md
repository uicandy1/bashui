# state commmand documentation

```
state - an in memory database for keeping the state of interactive applications

Usage: state (-s|--socket SOCKET) (-n|--notifications ARG) [-d|--db ARG]
             [-l|--loglevel ARG]
  Start the state database server

Available options:
  -s,--socket SOCKET       Unix domain socket path
  -n,--notifications ARG   Notification settings path
  -d,--db ARG              Initial DB path
  -l,--loglevel ARG        Log level (DEBUG, INFO, NOTICE, WARNING, ERROR,
                           CRITICAL, ALERT, EMERGENCY
  -h,--help                Show this help text

Waits for connections on a unix domain socket. Each connection acts as 
an atomic transaction (all queries performed during this connection 
will either succeed or fail as whole). The database uses immutable data: 
the full history of changes is kept and every change creates a new version 
of the data. The version history can be retrieved. Previous versions can be 
inspected.
The candy program can be used to send request to the state program.
See also: candy
```
