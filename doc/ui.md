# ui commmand documentation

This documentation is still pretty imcomplete:
- missing binding documentation
- missing database format documentation
- bunch of stuff missing...

```
ui - user interface server

Usage: ui [-l|--loglevel ARG] SETTINGSFILE [-f|--forkevents]
  Start the ui server

Available options:
  -l,--loglevel ARG        Log level (DEBUG, INFO, NOTICE, WARNING, ERROR,
                           CRITICAL, ALERT, EMERGENCY
  -f,--forkevents          Whether to fork event handlers
  -h,--help                Show this help text

The ui server shows the window of an application. The content of the 
widgets is taken from an in memory database. Declarative bindings tells ui 
how to use the data (for example, it is possible for many buttons to use 
the same label, and in this case if the text changes at one place in the database 
changes are observed at many places in the ui). 
The ui server waits for connections on a unix domain socket. Each connection acts as 
an atomic transaction (all queries performed during this connection 
will either succeed or fail as whole). The database uses immutable data: 
the full history of changes is kept and every change creates a new version 
of the data. The version history can be retrieved. Previous versions can be 
inspected.


The candy program can be used to send request to the state program. 
Settings File

The settings file has the following structure:
Settings {
    root = see below,
    db = see below,
    socketPath = "/path/to/unixdomainsocket"
}

root should be a binding (the topmost binding is typically a group).
the possible bindings are:
AppWindow WidgetId
    ex: AppWindow "ID_MAIN"
    Set the widget id of the main window of the application
ApplicationReady DataReference
    ex: ApplicationReady (Literal (StringValue "notify-send hello"))
    A program to execute when the application is ready (the ui server is ready to receive requests)
ButtonActivated WidgetId DataReference 
    ex: ButtonActivated "ID_BUTTON" (Literal (StringValue "notify-send button-activated"))
    Make 'notify-send button-activated' be run when the ID_BUTTON button is clicked.
ButtonText WidgetId DataReference
    ex: ButtonText "ID_BUTTON1" (Reference ["label"])
    Use the string with the 'label' key the database as the button label.
ComboBoxModel WidgetId DataReference [Column] 
    ex: ComboBoxModel "ID_COMBO1" (Reference ["comboitems"]) [Column "Title" (Reference ["thefield"]) Text]
    Populate the ID_COMBO1 combo box with the combiitems list, using the thefiled field as a text source
ComboBoxSelectionChanged WidgetId DataReference DataReference DataReference 
    ex: ComboBoxSelectionChanged "ID_COMBO" (Reference ["comboitems"]) (Literal (StringValue "notify 'combo: '")) (Reference ["thestring"])
    Call 'notify combo: ' when selection changes
ComboBoxSelectionCleared WidgetId DataReference
    ex: ComboBoxSelectionCleared "ID_COMBO" (Reference ["command"]) 
    Run program referenced by command when the selection from ID_COMBO is cleared
 ... to be continued ...

See also: candy
```
