{-|
Module      : Diff
Description : Compute the difference between two dbs
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Compute the diff between two dbs (determine the operations needed
to make them the same)
-}
module Diff(
    DbDiff(..),
    diffTree
    ) where
import MDb
import Data.Binary (Binary)
import Data.Map.Strict as Map

-- | differences between two trees
data DbDiff k v = Create k v | Update k v | Delete k
    deriving (Show, Read)

-- | Compute the diff between two trees.
diffTree :: (Ord k, Binary k, Binary v) => Db k v -> Db k v -> [DbDiff k (Db k v)]
diffTree (Tree fromHash fromM) (Tree toHash toM)
    | fromHash == toHash = []
    | otherwise = concat [added, deletedAndUpdated]
    where
        deletedAndUpdated = foldlWithKey 
            (\ diffs k v -> maybe 
                    (Delete k: diffs)
                    (\nv -> if dbHash nv == dbHash v then diffs else (Update k nv : diffs))
                    (Map.lookup k toM)
                )
            [] fromM 
        added = foldlWithKey 
            (\ diffs k v -> maybe 
                    (Create k v: diffs)
                    (const diffs)
                    (Map.lookup k fromM)
                )
            [] toM
