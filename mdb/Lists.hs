{-|
Module      : Lists
Description : Trees as lists
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Support for using numerical keys to create pseudo lists
from dicts.
-}
module Lists(
    NumericalKey(..),
    lappend,
    lprepend,
    lreindex
    ) where

import MDb
import Data.Binary (Binary)
import Data.Map.Lazy
import Data.Maybe (fromMaybe)
import Data.Map.Strict as Map

-- | Numerical keys can be converted to and from ints
class NumericalKey a where
    toInt :: a -> Maybe Int
    fromInt :: Int -> a

-- | Convert to an Int always
toIntOrZero :: (NumericalKey k) => k -> Int
toIntOrZero k =
    fromMaybe 0 $ toInt k

-- | Find the maximum numerical key in a db
maxNumKey :: (NumericalKey k) => Db k v -> Int
maxNumKey (Leaf _ _) = 0
maxNumKey (Tree _ m) = 
    foldrWithKey (\ k _ acc -> toIntOrZero k + acc) 0 m

-- | Set a value at a given index
iset :: (Ord k, NumericalKey k, Binary k, Binary v) => Int -> Db k v -> Db k v -> Either DbError (Db k v)
iset n =
    set (fromInt n) 

-- | Append items to a list
lappend :: (NumericalKey k, Ord k, Binary k, Binary v) => [Db k v] -> Db k v -> Either DbError (Db k v)
lappend _ (Leaf _ _) = Left IndexingALeaf
lappend items db@(Tree _ m) = 
    Right (MDb.fromDbList $ zip allKeys (prevItems ++ items) )
    where
        prevItems = snd <$> Map.toList m
        allKeys = fromInt <$> [0..]

-- | Prepend items to a list (will shift the whole list and compact it)
lprepend :: (NumericalKey k, Ord k, Binary k, Binary v) => [Db k v] -> Db k v -> Either DbError (Db k v)
lprepend _ (Leaf _ _) = Left IndexingALeaf 
lprepend items db@(Tree _ m) =
    Right (MDb.fromDbList $ zip allKeys (items ++ prevItems) )
    where
        prevItems = snd <$> Map.toList m
        allKeys = fromInt <$> [0..]

-- | Reindex
lreindex :: (NumericalKey k, Ord k, Binary k, Binary v) => Db k v -> Either DbError (Db k v)
lreindex = lprepend []
