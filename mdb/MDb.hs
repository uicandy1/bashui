{-|
Module      : MDb
Description : The database
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Database manipulations.
-}
module MDb(
keysHash, 
        Db (..),
        DbHash (..),
        isTree,
        isLeaf,
        dbLeaf,
        dbHash,
        set,
        MDb.empty,
        MDb.fromList,
        MDb.fromDbList,
        MDb.delete,
        MDb.lookup,
        deepLookup,
        deepLookupValue,
        deepSetValue,
        deepSet,
        deepDelete,
        deepDiff,
        dbAsDbList,
        dbAsKeyDbList,
        DbError(..),
        deepLookupHash,
        keyChanged,
        someKeyChanged,
        selectRowColumns,
        justSelectRowColumns,
        select,
        justSelect,
        numKeys
    ) where

import Data.Map.Strict as Map
import Data.Maybe (isNothing, fromMaybe, isJust)
import GHC.Exts (sortWith)
import Data.List (find)
import Data.Binary (Binary, encode)
import Crypto.Hash.SHA1
import Data.ByteString (ByteString(..))
import Data.ByteString.Lazy (toStrict)
import Text.ParserCombinators.ReadPrec 
import Text.Read

-- | Hash of db values
type DbHash = ByteString

-- | Database definition
data Db key value = 
    Leaf DbHash value |
    Tree DbHash (Map key (Db key value))
        deriving (Eq)

instance (Read k, Read v, Binary v, Binary k, Ord k, Binary k) => Read (Db k v) where
    readListPrec = readListPrecDefault
    readPrec = parens $ prec app_prec ( do
                                Ident "Leaf" <- lexP
                                v <- step readPrec
                                return $ dbLeaf v)
                        +++ prec app_prec (do
                                Ident "Tree" <- lexP
                                vm <- step readPrec
                                return $ mapToTree vm)
                    where app_prec = 10

instance (Show k, Show v) => Show (Db k v) where
    showsPrec d (Leaf _ v) = showParen (d > app_prec) $
        showString "Leaf " . showsPrec (app_prec+1) v
        where app_prec = 10
    showsPrec d (Tree _ m) = showParen (d > app_prec) $
        showString "Tree " . showsPrec (app_prec+1) m
        where app_prec = 10

-- | Database errors
data DbError =
    IndexingALeaf |
    InvalidIndex  |
    NotALeaf |
    NotATree
        deriving (Read, Show)

-- | Is the Db a tree
isTree :: Db k v -> Bool
isTree (Tree _ _) = True
isTree _ = False

-- | Is the Db a Leaf 
isLeaf :: Db k v -> Bool
isLeaf  = not . isTree

-- | Construct a leaf
dbLeaf :: (Binary v) => v -> Db k v
dbLeaf v = Leaf (valueHash v) v 

-- | Compute the hash of a db
dbHash :: Db k v -> DbHash
dbHash (Leaf h _) = h
dbHash (Tree h _) = h

-- | Hash of a db value
valueHash :: (Binary v) => v -> DbHash
valueHash = hash . toStrict . encode

-- | Hash of a list of keys
keysHash :: (Binary k) => [(k, Db k v)] -> DbHash
keysHash ks =
    valueHash $ Prelude.map (\(k, db) -> (k, dbHash db)) ks

-- | Construct a tree node from a map
mapToTree :: (Binary k, Binary v) => Map k (Db k v) -> Db k v
mapToTree m = Tree (keysHash (toAscList m)) m

-- | Construct an empty Tree
empty :: (Ord k, Binary k) => Db k v
empty =
    Tree (valueHash e) (Map.empty :: Map k (Db k v)) 
    where 
        -- this is a bit of a hack: what is the hash of an empty array
        e :: [String]
        e = [] 

-- | Construct a Tree from a list of key Db paris
fromDbList :: (Ord k, Binary k, Binary v) => [(k, Db k v)] -> Db k v
fromDbList dbl =
    mapToTree $ Map.fromList dbl

-- | Construct a Tree from a list of key value pairs
fromList :: (Ord k, Binary k, Binary v) => [(k, v)] -> Db k v
fromList pairs =
    fromDbList dbPairs
    where
        dbPairs = Prelude.map (\(k, v) -> (k, Leaf (valueHash v) v)) pairs

-- | Set a db at a given key in a db
set :: (Ord k, Binary k, Binary v) => k -> Db k v -> Db k v -> Either DbError (Db k v)
set key value (Tree n m) = 
    Right $ mapToTree $ insert key value m

set _ _ (Leaf _ _) =
    Left IndexingALeaf
 
-- | Delete a given key from a db
delete :: (Ord k, Binary k, Binary v) => k -> Db k v -> Db k v
delete key (Tree n m) =
    Tree newhash newmap
    where
        newmap = Map.delete key m
        newhash = keysHash $ toAscList newmap

-- | Lookup a database at a given key in a database
lookup :: (Ord k) => k -> Db k v -> Maybe (Db k v)
lookup key (Tree _ m) =
    Map.lookup key m
lookup _ _ = Nothing

-- | Lookup a database at a key list (deep nesting) in a database
deepLookup :: (Ord k) => [k] -> Db k v -> Maybe (Db k v)
deepLookup [] db = Just db
deepLookup (key:others) db =
    MDb.lookup key db >>= deepLookup others

-- | Lookup a value at a key list (deep nesting) in a database.
-- if a Tree is found Nothing is returned (i.e. must be a Leaf).
deepLookupValue :: (Ord k) => [k] -> Db k v -> Maybe v
deepLookupValue keys db = do
    found <- deepLookup keys db
    case found of
        Leaf _ v -> Just v
        _ -> Nothing

-- | Set a value at a key list (deeply nested)
deepSetValue :: (Ord k, Binary k, Binary v) => [k] -> v -> Db k v -> Either DbError (Db k v)
deepSetValue k v = deepSet k (Leaf (valueHash v) v)

-- | Set a db at a key list (deeply nested)
deepSet :: (Ord k, Binary k, Binary v) => [k] -> Db k v -> Db k v -> Either DbError (Db k v)
deepSet [] n _  =
    Right n
deepSet _ _ (Leaf _ _) =
    Left IndexingALeaf -- SHOULD WORK!, CAN WORK!, MAKE IT WORK!
deepSet [k] v (Tree _ m) = 
    Right $ Tree newhash newmap
    where
        newmap = Map.insert k v m
        newhash = keysHash $ toAscList newmap
deepSet (k:r) v db@(Tree n m) = 
    newtree
    where
        sub = fromMaybe MDb.empty (MDb.lookup k db)
        subm = case sub of 
            Tree _ m -> m
            _ -> Map.empty
        newtree = do
            updated <- deepSet r v (Tree n subm)
            case updated of 
                updatedSub@(Tree updatedN _) -> do
                    let newmap = Map.insert k updatedSub m
                    let newhash = keysHash $ toAscList newmap
                    Right $ Tree newhash newmap
                _ -> Left NotATree


-- | Delete at a deep key (list of keys)
deepDelete :: (Ord k, Binary k, Binary v) => [k] -> Db k v -> Db k v
deepDelete [] db = db
deepDelete _ db@(Leaf _ _) = db
deepDelete [k] db = MDb.delete k db
deepDelete (k:r) db@(Tree _ m) =
    maybe db (\subdb -> mapToTree $ Map.insert k (deepDelete r subdb) m) (Map.lookup k m)

-- | finds all the diff paths and new values between 2 dbs
-- (stopping where trees diverge)
deepDiffImpl :: (Ord k) => [k] -> Maybe (Db k v) -> Maybe (Db k v) -> [([k], Maybe (Db k v))]
deepDiffImpl k (Just (Tree n db)) m2@(Just (Tree n2 db2))
    | n == n2 = []
    | otherwise = (reverse k, m2):allSubs
    -- to be completed
    -- all keys from 2, keys from 1 that are not in 2
    where
        k2 = fst <$> toAscList db2
        k1 = Prelude.filter (\k -> isNothing $ Map.lookup k db2) $ fst <$> toAscList db
        keys = k1 ++ k2
        lu nk = deepDiffImpl (nk: k) (Map.lookup nk db) (Map.lookup nk db2)
        allSubs = concatMap lu keys

deepDiffImpl k Nothing m2@(Just (Tree n2 db2)) =
    (reverse k, m2):allSubs
    where
        keys = fst <$> toAscList db2
        lu nk = deepDiffImpl (nk: k) Nothing (Map.lookup nk db2)
        allSubs = concatMap lu keys

deepDiffImpl k (Just (Tree n db)) Nothing =
    (reverse k, Nothing):allSubs
    where
        keys = fst <$> toAscList db
        lu nk = deepDiffImpl (nk: k) (Map.lookup nk db) Nothing
        allSubs = concatMap lu keys

deepDiffImpl k (Just (Leaf n _)) m2@(Just (Leaf n2 v2))
    | n == n2 = []
    | otherwise = [(reverse k, m2)]

deepDiffImpl k Nothing m2@(Just (Leaf _ _)) = [(reverse k, m2)]
deepDiffImpl k m1@(Just (Leaf _ _)) Nothing = [(reverse k, Nothing)]

deepDiffImpl k (Just (Tree n db)) m2@(Just (Leaf _ _)) =
    (reverse k, m2):allSubs
    where
        keys = fst <$> toAscList db
        lu nk = deepDiffImpl (nk: k) (Map.lookup nk db) Nothing
        allSubs = concatMap lu keys

deepDiffImpl k (Just (Leaf _ _)) m2@(Just (Tree n2 db2)) =
    (reverse k, m2):allSubs
    where
        keys = fst <$> toAscList db2
        lu nk = deepDiffImpl (nk: k) Nothing (Map.lookup nk db2)
        allSubs = concatMap lu keys
deepDiffImpl _ _ _ = []
    
    
-- | Deep diff
deepDiff :: (Ord k) => Db k v -> Db k v -> [([k], Maybe (Db k v))]
deepDiff db1 db2 = deepDiffImpl [] (Just db1) (Just db2)

-- | Return the values of a db sorted by key
dbAsDbList :: (Ord k) => Db k v -> [Db k v]
dbAsDbList l@(Leaf _ _) = [l]
dbAsDbList (Tree _ m) =
    -- ??? with toAscList: already sorted (FIXME)
    snd <$> sortWith fst (toAscList m)

-- | Return a list of (key, db) from a db
dbAsKeyDbList :: (Ord k) => Db k v -> Maybe [(k, Db k v)]
dbAsKeyDbList (Leaf _ _) = Nothing
dbAsKeyDbList (Tree _ m) = Just (Map.toAscList m)

-- | lookup the hash of a key
deepLookupHash :: (Ord k, Binary k, Binary v) => [k] -> Db k v -> Maybe DbHash
deepLookupHash k db = do
    subDb <- deepLookup k db
    return $ dbHash subDb

-- | diff key between two dbs
keyChanged :: (Ord k, Binary k, Binary v) => Db k v -> Db k v -> [k] -> Bool
keyChanged fromDb toDb key =
    deepLookupHash key fromDb /= deepLookupHash key toDb

-- | diff multiple keys between two dbs
someKeyChanged :: (Ord k, Binary k, Binary v) => Db k v -> Db k v -> [[k]] -> Bool
someKeyChanged fromDb toDb keys = 
    isJust $ find (keyChanged fromDb toDb) keys

-- | select columns in a single db row
selectRowColumns :: (Ord k) => [[k]] -> Db k v -> [Maybe v]
selectRowColumns keys db =
    (`deepLookupValue` db) <$> keys

--- | selet existing values from a row
justSelectRowColumns :: (Ord k) => [[k]] -> Db k v -> Maybe [v]
justSelectRowColumns keys db = sequence $ selectRowColumns keys db

-- | select values from the db
select :: (Ord k) => [[k]] -> Db k v -> [[Maybe v]]      
select keys db =
    forAllRows db keys
    where
        forAllRows :: (Ord k) => Db k v -> [[k]] -> [[Maybe v]]
        forAllRows (Leaf _ l) [[]] = [[Just l]]
        forAllRows (Leaf _ _) _ = [[]] -- Nothing
        forAllRows (Tree _ m) fields =
            (selectRowColumns fields . snd) <$> toAscList m

-- | select existing values from the db
justSelect :: (Ord k) => [[k]] -> Db k v -> Maybe [[v]]      
justSelect keys db = 
    sequence $ sequence <$> select keys db

-- | number of keys in a tree
numKeys :: Db k v -> Int
numKeys (Leaf _ _) = 0
numKeys (Tree _ m) = length m
