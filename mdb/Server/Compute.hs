{-|
Module      : Server.Compute
Description : Execute DbComputations inside db transactions
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Computations on the db (ex: a count or a select) are 
serialized as a DbComputation. This module executes any DbComputation
inside a db transaction (this means for example that the whole thing
is replayable at the tip of the history)

Computations return a table (list of lists).
-}

module Server.Compute (
    compute,
    computeM
) where

import Server.Types
import Transaction.Transaction
import Transaction.Computations

-- | Execute a DbComputation and return a TransactionError or a [[DbValue]]
compute :: (Monad m) => DbComputation -> TransactionT DbKey DbValue m (Either TransactionError [[DbValue]])
compute (Count k) = do
    c <- count k
    return ((\n -> [[IntValue n]]) <$> c)

compute (Select fields from) =
    Transaction.Computations.select from fields
    
-- | Execute a DbComputation and return a TransactionError or a [[Maybe DbValue]]
computeM :: (Monad m) => DbComputation -> TransactionT DbKey DbValue m (Either TransactionError [[Maybe DbValue]])
computeM (Select fields from) =
    selectM from fields
