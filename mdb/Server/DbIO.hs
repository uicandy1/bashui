{-|
Module      : Server.DbIO
Description : Run the db server loop (accept and process connections)
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

A connection automatically establishes an atomic transaction.
-}

{-# LANGUAGE OverloadedStrings #-}
module Server.DbIO(
    DBIORef,
    createJournalRef,
    server,
    syncValueToDb 
    ) where

import Control.Concurrent (forkIO)
import Control.Monad
import Control.Monad.IO.Class(liftIO, MonadIO(..))
import Data.IORef(newIORef, readIORef, writeIORef, IORef, atomicModifyIORef')
import Diff(diffTree)
import MDb
import Network.Socket hiding (send, recv)
import Server.Compute
import Server.DiffToQuery
import Server.Directory
import Server.QueryBlock
import Server.Types
import System.Log.Logger
import Transaction.Transaction

-- | Out of transaction info allows to perform actions outside the current transaction
--   this should probably be a monad in the stack (below the transacction and before the IO
--   but currently it is simply passed around)
data OOT k v = OOT (IORef (UndoableJournal DbKey DbValue)) (UiDb -> UiDb -> IO ())

-- | Create a journal from a db. This is essentially the in memory db
--   in all its completeness (history etc)
createJournalRef :: UiDb -> String -> IO DBIORef
createJournalRef db message =
    newIORef $ UndoableJournal (Journal [ JournalEntry db message ]) (Journal [])

-- | Accept connections fork a thread for each connection and execute
-- a transaction on this thread 
server :: FilePath -> DBIORef  -> (UiDb -> UiDb -> IO ()) -> IO ()
server socketPath ioRef updateDb = withSocketsDo $ do
    infoM logContext "removing existing socket"
    removeIfExists socketPath
    infoM logContext "creating socket"
    sock <- socket AF_UNIX Stream 0 -- and try UDP?
    bind sock (SockAddrUnix socketPath)
    listen sock maxListenQueue -- TODO is maxListenQueue what we want?
    go ioRef sock
    infoM logContext "closing and exiting"
    close sock

    where
        logContext = "server"
        go :: IORef (UndoableJournal DbKey DbValue) -> Socket -> IO ()
        go ioRef sock = forever $ do
            infoM logContext "waiting for connection"
            (conn, _) <- accept sock
            infoM logContext "connection accepted, forking processing thread"
            forkIO $ do
                infoM logContext "processing started"
                transact (OOT ioRef updateDb) conn
                infoM logContext "processing done, exiting thread"
                close conn

-- | IO transaction
type IOTransaction m = TransactionT DbKey DbValue m

-- | Execute a transaction (connection worker threads run this)
transact :: OOT DbKey DbValue -> Socket -> IO ()
transact oot@(OOT ioRef updateDb) conn = do
    infoM logContext "processing thread"
    (UndoableJournal oj _) <- readIORef ioRef
    infoM logContext "execute transaction"
    eitherErrTr <- recvQb conn
    either
        (errorM logContext . show)
        (\(TransactionRequest trComment maybeHash) -> do 
            liftIO $ infoM logContext $ "trComment " ++ show trComment
            (newdb, operations) <- executeJournalIOTransactionAtHash oj maybeHash (runLoop oot conn) 
            (UndoableJournal j _)<- liftIO $ readIORef ioRef
            liftIO $ infoM logContext $ "checking for write in transaction " ++ show (anyIsWrite operations)

            transactionResult <- if anyIsRollback operations
                then return $ NoChange $ latestHash j
                else if anyIsWrite operations 
                then liftIO $ do
                    infoM logContext "updating journal"
                    notify <- atomicModifyIORef' ioRef 
                        (\uj@(UndoableJournal newJ _) ->
                            maybe 
                                (uj, False) 
                                (\j -> (UndoableJournal j (Journal []), True)) 
                                (updateJournal oj newJ newdb operations trComment)
                            ) 
                    (UndoableJournal newJ _) <- readIORef ioRef
                    infoM logContext $ "need to update db " ++ show notify
                    if notify then do
                        updateDb (latestDb j) (latestDb newJ)
                        return $ Success $ latestHash newJ
                    else 
                        return $ Error $ latestHash j
                else return $ NoChange $ latestHash j
            liftIO $ sendQb conn transactionResult)
        eitherErrTr 
    shutdown conn ShutdownSend
    return ()
    where
    
        logContext = "transact"
        runLoop :: OOT DbKey DbValue -> Socket -> IOTransaction IO ()
        runLoop oot socket = do
            liftIO $ infoM logContext "read query"
            queryOrError <- liftIO $ recvQb socket
            liftIO $ infoM logContext "query Read"
            either
                (\err -> case err of
                    ConnectionClosed -> do
                        liftIO $ infoM logContext "empty line read, exiting"
                        return ()
                    InvalidData d -> do
                        liftIO $ errorM logContext ("invalid data received, exiting: " ++ d)
                        return ()
                )
                (\query -> do
                    liftIO $ infoM logContext ("process query:" ++ show query)
                    do
                        res <- processQuery oot socket query
                        liftIO $ sendQb socket res
                    liftIO $ infoM logContext "check for end"
                    case query of
                        OOTCommit -> do
                            liftIO $ infoM logContext "end of transaction: commit"
                            return ()
                        OOTRollback -> do
                            liftIO $ infoM logContext "end of transaction: rollback"
                            return ()
                        _ -> 
                            runLoop oot socket
                )
                queryOrError
            where
                logContext = "transactionLoop"
                eitherToDbResult :: (MonadIO m) => Either TransactionError a -> IOTransaction m DbResult
                eitherToDbResult = either
                    (return . ResultError)
                    (\_ -> return ResultOK)
                    
                -- | process a single query. this may write stuff to the socket.
                processQuery :: (MonadIO m) => OOT DbKey DbValue -> Socket -> DbQuery -> IOTransaction m DbResult
                processQuery _ s (Set k v) = do
                    liftIO $ infoM logContext ("Set " ++ show k)
                    liftIO $ debugM logContext (show v)
                    writeKey k v >>= eitherToDbResult

                processQuery _ s (SetDb k db) = 
                    writeKeyAny k db >>= eitherToDbResult

                processQuery _ s (AppendDb k lines) =
                    fmap sequence (mapM (appendAny k) lines) >>= eitherToDbResult

                processQuery _ s (PrependDb k lines) =
                    fmap sequence (mapM (prependAny k) $ reverse lines) >>= eitherToDbResult

                processQuery _ s (Delete k) = do
                    liftIO $ infoM logContext ("Delete " ++ show k)
                    deleteKey k
                    return ResultOK

                processQuery _ s (Get k) = do
                    liftIO $ infoM logContext ("Get " ++ show k)
                    db <- readKeyAny k 
                    return $ ResultDb db
               
                processQuery _ s (Compute c) = do
                    eitherErrorV <- compute c 
                    either 
                        (return . ResultError)
                        (return . ResultCompute)
                        eitherErrorV

                processQuery oot s (Patch k diffs) = do
                    liftIO $ infoM logContext ("Patch " ++ show k)
                    let queries = diffToQuery k <$> diffs
                    mapM_ (processQuery oot s) queries
                    return ResultOK
                
                processQuery (OOT ioRef _) s OOTHistory = do
                    (UndoableJournal (Journal entries) _ ) <- liftIO $ readIORef ioRef
                    liftIO $ infoM logContext ("OOTHistory " ++ show (length entries))
                    return $ ResultJournal entries
                    -- mapM_ (\e -> liftIO $ send conn (C.pack $ format e)) entries
                    where
                        format :: JournalEntry DbKey DbValue -> String
                        format (JournalEntry db s) = show (dbHash db) ++ " " ++ s
                
                processQuery (OOT ioRef updateDb) s OOTUndo = liftIO $ do
                    (UndoableJournal uj@(Journal (u:v:undo)) (Journal redo) ) <- readIORef ioRef
                    -- FIXME: atomicModifyIORef'
                    let undoJ = Journal (v:undo)
                    let redoJ = Journal (u:redo)
                    --let JournalEntry dbfrom _ = u
                    --let JournalEntry dbto _ = v
                    writeIORef ioRef $ UndoableJournal undoJ redoJ
                    updateDb (latestDb uj) (latestDb undoJ)
                    return ResultOK
                    --updateDb dbfrom dbto
                        
                processQuery (OOT ioRef updateDb) s OOTRedo = liftIO $ do
                    (UndoableJournal uj@(Journal undo) (Journal (u:redo)) ) <- readIORef ioRef
                    -- FIXME: atomicModifyIORef'
                    let undoJ = Journal (u:undo)
                    let redoJ = Journal redo
                    writeIORef ioRef $ UndoableJournal undoJ redoJ
                    updateDb (latestDb uj) (latestDb undoJ)
                    return ResultOK

                processQuery (OOT ioRef updateDb) s OOTCommit = liftIO $ return ResultOK

                processQuery (OOT ioRef updateDb) s OOTRollback = do
                    rollback
                    liftIO $ return ResultOK 

                processQuery (OOT ioRef updatedb) s (OOTDiff fromHash toHash) = do
                    (UndoableJournal journal _) <- liftIO $ readIORef ioRef
                    let maybeDiffs = do  
                            (JournalEntry dbFrom _) <- lookupHash fromHash journal
                            (JournalEntry dbTo _) <- lookupHash toHash journal
                            return $ diffTree dbFrom dbTo

                    return $ maybe
                        (ResultError SomeWeirdShit) 
                        ResultDiff
                        maybeDiffs

-- | Sync a value directly to the database
syncValueToDb :: DBIORef -> [DbKey] -> DbValue -> String -> IO ()
syncValueToDb ioref key val comment = do
    atomicModifyIORef' ioref 
        (\oj@(UndoableJournal newJ@(Journal entries) _) -> do
            let JournalEntry db _ = head entries
            -- create an operation for the update
            let (updatedDb, ops) = executeTransaction (writeKey key val) db
            -- Update the journal
            maybe 
                (oj, False) 
                (\j -> (UndoableJournal j (Journal []), True)) 
                (updateJournal newJ newJ updatedDb ops comment)
            ) 
    return ()
