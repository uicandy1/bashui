{-|
Module      : Server.DiffToQuery
Description : Transforms a diff to the query to apply the diff
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
module Server.DiffToQuery(
    diffToQuery
    ) where
import Server.Types
import Diff


-- | Turn a diff into a query
diffToQuery :: [DbKey] -> UiDbDiff -> DbQuery
diffToQuery prefixPath (Create k v) = SetDb (prefixPath ++ [k]) v
diffToQuery prefixPath (Update k v) = SetDb (prefixPath ++ [k]) v
diffToQuery prefixPath (Diff.Delete k) = Server.Types.Delete (prefixPath ++ [k])
