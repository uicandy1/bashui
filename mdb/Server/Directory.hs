{-|
Module      : Server.Directory
Description : Remove a file from a directory if it exists
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
module Server.Directory(
    removeIfExists 
    ) where
import Control.Exception (catch, throwIO)
import System.Directory(removeFile)
import System.Directory.Internal.Prelude(isDoesNotExistError)

-- | Remove a file if it exists
removeIfExists :: FilePath -> IO ()
removeIfExists fileName = removeFile fileName `catch` handleExists
  where handleExists e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e
