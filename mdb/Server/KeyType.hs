{-|
Module      : Server.KeyType
Description : Data definition for database keys
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
{-# LANGUAGE FlexibleInstances #-}
module Server.KeyType (
    KeyType(..),
    isIndex 
    )
    where

import Data.Binary (Binary(..), Word8, Get(..))
import Text.ParserCombinators.ReadPrec 
import Text.Read
import Lists

-- | Key type. An Index key allows us to have ordered lists instead of dicts
data KeyType t = Key t | Index Int
    deriving (Eq, Ord)


instance (Show t) => Show (KeyType t) where
    show (Index i) =
        "#" ++ show i
    show (Key t) =
        show t

instance (Read t) => Read (KeyType t) where
    readListPrec = readListPrecDefault
    readPrec = parens $ prec app_prec ( do
                                Symbol "#" <- lexP
                                i <- step readPrec
                                return $ Index i)
                        +++ prec higher_prec (do
                                t <- step readPrec
                                return $ Key t)
                    where 
                        -- this higher prec is needed
                        app_prec = 12
                        higher_prec = 11 

instance (Binary t) => Binary (KeyType t) where
    put (Key s) = do
                            put (0 :: Word8)
                            put s
    put (Index i) = do
                            put (1 :: Word8)
                            put i
    get = do
            t <- Data.Binary.get :: Get Word8
            case t of
                0 -> do
                    s <- Data.Binary.get
                    return (Key s)
                1 -> do
                    i <- Data.Binary.get
                    return (Index i)

instance NumericalKey (KeyType t) where
    toInt (Index i) = Just i
    toInt _ = Nothing

    fromInt = Index

-- | Determine if a key is an index
isIndex :: KeyType t -> Bool
isIndex (Key _) = False
isIndex (Index _) = True
