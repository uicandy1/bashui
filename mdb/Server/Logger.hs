{-|
Module      : Server.Logger
Description : Configure the logger
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}

module Server.Logger (
    setupLogger
    )
    where
import System.Console.ANSI
import System.IO(stderr)
import System.Log.Formatter
import System.Log.Handler(setFormatter)
import System.Log.Handler.Simple(streamHandler)
import System.Log.Logger

-- | Setup the global logger
setupLogger :: String -> Priority -> IO ()
setupLogger appName level = do
    logger <- getRootLogger
    shandler <- streamHandler stderr DEBUG
    let handler = setFormatter shandler $
            simpleLogFormatter $ setSGRCode [SetColor Foreground Dull Red] ++
                "$prio: " ++
                setSGRCode [SetColor Foreground Dull Magenta] ++
                appName ++ 
                " ($loggername) "++
                setSGRCode [SetColor Foreground Dull White] ++
                "$msg " ++
                setSGRCode [SetColor Foreground Dull Magenta] ++
                "$tid" ++
                setSGRCode [Reset]
    saveGlobalLogger $ setLevel level . setHandlers [handler] $ logger 
