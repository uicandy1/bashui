{-|
Module      : Server.QueryBlock
Description : Information block used by our rudimentary communication protocol 
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
module Server.QueryBlock (
    QueryBlock(..),
    QueryBlockError(..),
    sendQb,
    recvQb
    ) where

import Data.Maybe(isJust, fromJust)
import Data.Binary(decode, encode)
import qualified Data.ByteString.Char8 as C
import Data.ByteString.Lazy(fromStrict, toStrict)
import Data.Int (Int)
import Foreign.Storable (sizeOf)
import Network.Socket hiding (sendAll, recv)
import Network.Socket.ByteString
import Text.Read(readMaybe)

-- | Error defintions
data QueryBlockError = ConnectionClosed | InvalidData String | UnexpectedError
    deriving(Show, Read)

-- | Query block definition
data QueryBlock = QbText Int | QbBinary Int deriving (Read, Show)

-- | Send an arbitrary payload as a query block
sendQb :: (Show payload) => Socket -> payload -> IO ()
sendQb conn payload = do
    sendAll conn $ toStrict $ encode l
    sendAll conn $ C.pack stringified
    return ()
    where
        stringified = show payload
        l = length stringified

-- | Receive raw data on a socket
recvN :: Socket -> Int -> IO (Maybe C.ByteString)
recvN conn n = do
    b <- recv conn (min n 2048)
    let lb = C.length b
    let remaining = n - lb
    if lb == 0 then
        return Nothing
    else 
        if remaining > 0 then do
            mr <- recvN conn remaining
            return $ mr >>= (Just . C.append b) 
        else return $ Just b 

-- | Size of an Int
sizeOfInt :: Int
sizeOfInt =
    sizeOf a
    where
        a :: Int
        a = 0

-- | Receive a query block from a socket
recvQb :: (Read pl) => Socket -> IO (Either QueryBlockError pl)
recvQb conn = do
    maybeSz <- recvN conn sizeOfInt
    if isJust maybeSz then do
        let sz = fromJust maybeSz
        let n = decode $ fromStrict sz :: Int
        maybeBuf <- recvN conn n
        if isJust maybeBuf then do
            let buf = fromJust maybeBuf
            let unpacked = C.unpack buf
            let maybepl = readMaybe unpacked 
            return $ maybe (Left $ InvalidData (show unpacked)) Right maybepl
        else return $ Left ConnectionClosed
    else return $ Left ConnectionClosed
