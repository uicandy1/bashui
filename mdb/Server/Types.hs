{-|
Module      : Server.Types
Description : Define concrete types for the ui db 
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Defines the concrete types that we use for the ui db.
-}
module Server.Types (
    InsertAt(..),    
    DbQuery(..),
    DbComputation(..),
    TransactionRequest(..),
    TransactionResult(..),
    DbResult(..),
    DbValue(..),
    DbKey(..),
    KeyType(..),
    UiDb(..),
    DbHash(..),
    UiDbDiff(..),
    DbPath(..),
    UndoableJournal(..),
    DBIORef
    ) where
import MDb
import Data.Binary (Binary(..), Word8, Get(..))
import Data.IORef(IORef)
import Server.KeyType
import Transaction.Transaction
import Diff

-- | A Db Computation
data DbComputation = 
    Count [DbKey] |
    Select [[DbKey]] [DbKey]
        deriving (Read, Show)

-- | Insertion position
data InsertAt = Head | Tail
    deriving (Read, Show)

-- | Transaction Request
data TransactionRequest = TransactionRequest String (Maybe String)
    deriving (Read, Show)

instance Binary TransactionRequest where
    put (TransactionRequest s h) = do
        put s
        put h
    get = do
        s <- get
        h <- get
        return $ TransactionRequest s h

-- | Transaction Result
data TransactionResult = NoChange DbHash | Success DbHash | Error DbHash
    deriving (Read, Show)

instance Binary TransactionResult where
    put (NoChange h) = do
        put (0 :: Word8)
        put h
    put (Success h) = do
        put (1 :: Word8)
        put h
    put (Error h) = do
        put (2 :: Word8)
        put h
    get = do
            t <- get :: Get Word8
            h <- get
            return $ case t of
                0 -> (NoChange h)
                1 -> (Success h)
                2 -> (Error h)
 
-- | A query on the database
data DbQuery = 
    Set [DbKey] DbValue |
    SetDb [DbKey] UiDb |
    AppendDb [DbKey] [UiDb] |
    PrependDb [DbKey] [UiDb] |
    Delete [DbKey] |
    Get [DbKey] |
    Compute DbComputation |
    Patch [DbKey] [UiDbDiff] |
    -- These queriesl are "out of transaction"
    OOTDiff DbHash DbHash |
    OOTHistory |
    OOTUndo |
    OOTRedo |
    OOTCommit |
    OOTRollback
        deriving (Read, Show)

instance Binary DbQuery where
    put (Set path v) = do
        put (0 :: Word8)
        put path
        put v
    {-put (SetDb path db) = do
        put (1 :: Word8)
        put path
        put db-}
    {-put (AppendDb path dbs) = do
        put (2 :: Word8)
        put path
        put dbs-}
    put (Server.Types.Delete path) = do
        put (3 :: Word8)
        put path
    put (Get path) = do 
        put (4 :: Word8)
        put path
    {-put (Compute computation) = do
        put (5 :: Word8)
        put computation-}
    put (OOTDiff hashFrom hashTo) = do 
        put (6 :: Word8)
        put hashFrom
        put hashTo
    put (OOTHistory) = do  
        put (7 :: Word8)
    put (OOTUndo) = do 
        put (8 :: Word8)
    put (OOTRedo) = do
        put (9 :: Word8)
    get = do
            t <- get :: Get Word8
            case t of
                0 -> do
                    path <- get
                    v <- get
                    return (Set path v)
                {- 1 -> do
                    path <- get
                    db <- get
                    return (SetDb path db)-}
                {- 2 -> do
                    path <- get
                    dbs <- get
                    return (AppendDb path dbs)-}
                3 -> do
                    path <- get
                    return (Server.Types.Delete path)
                4 -> do
                    path <- get
                    return (Get path)
                {- 5 -> do
                    cmp <- get
                    return (Compute cmp) -}
                6 -> do
                    hashFrom <- get
                    hashTo <- get
                    return (OOTDiff hashFrom hashTo)
                7 -> do
                    return (OOTHistory)
                8 -> do
                    return (OOTUndo)
                9 -> do
                    return (OOTRedo)

-- | The result of a query on the database
data DbResult = 
    ResultError TransactionError | 
    ResultOK | 
    ResultDb (Maybe UiDb) | 
    ResultCompute [[DbValue]] |
    ResultJournal [JournalEntry DbKey DbValue] |
    ResultDiff [Diff.DbDiff DbKey (Db DbKey DbValue)]
        deriving (Read, Show)

-- | Value of something in the model. This could be pretty much anything
data DbValue = 
    StringValue String | 
    IntValue Int |
    FloatValue Float
        deriving (Eq, Read, Show)

instance Binary DbValue where
    put (StringValue s) = do
                            put (0 :: Word8)
                            put s
    put (IntValue i) = do
                            put (1 :: Word8)
                            put i
    put (FloatValue f) = do
                            put (2 :: Word8)
                            put f
    get = do
            t <- get :: Get Word8
            case t of
                0 -> do
                    s <- get
                    return (StringValue s)
                1 -> do
                    i <- get
                    return (IntValue i)
                2 -> do
                    f <- get
                    return (FloatValue f)

-- | The DbKey type
type DbKey = KeyType String

-- | The concret UiDb type
type UiDb = Db DbKey DbValue

-- | A diff between two dbs
type UiDbDiff = DbDiff DbKey UiDb

-- | DbPath (deeply nested key for the db)
type DbPath = [DbKey]

-- | The undoable journal is split in two sub journals : one before and one after
data UndoableJournal k v = UndoableJournal (Journal k v) (Journal k v)

-- | An IORef to the journal (full in memory db with history etc) 
type DBIORef = IORef (UndoableJournal DbKey DbValue)

