{-|
Module      : Transaction.Computations
Description : Computations (like count or select)
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Computations (like count or select) inside a transaction
-}
module Transaction.Computations (
    count,
    Transaction.Computations.select,
    selectM 
    ) where
import MDb
import Data.Map.Strict as Map
import Transaction.Transaction

-- | Counts the number of subkeys in a tree (always returns 1 for a leaf)
count :: (Ord k, Monad m) => [k] -> TransactionT k v m (Either TransactionError Int)
count key = do
    maybeDb <- readKeyAny key 
    return (Right $ maybe 0 c maybeDb)
    where
        c :: Db k v -> Int
        c (Leaf _ _) = 1
        c (Tree _ m) = length m

-- | Selects values in a tree based on a set of keys
select :: (Ord k, Monad m) => [k] -> [[k]] -> TransactionT k v m (Either TransactionError [[v]])
select key fields = do
    maybeDb <- readKeyAny key 
    return $ maybe (Left KeyNotFound) (processDb fields) maybeDb 
    where  
        processDb :: (Ord k) => [[k]] -> Db k v -> Either TransactionError [[v]]      
        processDb keys db =
            maybe (Left MissingColumns) Right res
            where
                res = forAllRows db keys
        forAllRows :: (Ord k) => Db k v -> [[k]] -> Maybe [[v]]
        forAllRows (Leaf _ _) _ = Nothing
        forAllRows (Tree _ m) fields =
            sequence ((getKeys fields . snd) <$> toList m)
        getKeys :: (Ord k) => [[k]] -> Db k v -> Maybe [v]
        getKeys keys db = 
            sequence ((`deepLookupValue` db) <$> keys)

-- | Selects values in a tree based on a set of keys
selectM :: (Ord k, Monad m) => [k] -> [[k]] -> TransactionT k v m (Either TransactionError [[Maybe v]])
selectM key fields = do
    maybeDb <- readKeyAny key 
    return $ maybe (Left KeyNotFound) (processDb fields) maybeDb 
    where  
        processDb :: (Ord k) => [[k]] -> Db k v -> Either TransactionError [[Maybe v]]      
        processDb keys db =
            Right $ MDb.select keys db
