{-|
Module      : Transaction.Transaction
Description : Database transaction mechanism
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Transactions. 
-}
module Transaction.Transaction (
    TransactionError(..),
    JournalEntry(..),
    Journal(..),
    Transaction(..),
    TransactionState(..),
    TransactionT(..),
    lookupHash,
    isWrite,
    anyIsWrite,
    anyIsRollback,
    readKeyAny,
    readKey,
    writeKeyAny,
    writeKey,
    appendAny,
    prependAny,
    deleteKey,
    rollback,
    executeTransaction,
    executeIOTransaction, 
    executeJournalTransaction,
    executeJournalIOTransaction,
    executeJournalIOTransactionAtHash,
    latestDb,
    latestHash,
    sameAtTip,
    updateJournal 
    ) where
import Control.Monad.State.Strict
import Data.ByteString (ByteString(..), isInfixOf)
import Data.ByteString.Base64(encode)
import Data.ByteString.Char8(pack)
import Data.Binary (Binary)
import Data.List (find)
import Data.Maybe (fromMaybe)
import MDb
import Lists
import Data.Functor.Identity (Identity(..))

-- | Transaction error
data TransactionError =
    DatabaseError DbError |
    KeyNotFound |
    MissingColumns |
    SomeWeirdShit |
    ShiftingAtLeaf |
    Rollback
        deriving (Read, Show)

-- | Transaction operations. These are replayable at any point of the history
data Operation k v =
    -- Read a LEAF value. Reading a Tree does not count, it's not a tangible
    -- thing 
    ReadLeafValue [k] (Maybe ByteString) |
    -- Read the length (number of subkeys) of any key (not just leaf)
    -- This also works on leafs. If a key points to a leaf value, the result is 1.
    -- If there is no key at all... should probably just be zero
    ReadSubKeysLength [k] Int |
    -- This again works anywhere on the tree (leafs and subtrees). Leafs return
    -- an empty list. Subtrees return the list of their subkeys possibly empty.
    -- Not sure I need this to do a shift. The information retrieved (in a shift)
    -- has no impact on VALUES... More thinking More thinking
    -- ReadSubKeys [k] |

    -- The Write operations are simply the things you want to do. There is
    -- nothing to check. The important is that what you read is what you
    -- saw when you did your transaction on an isolated db. We still need
    -- to 
    Write [k] v |
    WriteDb [k] (Db k v) |
    Delete [k] |
    -- We can append with numerical keys
    Append [k] (Db k v) |
    Prepend [k] (Db k v) |
    RollbackTransaction

-- | The state maintained inside a transaction
data TransactionState k v =
    TransactionState [Operation k v] (Db k v) 

-- | A single entry in a journal 
data JournalEntry key value =
    JournalEntry (Db key value) String
        deriving (Read, Show)

-- | A journal 
newtype Journal k v = Journal [JournalEntry k v]

-- | The TransactionT monad transformer
type TransactionT k v m = StateT (TransactionState k v) m 


-- | Lookup a hash from a Journal
lookupHash :: DbHash -> Journal k v -> Maybe (JournalEntry k v)
lookupHash h (Journal entries) =
    find (\(JournalEntry db _) -> (dbHash db) == h) entries


-- | Checks that an operation is not read only
isWrite :: Operation k v -> Bool
isWrite (Write _ _) = True
isWrite (WriteDb _ _) = True
isWrite (Transaction.Transaction.Delete _) = True
isWrite (Append _ _) = True
isWrite (Prepend _ _) = True
isWrite _ = False

-- | Checkes that an operation is a rollback
isRollback :: Operation k v -> Bool
isRollback RollbackTransaction = True
isRollback _ = False

-- | Check that a list of operations dows not contain a write operation
anyIsWrite :: [Operation k v] -> Bool
anyIsWrite = any isWrite

-- | Check that a list of operations dows not contain a rollback operation
anyIsRollback :: [Operation k v] -> Bool
anyIsRollback = any isRollback

-- | Read a key as a database (the whole subtree is seen as read)
readKeyAny :: (Ord k, Monad m) => [k] -> TransactionT k v m (Maybe (Db k v))
readKeyAny k = do
    ts@(TransactionState _ db) <- get
    let maybeDb = deepLookup k db
    put $ tstate k maybeDb ts
    return maybeDb 
    where
        tstate :: [k] -> Maybe (Db k v) -> TransactionState k v -> TransactionState k v
        tstate k (Just vdb) (TransactionState e db) =
            TransactionState (newRe:e) db
            where newRe = ReadLeafValue k (Just $ dbHash vdb)
        tstate k Nothing (TransactionState e db) =
            TransactionState (newRe:e) db
            where newRe = ReadLeafValue k Nothing

-- | Read a key as a value 
readKey :: (Ord k, Monad m) => [k] -> TransactionT k v m (Either TransactionError (Maybe v))
readKey k = do
    ndb <- readKeyAny k 
    ts@(TransactionState _ db) <- get
    state $ const (maybe (Right Nothing) onlyLeaf ndb, ts)
    where
        onlyLeaf :: Db k v -> Either TransactionError (Maybe v)
        onlyLeaf (Leaf _ v) = Right $ Just v
        onlyLeaf _ = Left (DatabaseError NotALeaf)

-- | Write a key
writeKeyAny :: (Ord k, Binary k, Binary v, Monad m) => [k] -> Db k v -> TransactionT k v m (Either TransactionError ())
writeKeyAny k dbv = do
    ts@(TransactionState _ dbs) <- get
    either
        (return . Left . DatabaseError)
        (\dbu -> do
            put $ tstate k dbv dbu ts
            return $ Right ()) 
        (deepSet k dbv dbs)
    where
        tstate :: [k] -> Db k v -> Db k v -> TransactionState k v -> TransactionState k v
        tstate k dbv dbs (TransactionState e _) =
            TransactionState (newRe:e) dbs
            where newRe = WriteDb k dbv
    
-- | Write a key
writeKey :: (Ord k, Binary k, Binary v, Monad m) => [k] -> v -> TransactionT k v m (Either TransactionError ())
writeKey k v = do
    ts@(TransactionState _ dbs) <- get
    either
        (return . Left . DatabaseError)
        (\dbu -> do
            put $ tstate k v dbu ts
            return $ Right ()) 
        (deepSetValue k v dbs)
    where
        tstate :: [k] -> v -> Db k v -> TransactionState k v -> TransactionState k v
        tstate k v dbs (TransactionState e _) =
            TransactionState (newRe:e) dbs
            where newRe = Write k v


-- | Append a single numerical key
appendAny :: (Ord k, Binary k, NumericalKey k, Binary v, Monad m) =>  [k] -> Db k v -> TransactionT k v m (Either TransactionError ())
appendAny atKey dbv = do
    ts@(TransactionState _ dbs) <- get
    maybe
        (return $ Left SomeWeirdShit)
        (\dbu -> do
            put $ tstate atKey dbv dbu ts
            return $ Right ()) 
        (playOp dbs newRe)
    where
        newRe = Append atKey dbv
        -- tstate :: (Ord k, Binary k, NumericalKey k, Binary v, Monad m) => [k] -> Db k v -> Db k v -> TransactionState k v -> TransactionState k v
        tstate k dbv dbs (TransactionState e _) =
            TransactionState (newRe:e) dbs

-- | Prepend a collection
prependAny :: (Ord k, Binary k, NumericalKey k, Binary v, Monad m) =>  [k] -> Db k v -> TransactionT k v m (Either TransactionError ())
prependAny atKey dbv = do
    ts@(TransactionState _ dbs) <- get
    maybe
        (return $ Left SomeWeirdShit)
        (\dbu -> do
            put $ tstate atKey dbv dbu ts
            return $ Right ()) 
        (playOp dbs newRe)
    where
        newRe = Prepend atKey dbv
        -- tstate :: (Ord k, Binary k, NumericalKey k, Binary v, Monad m) => [k] -> Db k v -> Db k v -> TransactionState k v -> TransactionState k v
        tstate k dbv dbs (TransactionState e _) =
            TransactionState (newRe:e) dbs
-- | Delete a key
deleteKey :: (NumericalKey k, Ord k, Binary k, Binary v, Monad m) => [k] -> TransactionT k v m (Either TransactionError ())
deleteKey k = do
    ts@(TransactionState _ dbs) <- get
    maybe
        (return $ Left SomeWeirdShit)
        (\dbu -> do
            put $ tstate k dbs dbu ts
            return $ Right ()) 
        (playOp dbs newRe)
    where
        newRe = Transaction.Transaction.Delete k
        -- tstate :: (Ord k, Binary k, NumericalKey k, Binary v, Monad m) => [k] -> Db k v -> Db k v -> TransactionState k v -> TransactionState k v
        tstate k dbv dbs (TransactionState e _) =
            TransactionState (newRe:e) dbs

-- | Rollback the transaction
rollback :: (NumericalKey k, Ord k, Binary k, Binary v, Monad m) => TransactionT k v m (Either TransactionError ())
rollback = do
    ts@(TransactionState e dbs) <- get
    put $ TransactionState (RollbackTransaction:e) dbs
    return $ Right ()

-- | The transaction monad
type Transaction k v a = TransactionT k v Identity a

-- | Latest step in a journal
latestDb :: Journal k v -> Db k v
latestDb (Journal (JournalEntry db _:_)) = db


-- | Step at a given hash in a journal
dbAtHash :: Journal k v -> String -> Maybe (Db k v)
dbAtHash (Journal entries) hash = do
    (JournalEntry fdb _) <- find (entryMatches hash) entries
    return fdb
    where
        entryMatches :: String -> JournalEntry k v -> Bool
        entryMatches s (JournalEntry db comment) =
            (isInfixOf packeds $ pack comment) ||
                (isInfixOf packeds (encode (dbHash db)))
            where packeds = pack s


-- | Hash of the latest step in the journal
latestHash :: Journal k v -> DbHash
latestHash = dbHash . latestDb

-- | Execute an IO transaction
executeTransaction :: Transaction k v a -> Db k v -> (Db k v, [Operation k v])
executeTransaction t db = 
    (ndb, ops)
    where 
        TransactionState ops ndb = execState t (TransactionState [] db)

-- | Execute an IO transaction
executeIOTransaction :: (Show k, Show v, MonadIO m) => TransactionT k v m a -> Db k v -> m (Db k v, [Operation k v])
executeIOTransaction t db = do
    TransactionState ops ndb <- execStateT t (TransactionState [] db)
    return (ndb, ops)

-- | begin a transaction in a journal
executeJournalTransaction :: Journal k v -> Transaction k v a -> (Db k v, [Operation k v])
executeJournalTransaction j t = 
    executeTransaction t (latestDb j) 

-- | begin a transaction in a journal
executeJournalIOTransaction :: (Show k, Show v, MonadIO m) => Journal k v -> TransactionT k v m a -> m (Db k v, [Operation k v])
executeJournalIOTransaction j t = 
    executeIOTransaction t (latestDb j) 

-- | begin a transaction in a journal at a given hash
executeJournalIOTransactionAtHash :: (Show k, Show v, MonadIO m) => Journal k v -> Maybe String -> TransactionT k v m a -> m (Db k v, [Operation k v])
executeJournalIOTransactionAtHash j Nothing t = 
    executeIOTransaction t (latestDb j) 
executeJournalIOTransactionAtHash j (Just h) t = 
    executeIOTransaction t $ fromMaybe (latestDb j) (dbAtHash j h)
    
-- | run transaction in a db
playOp :: (Ord k, Binary k, NumericalKey k, Binary v) => Db k v -> Operation k v -> Maybe (Db k v) 
playOp db (ReadLeafValue k h) 
    | h /= dbh = Nothing
    | otherwise = mdbd
    where
        mdbd = deepLookup k db
        dbh = do
            dbd <- mdbd
            return $ dbHash dbd

playOp db (Write k v) =
    either (const Nothing) Just res
    where
        res = deepSetValue  k v db

playOp db (WriteDb k v) =
    either (const Nothing) Just res
    where
        res = deepSet k v db

-- Only works with numerical keys (by design)
playOp db (Transaction.Transaction.Delete []) = Nothing
playOp db (Transaction.Transaction.Delete k) = do
    toInt tbd
    either (const Nothing) Just eitherNewdb
    where
        tbd = last k
        indb = init k
        eitherNewdb = deepMap indb (\subdb -> lreindex $ delete tbd subdb) db
         
playOp db (Append k dbv) =
    either (const Nothing) Just $ deepMap k (lappend [dbv]) db

playOp db (Prepend k dbv) =
    either (const Nothing) Just $ deepMap k (lprepend [dbv]) db

deepMap :: (Ord k, Binary k, Binary v) => [k] -> (Db k v -> Either DbError (Db k v)) -> Db k v -> Either DbError (Db k v)
deepMap k f db = do
    subdb <- maybe (Left InvalidIndex) Right $ deepLookup k db
    updated <- f subdb
    deepSet k updated db

-- | play a list of operations over an initial db
playOperations :: (Ord k, Binary k, Binary v, NumericalKey k) => Db k v -> [Operation k v] -> Maybe (Db k v)
playOperations = foldM playOp

-- | compares the hash of the tip of two journals
sameAtTip :: Journal k v -> Journal k v -> Bool
sameAtTip (Journal []) (Journal []) = True
sameAtTip (Journal []) _ = False
sameAtTip _ (Journal []) = False

-- | sameAtTip (Journal [(JournalEntry db1 _):_]) (Journal [(JournalEntry db2 _):_]) =
sameAtTip j1 j2 =
    dbHash (latestDb j1) == dbHash (latestDb j2)

-- | update a journal with the result of a transaction (if the base and the tip are the same,
-- the candidate is kept and added at the tip. Otherwise the operations are played at
-- the tip.
updateJournal :: (Ord k, Binary k, NumericalKey k, Binary v) => Journal k v -> Journal k v -> Db k v -> [Operation k v] -> String -> Maybe (Journal k v)
updateJournal base tip@(Journal entries) candidate operations comment
    | baseHash == tipHash = Just $ Journal (JournalEntry candidate comment : entries)
    | otherwise = do
                newDb <- playOperations latestTip operations 
                return $ Journal (JournalEntry newDb  comment : entries)
    where
        latestBase = latestDb base
        latestTip = latestDb tip
        baseHash = dbHash latestBase
        tipHash = dbHash latestTip
