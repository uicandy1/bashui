#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "capture.glade" [
            AppWindow "ID_MAIN",
            MouseCapture "ID_DRAWING" (Literal (StringValue "mouse"))
        ], 
    db = Tree (fromList [
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

# handlers
# --------
mouse () {
    cat > capture.log
}
export -f "mouse"

# run it
# ------
runui
