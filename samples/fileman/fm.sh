#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "fileman.glade" [
            AppWindow "ID_MAIN",
            LabelText "MY_LABEL" (Reference ["directory"]),
            EntryActivated "PATH_ENTRY" (Literal (StringValue "./action/onactivatepath.sh") ),
            EntryText "PATH_ENTRY" (Reference ["directory"]),
            TreeViewListItems "LEFT_TREEVIEW" (Reference ["locations"]) 
                [
                    Column "Locations" (Reference ["label"]) Text
                ],
            RowActivated "LEFT_TREEVIEW" (Reference ["locations"]) (Reference ["action"]),
            TreeViewSelectionChanged "LEFT_TREEVIEW" (Reference ["locations"]) (Reference ["action"]),
            TreeViewListItems "RIGHT_TREEVIEW" (Reference ["files"]) 
                [
                    Column "" (Reference ["icon"]) Icon,
                    Column "Files" (Reference ["label"]) Text,
                    Column "Size" (Reference ["size"]) Text,
                    Column "Rights" (Reference ["rights"]) Text
                ],
            RowActivated "RIGHT_TREEVIEW" (Reference ["files"]) (Reference ["action"]),
            IconViewListItems "RIGHT_ICONVIEW" (Reference ["files"]) 
                [
                    Column "Text" (Reference ["label"]) Text,
                    Column "" (Reference ["icon"]) Icon
                ],
            TreeViewSelectionChanged "RIGHT_TREEVIEW" (Reference ["files"]) (Reference ["selection"]),
            ToolButtonActivated "BACK_BUTTON" (Literal (StringValue "./action/onundo.sh") ),
            ToolButtonActivated "FORWARD_BUTTON" (Literal (StringValue "./action/onredo.sh") ),
            WidgetSensitive "BACK_BUTTON" (Reference ["canUndo"]),
            WidgetSensitive "FORWARD_BUTTON" (Reference ["canRedo"]),
            TreeViewListItems "FILE_ATTRIBUTES" (Reference ["attributes"]) 
                [
                    Column "Attribute" (Reference ["name"]) Text,
                    Column "Value" (Reference ["value"]) Text
                ],
            TextViewText "HEXDUMP_TEXTVIEW" (Reference ["hexdump"]),
            ImageBase64 "PREVIEW_IMAGE" (Reference ["image"]),
            ApplicationReady (Literal (StringValue "$script/view/update.sh now | uidb"))
        ], 
    db = Tree (fromList [
            ("directory",Leaf (StringValue "potato")),
            ("canUndo", Leaf (IntValue 0)),
            ("canRedo", Leaf (IntValue 0)),
            ("locations",Tree (fromList [
                ("1",Tree (fromList [
                    ("action",Leaf (StringValue "action/onclickleft.sh aaa")),
                    ("label",Leaf (StringValue "aaa"))
                    ])),
                ("2",Tree (fromList [
                    ("action",Leaf (StringValue "action/onclickleft.sh bbb")),
                    ("label",Leaf (StringValue "bbb"))
                    ])),
                ("3",Tree (fromList [
                    ("action",Leaf (StringValue "action/onclickleft.sh ccc")),
                    ("label",Leaf (StringValue "ccc"))
                    ]))
                ])),
            ("files",Tree (fromList [
                ("1",Tree (fromList [
                    ("action",Leaf (StringValue "action/onselectfile.sh aaa")),
                    ("label",Leaf (StringValue "aaa"))
                    ])),
                ("2",Tree (fromList [
                    ("action",Leaf (StringValue "action/onselectfile.sh bbb")),
                    ("label",Leaf (StringValue "bbb"))
                    ])),
                ("3",Tree (fromList [
                    ("action",Leaf (StringValue "action/onselectfile.sh ccc")),
                    ("label",Leaf (StringValue "ccc"))
                    ]))
                ])),
            ("attributes",Tree (fromList [
                ("1",Tree (fromList [
                    ("value",Leaf (StringValue "0")),
                    ("name",Leaf (StringValue "aaa"))
                    ]))
                ])),
            ("hexdump", Leaf (StringValue "")),
            ("image", Leaf (StringValue ""))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

export mainapp=ui
export dbapp=candy
export appdata="$tmppath"/appdata
export socketPath="$uiSocketPath"
mkdir -p "$appdata"
source "$script"/model/fmm.sh 
setup "$(pwd)"

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------
# run it
# ------

runui


