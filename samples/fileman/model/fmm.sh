#! /usr/bin/env bash
# The simplest (not the fastest) strategy is to implement
# a single updater of the view db (i.e. regenerate all).
# This is what we do here
set -e
script="$(readlink -f "$(dirname "$0")")"
setup () {
    local path="$(readlink -f "$1")"
    echo -n "" > "$appdata/undo"
    echo -n "" > "$appdata/redo"
    echo "$path" > "$appdata/path"
}
setpath () {
    local path="$(readlink -f "$1")"
    if [ -d "$path" ]
    then
        local oldpath=$(cat "$appdata/path")
        if [ "$path" != "$oldpath" ]
        then
            echo "$path" >"$appdata/path" 
            echo "$oldpath" >>"$appdata/undo"
            echo -n "" > "$appdata/redo"        
        fi
    fi 
}
ur () {
    local from="$1"
    local to="$2"
    if [ "$(wc -l "$appdata/$from")" != "0" ]
    then
        local last=$(tail -n 1 < "$appdata/$from")
        head -n -1 "$appdata/$from" > "$appdata/$from.step"
        cp "$appdata/$from.step" "$appdata/$from"
        rm "$appdata/$from.step"
        local cur="$(cat "$appdata/path")"
        echo "$cur" >> "$appdata/$to"
        echo "$last" >"$appdata/path" 
    fi
}
redo() {
    ur "redo" "undo"
}

undo() {
    ur "undo" "redo"
}

ursteps() {
    local from="$1"
    wc -l <"$appdata/$from"
}

canUndo() {
    if [ "$(ursteps undo)" == "0" ]
    then
        echo false
    else
        echo true
    fi
}

canRedo() {
    if [ "$(ursteps redo)" == "0" ]
    then
        echo false
    else
        echo true
    fi
}

setfile() {
    local f="$1"
    echo "$f" > "$appdata/file"
}

clearfile() {
    echo "" > "$appdata/file"
}
