#! /usr/bin/env bash
set -e
# use a temp directory in the file system as our database
# 
#
script="$(readlink -f "$(dirname $0)")"
export appdata=$(mktemp -d)
source "$script"/../model/fmm.sh 
function finish {
  rm -fr "$appdata"
}
trap finish EXIT

expect () {
    msg="$1"
    expected="$2"
    value="$3"
    line="$4"
    if [ "$expected" != "$value" ]
    then
        echo "$msg, ($line) \"$value\" != \"$expected\""
        cat output
    fi
}
# working dirs
browsedira="$script/a"
browsedirb="$script/a/b"
browsedirc="$script/a/b/c"
mkdir -p "$browsedirc"
# setup
setup "$browsedira"
expect "setup creates the expected path" "$browsedira" "$(cat "$appdata/path" | tee output)" $LINENO
expect "setup an empty undo file" "0" "$(cat "$appdata/undo" | tee output | wc -l)" $LINENO
expect "setup an empty redo file" "0" "$(cat "$appdata/redo" | tee output | wc -l)" $LINENO
expect "setup makes undo impossible" "false" "$(canUndo | tee output)" $LINENO
expect "setup makes redo impossible" "false" "$(canRedo | tee output)" $LINENO

# setpath

setpath "$browsedirb"
expect "setpath sets the expected path" "$browsedirb" "$(cat "$appdata/path" | tee output)" $LINENO
expect "setting the path creates an undo step" "1" "$(ursteps undo | tee output)" $LINENO
expect "setting the path does not create a redo step" "0" "$(ursteps redo | tee output)" $LINENO
expect "setting the path makes undo possible" "true" "$(canUndo | tee output)" $LINENO
expect "setting the path makes redo impossible" "false" "$(canRedo | tee output)" $LINENO

setpath "$browsedirc"
expect "setpath sets the expected path" "$browsedirc" "$(cat "$appdata/path" | tee output)" $LINENO
expect "setting the path creates an undo step" "2" "$(ursteps undo | tee output)" $LINENO
expect "setting the path does not create a redo step" "0" "$(ursteps redo | tee output)" $LINENO
expect "setting the path makes undo possible" "true" "$(canUndo | tee output)" $LINENO
expect "setting the path makes redo impossible" "false" "$(canRedo | tee output)" $LINENO

# undo
undo
expect "undo restores the previous path" "$browsedirb" "$(cat "$appdata/path" | tee output)" $LINENO
expect "undo steps" "1" "$(ursteps undo | tee output)" $LINENO
expect "redo steps" "1" "$(ursteps redo | tee output)" $LINENO
undo
expect "undo steps" "0" "$(ursteps undo | tee output)" $LINENO
expect "redo steps" "2" "$(ursteps redo | tee output)" $LINENO
expect "undo restores the previous path" "$browsedira" "$(cat "$appdata/path" | tee output)" $LINENO
redo
expect "undo restores the previous path" "$browsedirb" "$(cat "$appdata/path" | tee output)" $LINENO
expect "undo steps" "1" "$(ursteps undo | tee output)" $LINENO
expect "redo steps" "1" "$(ursteps redo | tee output)" $LINENO
redo
expect "undo restores the previous path" "$browsedirc" "$(cat "$appdata/path" | tee output)" $LINENO
expect "undo steps" "2" "$(ursteps undo | tee output)" $LINENO
expect "redo steps" "0" "$(ursteps redo | tee output)" $LINENO
expect "undo impossible" "true" "$(canUndo | tee output)" $LINENO
expect "redo impossible" "false" "$(canRedo | tee output)" $LINENO

