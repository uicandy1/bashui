#! /usr/bin/env bash
# The simplest (not the fastest) strategy is to implement
# a single updater of the view db (i.e. regenerate all).
# This is what we do here
set -e
# this is like refreshing the whole view data at once
fromPath=$(cat "$appdata/path")

pathentry () {
    echo string directory "$fromPath"
}

undoredo () {
    echo string canUndo "$(canUndo)"
    echo string canRedo "$(canRedo)"
}

leftlist () {
    echo lines locations label,action ___
    for l in $(find "$fromPath" -maxdepth 1 -mindepth 1 -type d -printf "%f\n")
    do
        echo "$l" \"action/onclickleft.sh \\\"$fromPath/$l\\\"\"
    done
    echo ___
}

mfolder=folder
mexe=application-x-executable
maudio=audio-x-generic
mfont=font-x-generic
mimage=image-x-generic
mpackage=package-x-generic
mhtml=text-html
mtext=text-x-generic
mtemplate=text-x-generic-template
mscript=text-x-script
mvideo=video-x-generic
maddress=x-office-address-book
mcalendar=x-office-calendar
mdocument=x-office-document
mpresentation=x-office-presentation
mspreadsheet=x-office-spreadsheet

declare -A exts=( 
    ["png"]=$mimage 
    ["svg"]=$mimage
    ["jpg"]=$mimage
    ["deb"]=$mpackage 
    ["html"]=$mhtml
    ["glade"]=$mdocument
    ["sh"]=$mscript )

fileicon () {
    local f=$1
    extension="${f##*.}"
    mime="${exts[$extension]}" 
    if [ -n "$mime" ]
    then
        echo "$mime"
        return 0 
    fi
    echo "x-office-document"
}

rightlist () {
    local f="%A %s %U %G %h " 
    echo lines files rights,size,user,group,links,label,action,selection,icon ___
    for l in $(find "$fromPath" -maxdepth 1 -mindepth 1 -type f -printf "%f\n")
    do
        stat --printf "$f" "$fromPath/$l"
        local fn="$(basename "$l")"
        echo \"$fn\" \"xdg-open \'$fromPath/$l\'\" \"action/onselectfile.sh \'$fromPath/$l\'\" $(fileicon "$l")
    done
    echo ___
}


fileattrs () {
    local fn="$1"
    local f='"rights" %A\n'
    f+='"device number" %d\n'
    f+='"file type" "%F"\n'
    f+='"user group" %G\n'
    f+='"hard links" %h\n'
    f+='"inode number" %i\n'
    f+='"mount point" %m\n'
    f+='"file name" %n\n'
    f+='"size" %s\n'
    f+='"user name" %U\n'
    f+='"creation date" %w\n'
    f+='"access date" %x\n'
    f+='"data modification date" %y\n'
    f+='"status change date" %z\n'
    stat --printf "$f" "$fn"
}

selectedfile () {
    local f
    # file attributes
    echo lines attributes name,value ___
    f="$(cat $appdata/file)" 
    if [ -z "$f" ]
    then
        echo -n ""
    else
        fileattrs "$f"
    fi
    echo ___

    # hex dump
    echo text hexdump ___
    xxd < "$f"
    echo ___

    # image preview
    if identify "$f" >/dev/null 2>&1
    then
        echo text image ___
        convert "$f" -resize 200x png:- | base64
        echo ___
    fi
}


all () {
    pathentry
    leftlist
    rightlist
    undoredo
}
# dump all in one shot
#(pathentry; leftlist) | stack exec uiCDb-exe -- --
#if [ -n "$1" ]
#then
#    echo UPDATE "$1"
#    "$1" | "$dbapp" --
#fi
#fromPath="$(pwd)"
#rightlist
if [ "$1" == "now" ]
then
    all
fi
