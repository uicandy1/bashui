#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"
settings <<EOF
Settings {
    root = Group "fileman.glade" [
            AppWindow "ID_MAIN",
            LabelText "MY_LABEL" (Reference ["directory"]),
            EntryActivated "PATH_ENTRY" (Literal (StringValue "activatePath") ),
            EntryText "PATH_ENTRY" (Reference ["directory"]),
            TreeViewListItems "LEFT_TREEVIEW" (Reference ["locations"]) 
                [
                    Column "Locations" (Reference ["label"]) Text
                ],
            RowActivated "LEFT_TREEVIEW" (Reference ["locations"]) (Reference ["action"]),
            TreeViewSelectionChanged "LEFT_TREEVIEW" (Reference ["locations"]) (Reference ["action"]),
            TreeViewListItems "RIGHT_TREEVIEW" (Reference ["files"]) 
                [
                    Column "" (Reference ["icon"]) Icon,
                    Column "Files" (Reference ["label"]) Text,
                    Column "Size" (Reference ["size"]) Text,
                    Column "Rights" (Reference ["rights"]) Text
                ],
            RowActivated "RIGHT_TREEVIEW" (Reference ["files"]) (Reference ["action"]),
            ContextMenu "RIGHT_TREEVIEW" (Reference ["fileCxtMenu"]) (MenuItemLabelCmd (Reference ["label"]) (Reference ["action"])),
            EntryChanged "MKDIR_ENTRY" (Literal (StringValue "mkdirpath")),
            DialogOnChange "ID_CREATEFOLDER" (Reference ["popCreateFolder"]),
            ButtonActivated "CREATE_DIRECTORY" (Literal (StringValue "createfolder")),
            HideOnDelete "ID_CREATEFOLDER",
            IconViewListItems "RIGHT_ICONVIEW" (Reference ["files"]) 
                [
                    Column "Text" (Reference ["label"]) Text,
                    Column "" (Reference ["icon"]) Icon
                ],
            TreeViewSelectionChanged "RIGHT_TREEVIEW" (Reference ["files"]) (Reference ["selection"]),
            ToolButtonActivated "BACK_BUTTON" (Literal (StringValue "undo") ),
            ToolButtonActivated "FORWARD_BUTTON" (Literal (StringValue "redo") ),
            WidgetSensitive "BACK_BUTTON" (Reference ["canUndo"]),
            WidgetSensitive "FORWARD_BUTTON" (Reference ["canRedo"]),
            TreeViewListItems "FILE_ATTRIBUTES" (Reference ["attributes"]) 
                [
                    Column "Attribute" (Reference ["name"]) Text,
                    Column "Value" (Reference ["value"]) Text
                ],
            TextViewText "HEXDUMP_TEXTVIEW" (Reference ["hexdump"]),
            ImageBase64 "PREVIEW_IMAGE" (Reference ["image"]),
            ApplicationReady (Literal (StringValue "initwindow"))
        ], 
    db = Tree (fromList [
            ("directory",Leaf (StringValue "$(pwd)")),
            ("canUndo", Leaf (IntValue 0)),
            ("canRedo", Leaf (IntValue 0)),
            ("locations",Tree (fromList [
                ("1",Tree (fromList [
                    ]))
                ])),
            ("files",Tree (fromList [
                ("1",Tree (fromList [
                    ]))
                ])),
            ("attributes",Tree (fromList [
                ("1",Tree (fromList [
                    ]))
                ])),
            ("fileCxtMenu",Tree (fromList [
                ])),
            ("popCreateFolder", Leaf (IntValue 0)),
            ("hexdump", Leaf (StringValue "")),
            ("image", Leaf (StringValue ""))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

initdb <<EOF
Tree (fromList [
        ("path", Leaf (StringValue "$(pwd)")),
        ("file", Leaf (StringValue "")),
        ("undo", Tree (fromList [])),
        ("redo", Tree (fromList []))
    ])
EOF

notifications <<EOF
[
    Watch Haskell [Item [] [["path"]] ChangedRows] "updatePath",
    Watch Haskell [Item [] [["file"]] ChangedRows] "updateFile"
]
EOF

# handlers
# --------
setpath () {
    local content="$1"
    local tr=$(beginTransaction "set path")
    local oldpath=$( ( echo get path | trDb "$tr" ) )
    trDb "$tr" <<EOF
prependlines undo path ____
$oldpath
____
lines redo path ____
____
string path $content
EOF
    endTransaction "$tr"
}
export -f setpath
activatePath () {
    local content=$(cat)
    setpath "$content"
}
export -f activatePath
selectPath () {
    local content="$1"
    setpath "$content"
}
export -f selectPath
selectFile () {
    local file="$1"

    echo string file "$file" | statedb "select file as $file"
}
export -f selectFile

undo () {
    local tr=$(beginTransaction "undo")
    local oldpath=$( ( echo get path | trDb "$tr") )
    local newpath=$( ( echo get "undo.#0.path" | trDb "$tr" ) )
    trDb "$tr" <<EOF
del undo.#0
string path $newpath
prependlines redo path ____
$oldpath
____
EOF
    endTransaction "$tr"
}
export -f undo

redo () {
    local tr=$(beginTransaction "redo")
    local oldpath=$( ( echo get path | trDb "$tr" ) )
    local newpath=$( ( echo get "redo.#0.path" | trDb "$tr" ) )
    trDb "$tr" <<EOF
del redo.#0
string path $newpath
prependlines undo path ____
$oldpath
____
EOF
    endTransaction "$tr"
}
export -f redo

initwindow () {
    ./update.sh | uidb "initwindow"
}
export -f initwindow

launchterminal () {
    inpath=$1
    cd $inpath
    x-terminal-emulator    
}
export -f launchterminal
mkdirpath () {
    path="$(cat)"
    echo "string mkdirpath $path" | statedb "update mkdir path"
}
export -f mkdirpath
mkdirdialog () {
    local old=$( ( echo get popCreateFolder | uidb ) )
    echo "int popCreateFolder $(( old + 1 ))" | uidb "pop create folder dialog"
}
export -f mkdirdialog

createfolder () {
    local tr=$(beginTransaction "get paths")
    local oldpath=$( ( echo get path | trDb "$tr" ) )
    local mkdirpath=$( ( echo get mkdirpath | trDb "$tr" ) ) 
    endTransaction "$tr"
    if [[ ! -z "$mkdirpath" ]]
    then
        cd "$oldpah"
        mkdir -p "$mkdirpath"
        updatePath
    fi
 }
export -f createfolder

# notifications
# -------------
updatePath () {
    ./update.sh | uidb "updatepath"
}
export -f updatePath
updateFile () {
    ./updatefile.sh
}
export -f updateFile

run
