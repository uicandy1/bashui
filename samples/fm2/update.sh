#! /usr/bin/env bash
# The simplest (not the fastest) strategy is to implement
# a single updater of the view db (i.e. regenerate all).
# This is what we do here
set -e
# this is like refreshing the whole view data at once
fromPath=$( ( echo get "path" | statedb ) )
pathentry () {
    echo string directory "$fromPath"
}
truefalse () {
    if [ -z "$1" ]
    then
        echo false
    else
        echo true
    fi
}
canUndo () {
    local can=$( ( echo get "undo.#0" | statedb ) )
    truefalse "$can"
}
canRedo () {
    local can=$( ( echo get "redo.#0" | statedb ) )
    truefalse "$can"
}
undoredo () {
    echo string canUndo "$(canUndo)"
    echo string canRedo "$(canRedo)"
}

leftlist () {
    echo lines locations label,action ___
    for l in $(find "$fromPath" -maxdepth 1 -mindepth 1 -type d -printf "%f\n")
    do
        echo "$l" \"selectPath \\\"$fromPath/$l\\\"\"
    done
    echo ___
}

mfolder=folder
mexe=application-x-executable
maudio=audio-x-generic
mfont=font-x-generic
mimage=image-x-generic
mpackage=package-x-generic
mhtml=text-html
mtext=text-x-generic
mtemplate=text-x-generic-template
mscript=text-x-script
mvideo=video-x-generic
maddress=x-office-address-book
mcalendar=x-office-calendar
mdocument=x-office-document
mpresentation=x-office-presentation
mspreadsheet=x-office-spreadsheet

declare -A exts=( 
    ["png"]=$mimage 
    ["svg"]=$mimage
    ["jpg"]=$mimage
    ["deb"]=$mpackage 
    ["html"]=$mhtml
    ["glade"]=$mdocument
    ["sh"]=$mscript )

fileicon () {
    local f=$1
    extension="${f##*.}"
    mime="${exts[$extension]}" 
    if [ -n "$mime" ]
    then
        echo "$mime"
        return 0 
    fi
    echo "x-office-document"
}

rightlist () {
    local f="%A %s %U %G %h " 
    echo lines files rights,size,user,group,links,label,action,selection,icon ___
    for l in $(find "$fromPath" -iname "[!.]*" -maxdepth 1 -mindepth 1 -type f -printf "%f\n")
    do
        stat --printf "$f" "$fromPath/$l"
        local fn="$(basename "$l")"
        echo \"$fn\" \"xdg-open \'$fromPath/$l\'\" \"selectFile \'$fromPath/$l\'\" $(fileicon "$l")
    done
    echo ___
}
contextmenu () {
    cat <<EOF
lines fileCxtMenu label,action ____
"Open in terminal" "launchterminal '$fromPath'"
"Create Directory" "mkdirdialog '$fromPath'"
____
EOF
}

all () {
    pathentry
    leftlist
    rightlist
    undoredo
    contextmenu
}
all
