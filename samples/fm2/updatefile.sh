#! /usr/bin/env bash
set -e
fromFile=$( ( echo get "file" | statedb ) )

fileattrs () {
    local fn="$1"
    echo \"content\" \"$(file -b  "$fn")\"
    local f='"rights" %A\n'
    f+='"device number" %d\n'
    f+='"file type" "%F"\n'
    f+='"user group" %G\n'
    f+='"hard links" %h\n'
    f+='"inode number" %i\n'
    f+='"mount point" %m\n'
    f+='"file name" %n\n'
    f+='"size" %s\n'
    f+='"user name" %U\n'
    f+='"creation date" %w\n'
    f+='"access date" %x\n'
    f+='"data modification date" %y\n'
    f+='"status change date" %z\n'
    stat --printf "$f" "$fn"
}
hexdump () {
    local f
    f="$1" 
    # hex dump
    echo text hexdump ___
    xxd < "$f"
    echo ___
}
attrs () {
    local f
    f="$1" 
    echo lines attributes name,value ___
    fileattrs "$f"
    echo ___
}
preview () {
    local f
    f="$1" 
    # image preview
    if identify "$f" >/dev/null 2>&1
    then
        echo text image ___
        convert "$f" -resize 200x png:- | base64
        echo ___
    fi
}
if [ ! -f "$fromFile" ]
then
    (
        echo lines attributes name,value ___
        echo ___
        echo text hexdump ___
        echo ___
        echo text image ___
        echo ___
    ) | uidb "allinone"
else
    attrs "$fromFile" | uidb "attrs" &
    hexdump "$fromFile" | uidb "hexdump" &
    preview "$fromFile" | uidb "preview" &
fi
