#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "hangman.glade" [
            AppWindow "ID_MAIN",
            EntryChanged "ID_ENTRY" (Literal (StringValue "changed")),
            TreeViewListItems "ID_TREE" (Reference ["solutions"]) 
                [
                    Column "Word" (Reference ["word"]) Text
                ]
        ], 
    db = Tree (fromList [
            ("solutions", Tree (fromList [
            ]))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

# handlers
# --------
changed () {
    local content=$(cat) 
    local pattern="^$(echo "$content" | sed "s/[ -.?*_]/./g")\$"
    uidb "searching $content"<<EOF
lines solutions word ____
$(grep -ie "$pattern" "/usr/share/dict/words" || echo -n '""')
____
EOF
}
export -f "changed"

# run it
# ------
runui
