#! /usr/bin/env bash
set -e
script="$(readlink -f "$(dirname $0)")"
source "$script/testlib.sh"
#tmppath="$(mktemp -d)"
tmppath=/tmp/uicandytest
mkdir -p "$tmppath" 
export tmppath
export uiSocketPath="$tmppath/ui.ipc"
export stateSocketPath="$tmppath/state.ipc"

settings_path="$tmppath/settings"
initdb_path="$tmppath/initdb"
notifications_path="$tmppath/notifications"
loglevel=${LOGLEVEL-WARNING}


statedb () {
    local change=${1-unnamed} 
    candy -c "$change" -s "$stateSocketPath" --
}

cat >"$initdb_path" <<EOF
    Tree (fromList [ ])
EOF

cat >"$notifications_path" <<EOF
[
]
EOF

# start the state db
state -l "$loglevel" -s "$stateSocketPath" -n "$notifications_path" -d "$initdb_path" &
stateid=$!

sleep 2

topic "candy command documentation"
candy --help | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | code

# do some tests
# -------------
#--------------
spec "initially there should be one step in the history" $(statedb <<< history | sed '/^$/d' | wc -l) "1"

#--------------
topic "Setting a string"
doc <<'EOF'
`string key value` can be used to assign a value to a key in the db. Key can contain
dots to access a nested value. Value can be quoted.
EOF
echo "string abc 123" | snippet | statedb
spec "string sets a string" $(statedb <<< "get abc") "123"

#--------------
spec "after one change there should be two steps  in the history" $(statedb <<< history | sed '/^$/d' | wc -l) "2"

#-------------
topic "Reading as a string"
doc <<'EOF'
`get key` can be used to read the value of a key. Key dan contain dots to access
a nested value.
EOF
echo "get abc" | snippet 
echo "get abc" | statedb | result 

#--------------
topic "History of changes"
doc <<'EOF'
`history` will display a list of changes in the current db. The purpose of this is
that it is possible to look at any value in the db at any point in the history.
EOF
echo "history" | snippet 
echo "history" | statedb | result

#--------------
statedb <<< "string abc 123"
spec "after two changes there should be three steps  in the history" $(statedb <<< history | sed '/^$/d' | wc -l) "3"
(statedb <<< history | sed '/^$/d' | head -n 2 | awk '{print $1}') | 
    (read a; read b;  spec "two identical changes produce the same hash" "$a" "$b")

#--------------
# FIXME, disgusting
spec "FIXME, count returns an ugly ass haskell array" "$(statedb <<< "count abc")" '[[IntValue 1]]'

#--------------
topic "Setting multiple lines"
doc <<'EOF'
It is possible to have ordered lists of items in the database. These will use a special
key scheme: #1, #2, .. #n.
The `lines` command can be used to initialize a sequencial list of items.
EOF
cat >linecmd <<'EOF'
lines thelines thetext ___
"first line"
"second line"
"third line"
"fourth \" line"
___
EOF
cat linecmd | snippet | statedb

spec "lines sets multiple lines" "$(statedb <<< "get thelines" | sed -n 1p)" "first line"
spec "lines sets multiple lines that can contain quotes" "$(statedb <<< "get thelines" | sed -n 4p)" "fourth \" line"

#--------------
topic "Appending lines to a list"
doc <<'EOF'
It is possible to append lines to a list of items.
EOF

cat >linecmd <<'EOF'
appendlines thelines thetext ___
"one more"
"and the last one"
___
EOF
cat linecmd | snippet | statedb

spec "appendlines adds  multiple lines at the end of a list" "$(statedb <<< "get thelines" | sed -n 6p)" "and the last one"

#--------------
topic "Prepending lines to a list"
doc <<'EOF'
It is possible to prepend lines to a list of items.
EOF
cat >linecmd <<'EOF'
prependlines thelines thetext ___
"real first one"
"real second one"
___
EOF
cat linecmd | snippet | statedb

spec "prepend lines adds  multiple lines at the top of a list" "$(statedb <<< "get thelines" | sed -n 1p)" "real first one"
spec "prepend lines adds  multiple lines at the top of a list" "$(statedb <<< "get thelines" | sed -n 2p)" "real second one"
spec "prepend lines adds  multiple lines at the top of a list" "$(statedb <<< "get thelines" | sed -n 8p)" "and the last one"

#--------------
topic "Counting the lines in a list"
doc <<'EOF'
The `count` query returns the number of lines in a list
EOF
echo "count thelines" | snippet 
echo "count thelines" | statedb | result
spec "count returns the expected number of lines" "$(statedb <<< "count thelines")" '[[IntValue 8]]'

statedb <<< "history"

#--------------
topic "Multiple fields per line"
doc <<'EOF'
All line commands (`lines`, `appendlines`, `prependlines`) allow the insertion
of multiple fields per line.
EOF
cat >linecmd <<'EOF'
lines thecolumns firstfield,secondfield  ___
"line1 field 1" "line1 field 2"
"line2 field 1" "line2 field 2"
___
EOF
cat linecmd | snippet | statedb
doc <<'EOF'
Now if we try `get thecolumns` we will get:
EOF

echo "get thecolumns" | statedb | result 

doc <<'EOF'
For structured content get is no very convenient but `geth` and
`getj` can be used. 

`getj thecolumns` returns structured data formatted as json
EOF
echo "getj thecolumns" | statedb | result 

doc <<'EOF'
`geth thecolumns` returns structured data formatted with the Haskell read format
EOF
echo "geth thecolumns" | statedb | result 


#at           partialhash comment start a transaction at a partialhash
#commit                           commit the current transaction
#count        path                count the children at path
#del          path                remove the selected key from the database at path
#get          path                return the value of a given key as a string
#geth         path                return the value of a given key as Haskell read format
#getj         path                return the value of a given key as json
#help                             show help information
#lines        path columns sep    set lines at path
#history                          display the change history of the db
#patch        path sep ...        sync under path with the provided changes
#prependLines path columns sep    add some lines before the existing line at path
#quit                             quit the application
#redo                             redo (the whole history) one step
#rollback                         rollback the current transaction
#select       fields from path    select fields from path
#transact     comment             start a transaction at the tip
#undo                             undo (the whole history) one step


# terminate the state db
kill "$stateid"
rm -fr "$tmppath"


# FIXME
# -notifications optional
# -initial db optional 
# -notification of readiness on state db
# -stupid ok why we display that in non interactive mode
