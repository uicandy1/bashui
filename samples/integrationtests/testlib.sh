doctarget=${DOC:-/dev/null}
echo "">"$doctarget"
# test functon
spec () {
    msg=$1
    expected=$2
    observed=$3
    if [[ "$expected" != "$observed" ]]
    then
        echo "FAILED: $msg (expected \"$expected\", got \"$observed\")"
    fi
}
# document a function topic
topic () {
    echo "# $1" >>"$doctarget"
    echo "" >>"$doctarget"
}
# document a function usage
doc () {
    cat >>"$doctarget"
    echo "" >>"$doctarget"
}
# document some code
code () {
    echo '```' >>"$doctarget"
    cat >>"$doctarget"
    echo '```' >>"$doctarget"
}
# set the snippet for the documentation of a function
snippet () {
    cat >>"$doctarget"  <<'EOF'
### code
```
EOF
    tee -a "$doctarget"
    cat >>"$doctarget" <<'EOF'
```

EOF
}
# the output of a snipet of code
result () {
    echo '### result' >>"$doctarget"
    echo "" >>"$doctarget"
    echo '```' >>"$doctarget" 
    tee -a "$doctarget"
    echo '```' >>"$doctarget" 
    echo "" >>"$doctarget"
}
