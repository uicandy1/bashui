#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "notes.glade" [
            AppWindow "ID_MAIN",
            ButtonActivated "ID_ADD" (Literal (StringValue "create")),
            ButtonActivated "ID_DELETE" (Literal (StringValue "delete")),
            ButtonActivated "ID_UPDATE" (Literal (StringValue "update")),
            EntryText "ID_NOTEENTRY" (Reference ["note"]),
            EntryChanged "ID_NOTEENTRY" (Literal (StringValue "changed")),
            TreeViewListItems "ID_NOTES" (Reference ["notes"]) 
                [
                    Column "Note" (Reference ["note"]) Text
                ],
            TreeViewSelectionChanged "ID_NOTES" (Reference ["notes"])(Literal (StringValue "selectnote")),
            TreeViewSelectionCleared "ID_NOTES" (Literal (StringValue "deselect"))
        ], 
    db = Tree (fromList [
            ("note", Leaf (StringValue "")),
            ("selection", Leaf (StringValue "")),
            ("notes", Tree (fromList [
            ]))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

initdb <<EOF
Tree (fromList [
            ("note", Leaf (StringValue "")),
            ("notes", Tree (fromList [
            ]))
        ])
EOF

notifications <<EOF
[
    Watch Haskell [Item ["notes"] [["note"]] ChangedRows] "ui_updatelist"
]
EOF

# handlers
# --------

changed () {
    local content=$(cat) 
    echo string note "$content" | statedb "note changed $content"
}
export -f "changed"

setnote () {
    (
        echo appendlines notes note ___
        echo ___
    ) | statedb
}
export -f setnote

selectnote () {
    id=$1
    echo string selection "$id" | statedb "select $id"
    # we want to get the selected note
    local notetext=$( ( echo get "notes.$id" | statedb ) )
    echo string "note" "$notetext" | uidb
}
export -f selectnote

deselect () {
    echo string selection | statedb "deselect clear selection"
    echo string note | statedb "deselect clear text"
    echo string note | uidb "deselect clear text"
}
export -f deselect

create() {
    local created=$( (
        echo get note 
    ) | statedb )
    statedb "create note $created" <<EOF
appendlines notes note ___
"$created"
___
EOF
}
export -f create

update() {
    local selected=$( (
        echo get selection 
    ) | statedb )
    local created=$( (
        echo get note 
    ) | candy -s "$stateSocketPath" -- )
    echo "string notes.$selected.note $created" | statedb "update note"
}
export -f update

delete() {
    local selected=$( (
        echo get selection 
    ) | statedb )
    echo del "notes.$selected" | statedb "delete note $selected"
    echo string note | statedb "delete note clear text"
    echo string note  | uidb "delete note clear text"
}
export -f delete

# notifications
# -------------
ui_updatelist () {
    (
        echo "patch notes _____"
        while read l 
        do
            if [ ! -z "$l" ]
            then
                echo "$l"
            fi
        done
        echo "_____"
    ) | uidb "full update"
}
export -f ui_updatelist

# run it
# ------
echo $uiSocketPath

run
