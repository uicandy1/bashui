#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "ui2.glade" [
            AppWindow "ID_MAIN"
        ],
    db = Tree (fromList [
            ("entrytext", Leaf (StringValue "")),
            ("bogustext", Leaf (StringValue "bogus text"))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

runui2
