#! /usr/bin/env bash
set -e
script="$(readlink -f "$(dirname $0)")"
tmppath="$(mktemp -d)"
export tmppath
export uiSocketPath="$tmppath/ui.ipc"
export stateSocketPath="$tmppath/state.ipc"

settings_path="$tmppath/settings"
initdb_path="$tmppath/initdb"
notifications_path="$tmppath/notifications"
loglevel=${LOGLEVEL-WARNING}

settings () {
    cat >"$settings_path"
}

initdb () {
    cat >"$initdb_path"
}
notifications () {
    cat >"$notifications_path"
}

statedb () {
    local change=${1-unnamed} 
    candy -c "$change" -s "$stateSocketPath" --
}
export -f statedb

uidb () {
    local change=${1-unnamed} 
    candy -c "$change" -s "$uiSocketPath" --
}
export -f uidb
trDb () {
    candy -x -s "$(echo "$1" | sed 's/:.*$//')" --
}
export -f trDb
beginTransaction () {
    local name="$tmppath/trans$1$RANDOM.ipc"
    local transactionName="${1:unnamed}"
    candy -c "$transactionName" -t "$name" -s "$stateSocketPath" 1>/dev/null 2>&1 &
    local pid=$!
    echo "$name:$pid"
}
export -f beginTransaction
endTransaction () {
    echo "commit" | trDb "$1"
    local pid=$(echo $1 | sed 's/[^:]*://')
    local socket=$(echo "$1" | sed 's/:.*$//')
    echo "($1) wait for $pid rm $socket"
    wait "$pid"
    rm "$socket"
}
export -f endTransaction
runstart () {
    # launch the state db
    # -------------------
    state -l "$loglevel" -s "$stateSocketPath" -n "$notifications_path" -d "$initdb_path" &
    stateid=$!
    # launch the ui db
    # ----------------
    ui "$settings_path" -l "$loglevel" & 
    uipid=$!
}
runwait () {
    wait "$uipid"
    kill "$stateid"
    rm -fr "$tmppath"
}
run () {
    runstart
    runwait
}
# no state db, only ui
runstartui () {
    ui "$settings_path" -l "$loglevel" &
    uipid=$!
}
runwaitui() {
    wait "$uipid"
    rm -fr "$tmppath"
}
runui () {
    runstartui
    runwaitui
}
runui2 () {
    ui2 "$settings_path" -l "$loglevel"
}
