#! /usr/bin/env bash
depth=$1
# read x y from the command line
read -a arr
x=${arr[0]}
y=${arr[1]}
newshape () {
    echo appendlines shapes x,y,depth,shape ___
    echo "$x" "$y" "$depth" rectangle
    echo ___
}
newshape |  "$dbapp" -c "add rectangle" -s "$statePath" --
