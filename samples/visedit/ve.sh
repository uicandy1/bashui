#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "vis.glade" [
            AppWindow "ID_MAIN",
            ImageSvg "ID_DRAWING" (Reference ["shapes"]),
            MouseButtonPress "ID_DRAWING_EVENTS" (Reference ["mouseclick"]),
            MenuItemActivated "ID_UNDO" (Reference ["undo"]),
            MenuItemActivated "ID_REDO" (Reference ["redo"]),
            MenuItemLabel "ID_UNDO" (Reference ["undoLabel"]),
            MenuItemLabel "ID_REDO" (Reference ["redoLabel"]),
            WidgetSensitive "ID_UNDO" (Reference ["undoAvailable"]),
            WidgetSensitive "ID_REDO" (Reference ["redoAvailable"])
        ], 
    db = Tree (fromList [
            ("mouseclick", Leaf (StringValue "./commands/ve-add.sh 0")),
            ("undo", Leaf (StringValue "./commands/ve-undo.sh")),
            ("redo", Leaf (StringValue "./commands/ve-redo.sh")),
            ("shapes", Leaf (StringValue "")),
            ("undoLabel", Leaf (StringValue "Undo")),
            ("redoLabel", Leaf (StringValue "Redo")),
            ("undoAvailable", Leaf (IntValue 1)),
            ("redoAvailable", Leaf (IntValue 1))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

initdb <<EOF
Tree (fromList [
            ("shapes", Tree (fromList []))
        ]
    )
EOF

notifications <<EOF
[
Watch ShellFriendly [Item ["shapes"] [["depth"], ["shape"], ["x"], ["y"]] Rows] "./view/view.sh vev-svg.sh"
]
EOF

# run it
# ------
export mainapp=ui
export dbapp=candy
export stateapp=state
export socketPath="$uiSocketPath"
export statePath="$stateSocketPath"


run

