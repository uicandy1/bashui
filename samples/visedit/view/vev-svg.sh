#! /usr/bin/env bash
# generate svg rectangle
rectangle () {
    local x=$1
    local y=$2
    cat <<EOF
<rect x="$x" y="$y" width="300" height="100" style="fill:rgb(0,0,255);stroke-width:3;stroke:rgb(0,0,0)" />
EOF
}

zoo="$(cat)"
echo text shapes ___
echo "<svg width="400" height="300">"
echo "$zoo"  | while read  d c x y
do
    $c $x $y
done
echo "</svg>"
echo ___
echo string mouseclick ./commands/ve-add.sh 1

