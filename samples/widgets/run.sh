#! /usr/bin/env bash
# note: normally, uicandy.sh would be in the path
script="$(readlink -f "$(dirname $0)")"
source "$script/../uicandy.sh"

settings <<EOF
Settings {
    root = Group "vis.glade" [
            AppWindow "ID_MAIN",
            EntryChanged "ID_ENTRY" (Literal (StringValue "notify")),
            ButtonText "ID_BUTTON" (Reference ["entrytext"]),
            ButtonActivated "ID_BUTTON" (Literal (StringValue "button")),
            RangeMinMax "ID_SCALE" (Literal (FloatValue 0.0)) (Literal (FloatValue 100.0)),
            RangeValueChange "ID_SCALE" (Literal (StringValue "notify")),
            SpinButtonMinMax "ID_SPIN" (Literal (FloatValue 0.0)) (Literal (FloatValue 100.0)),
            SpinButtonIncrements "ID_SPIN" (Literal (FloatValue 1.0)) (Literal (FloatValue 10.0)),
            SpinButtonSpinned "ID_SPIN" (Literal (StringValue "notify")),
            LabelText "ID_LABEL" (Reference ["bogustext"]),
            ToggleButtonToggled "ID_CHECK_PIZZA" (Literal (StringValue "notify 'pizza: '")),
            ToggleButtonToggled "ID_RADIO1" (Literal (StringValue "notify 'r1: '")),
            ToggleButtonToggled "ID_RADIO2" (Literal (StringValue "notify 'r2: '")),
            ToggleButtonToggled "ID_RADIO3" (Literal (StringValue "notify 'r3: '")),
            ComboBoxModel "ID_COMBO" (Reference ["comboitems"]) [Column "Blah" (Reference ["thestring"]) Text],
            ComboBoxSelectionChanged "ID_COMBO" (Reference ["comboitems"]) (Literal (StringValue "notify 'combo: '")) (Reference ["thestring"])
        ], 
    db = Tree (fromList [
            ("entrytext", Leaf (StringValue "")),
            ("bogustext", Leaf (StringValue "bogus text")),
            ("comboitems", Tree (fromList [
                (#0, Tree (fromList [ ("thestring", Leaf (StringValue "blah blah item1")) ])),
                (#1, Tree (fromList [ ("thestring", Leaf (StringValue "blah blah item2")) ]))
                ]))
            ]
        ),
    socketPath = "$uiSocketPath"
}
EOF

# handlers
# --------
notify () {
    local msg=$(cat)
    local arg=$1
    cat | uidb "set string $arg$msg" <<EOF
string entrytext $arg$msg
EOF
}
export -f notify
button () {
    local val=$(echo "get entrytext" | uidb)
    cat | uidb "set bogustext $val" <<EOF
string bogustext $val
EOF
}
export -f button
# run it
# ------
runui

