{-|
Module      : Main
Description : State db server
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

State db server
-}
module Main where
import Data.IORef(newIORef, IORef)
import Data.Semigroup ((<>))
import MDb
import Notifications
import Options.Applicative
import Options.Applicative.Help.Pretty
import Server.DbIO(server, createJournalRef)
import Server.Logger
import System.Exit(ExitCode(..), exitWith)
import System.Log.Logger
import Text.Read(readMaybe)
import Server.Types

-- | Exit code
invalidSettings = ExitFailure 1

-- | Server configuraion
data ServerSettings = ServerSettings
  { socket     :: String
  , notifications    :: String
  , database :: String
  , logLevel :: Priority
  }
  deriving Show

-- | Runs i ihe gui based on some settings
runDb :: ServerSettings -> UiDb -> IO ()
runDb ss ddb = do
    infoM logContext "loading watches"
    maybeN <- loadWatches (notifications ss)
    maybe
        (do
            errorM logContext "Could not load the notifications"
            return ()
        )
        (\ n -> do
            infoM logContext $ "notifications " ++ (show n)
            ioRef <- newIORef ddb
            infoM logContext  "starting server"
            dbioref <- createJournalRef ddb "initial db"
            server (socket ss) dbioref (process n ioRef)
            infoM logContext  "exiting")
        maybeN
    where
        logContext = "runDb"
        process :: [Watch] -> IORef UiDb -> UiDb -> UiDb -> IO ()
        process n ioRef = 
            callWatches n 

-- | Generate an option parser
optParser :: Parser ServerSettings
optParser = ServerSettings
      <$> strOption
          ( long "socket"
         <> short 's'
         <> metavar "SOCKET"
         <> help "Unix domain socket path" )
      <*> strOption
          ( long "notifications"
         <> short 'n'
         <> help "Notification settings path" )
      <*> strOption
          ( long "db"
         <> short 'd'
         <> value ""
         <> help "Initial DB path" )
      <*> option auto ( 
          ( long "loglevel"
         <> short 'l'
         <> value WARNING
         <> help "Log level (DEBUG, INFO, NOTICE, WARNING, ERROR, CRITICAL, ALERT, EMERGENCY" ))

-- | Parse the options
opts :: ParserInfo ServerSettings
opts = info (optParser <**> helper)
  ( fullDesc
  <> header "state - an in memory database for keeping the state of interactive applications"
  <> progDesc "Start the state database server"
  <> (footerDoc . Just) 
        (text "Waits for connections on a unix domain socket. Each connection acts as \n\
        \an atomic transaction (all queries performed during this connection \n\
        \will either succeed or fail as whole). The database uses immutable data: \n\
        \the full history of changes is kept and every change creates a new version \n\
        \of the data. The version history can be retrieved. Previous versions can be \n\
        \inspected." </> text "" </>
        text "The candy program can be used to send request to the state program." </> text "" </>
        text "See also: candy")
     )

-- | Run the state application (program state database)
main :: IO ()
main = do
    options <- execParser opts
    setupLogger "sdb" $ logLevel options
    infoM "main" $ "options = " ++ show options
    let dbpath = database options 
    if dbpath /= "" then do
            text <- readFile dbpath
            maybe (do
                    errorM "main" $ "could not read db file" ++ dbpath
                    exitWith invalidSettings
                    return () )
                (runDb options)
                (readMaybe text)
        else do
            infoM "main" $ "no dbpath, using empty db"
            runDb options MDb.empty
