{-|
Module      : Notifications
Description : State db notifications
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The state db triggers notifications (when the db
changes)
-}
module Notifications (
    Watch(..),
    loadWatches,
    callWatches
    ) where
import Data.Maybe(fromMaybe)
import Diff
import MDb
import System.Process 
import Text.Read(readMaybe)
import Server.Types

-- | Transmission policy for changes on selected paths
data Mode = 
    -- | Everything that is selected is transmitted in full when it changes
    Rows |
    -- | When a list is selected, the crud operation (create update delete) will
    -- be transmitted with the changes only for changed rows
    ChangedRows
    --Objects   -- git like think, where Trees are a list of hashes and objects are
                -- transmitted with their hash allowing a fast sync
    deriving (Read, Show)

-- | Item (path to watch) (from this path, paths of selected columns)
data Item = Item DbPath [DbPath] Mode
    deriving (Read, Show)

-- | Encoding of the notification
data Encoding = Binary | Haskell | ShellFriendly -- | Json | Yaml | Protobuf
    deriving (Read, Show)

-- | Watch
data Watch = Watch Encoding [Item] String
    deriving (Read, Show)

-- | Return watch paths from a watch
watchPaths :: Watch -> [DbPath]
watchPaths (Watch _ i _) = 
    (\ (Item p _ _) -> p) <$> i

-- | Return watch command from a watch
watchCommand :: Watch -> String
watchCommand (Watch _ _ c) = c

-- | Read settings from a file
loadWatches :: FilePath -> IO (Maybe [Watch])
loadWatches path = do
    text <- readFile path
    return $ readMaybe text

-- | Stringify a db value
stringify :: DbValue -> String
stringify (StringValue s) = s
stringify (IntValue s) = show s
stringify (FloatValue s) = show s

-- | Serialize a table of values
serializeValues :: Encoding -> Maybe [[ DbValue ]] -> String
serializeValues _ Nothing = "??"
serializeValues Haskell (Just values) =
    show values
serializeValues ShellFriendly (Just values) =
    terminate "\n" (row <$> values)
    where 
        row :: [DbValue] -> String
        row r =
            unwords (stringify <$> r)

-- | Serialize a single diff
serializeDiff :: Encoding -> [DbPath] -> DbDiff DbKey UiDb -> String
serializeDiff ShellFriendly p (Create k v) =
    unwords $ ["create", show k] ++  maybe [] (fmap show) (justSelectRowColumns p v)
serializeDiff ShellFriendly p (Update k v) =
    unwords $ ["update", show k] ++  maybe [] (fmap show) (justSelectRowColumns p v)
serializeDiff ShellFriendly p (Diff.Delete k) =
    unwords ["delete", show k]

-- | terminate each line by the provided terminator and concatenate the result
terminate :: String -> [String] -> String
terminate t lines =
    concat $ (++ t) <$> lines

-- | Serializes diffs
serializeDiffs :: Encoding -> [DbPath] -> [DbDiff DbKey UiDb] -> String
serializeDiffs Haskell _ diffs = show diffs

serializeDiffs e i diffs =
    terminate "\n" $ serializeDiff e i <$> diffs

-- | Select
selectValues :: UiDb -> Item -> Maybe [[ DbValue ]]
selectValues db (Item dbpath keys _) = do
    subDb <- deepLookup dbpath db
    justSelect keys subDb

-- | Serializes a single item (an item being a "Path" + some data do select at this path
serializeItem :: Encoding -> UiDb -> UiDb -> Item -> String
serializeItem encoding _ db item@(Item dbpath keys Rows) = 
    serializeValues encoding $ selectValues db item

serializeItem encoding prevDb db item@(Item dbpath keys ChangedRows) = 
    serializeDiffs encoding keys diffs
    where
        p = fromMaybe MDb.empty $ deepLookup dbpath prevDb
        n = fromMaybe MDb.empty $ deepLookup dbpath db 
        -- FIXME: this will not work if we don't have 2 trees
        diffs :: [DbDiff DbKey UiDb]
        diffs = if isTree p && isTree n then diffTree p n else []
        --diffs = trace (show (dbpath, p, n, diffTree p n)) $ if isTree p && isTree n then diffTree p n else []

-- | call a single watch
callWatch :: UiDb -> UiDb -> Watch -> IO ()
callWatch prevDb db (Watch encoding items command) = do
    (exitCode, stdout, stderr) <- readCreateProcessWithExitCode createP serialized
    putStrLn stdout
    return ()
    where
        serialized = terminate "\n\n" $ serializeItem encoding prevDb db <$> items

        createP = CreateProcess {  
            cmdspec = RawCommand "bash" ["-c", command],
            cwd = Nothing,
            env = Nothing,
            std_in = CreatePipe,
            std_out = CreatePipe,
            std_err = CreatePipe,
            close_fds = False,
            create_group = False,
            delegate_ctlc = False,
            detach_console = False,
            create_new_console = False,
            new_session = False,
            child_group = Nothing,
            child_user = Nothing,
            use_process_jobs = False
            }
-- | call the necessary notifications based on the db changes
callWatches :: [Watch] -> UiDb -> UiDb -> IO ()
callWatches w prevDb newDb =
    mapM_ (callWatch prevDb newDb) (filter (notificationApplicable prevDb newDb) w)
    where 
        notificationApplicable :: UiDb -> UiDb -> Watch -> Bool
        notificationApplicable dbFrom dbTo watch =
            someKeyChanged dbFrom dbTo (watchPaths watch)
