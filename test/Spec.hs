import Test.QuickCheck
import MDb
import Server.Types
import Control.Monad(liftM)
import Data.Binary (Binary, encode, decode)
import Data.Either (fromRight, isLeft)
import qualified Data.Map.Strict as M
import Data.List
import System.Random

main :: IO ()
main = checks
--main = verboseCheck prop_setDbValue

qc :: (Testable prop) => String -> prop -> IO()
qc label prop = do
    putStr label
    putStr ": "
    quickCheck prop

checks :: IO ()
checks = do
    qc "empty() always produces a tree" prop_emptyAlwaysProducesATree
    qc "a db either a tree or a leaf" prop_isTreeAndIsLeafAreOpposite  
    qc "different values have different hashes" prop_differentValueDifferentHash 
    qc "MDb.fromList is not impacted by list order" prop_fromListNotImpactedByOrder 
    qc "DbValue can be serialized and deserialized to binary" prop_binaryDbValueSerializeDeserialize
    qc "A value that is set can be looked up" prop_setDbValue
    qc "Setting a value at a leaf always fails" prop_setDbAtLeaf 

------------------------------------------------------------
-- GENERATORS
instance Arbitrary DbValue where
    arbitrary = oneof [
        liftM StringValue arbitrary,
        liftM IntValue arbitrary,
        liftM FloatValue arbitrary
        ] 

instance (Arbitrary s) => Arbitrary (KeyType s) where
    arbitrary = oneof [
        liftM Index arbitrary,
        liftM Key arbitrary
        ]

genleafdb :: (Arbitrary k, Arbitrary v, Binary v, Binary k, Ord k) => Gen (Db k v)
genleafdb = liftM dbLeaf arbitrary

gentreedb :: (Arbitrary k, Arbitrary v, Binary v, Binary k, Ord k) => Gen (Db k v)
gentreedb = liftM fromList arbitrary 

-- this is not great because no size
instance (Arbitrary k, Arbitrary v, Binary v, Ord k, Binary k) => Arbitrary (Db k v) where
    arbitrary = oneof [
        genleafdb,
        gentreedb
        ]

--------------------------------------------------------------------------
-- UTILITIES 
-- | randomly shuffle a list
shuffleList :: StdGen -> [a] -> [a]
shuffleList g l = snd <$> (sortOn fst $ zip ((randoms g) :: [Int]) l)

--------------------------------------------------------------------------
-- PROPERTIES

-- | empty Db always created as a tree
prop_emptyAlwaysProducesATree :: Bool
prop_emptyAlwaysProducesATree = isTree (empty :: UiDb)

-- a db is either a Tree or a Leaf
prop_isTreeAndIsLeafAreOpposite :: Db DbKey DbValue -> Bool
prop_isTreeAndIsLeafAreOpposite  d = isTree d /= isLeaf d 

-- different db values have different hashes
prop_differentValueDifferentHash :: Gen Bool
prop_differentValueDifferentHash = do
    leaf1 <- genleafdb :: Gen UiDb    
    leaf2 <- genleafdb :: Gen UiDb    
    return $ if leaf1 == leaf2 then dbHash leaf1 == dbHash leaf2 else dbHash leaf1 /= dbHash leaf2

-- | Shuffled lists produces Dbs with the same hash
prop_fromListNotImpactedByOrder :: Int -> [(DbKey, DbValue)] -> Bool
prop_fromListNotImpactedByOrder seed l = do
    dbHash (fromList l2) == dbHash (fromList $ shuffleList g l2)  
    where
        g = mkStdGen seed
        l2 = M.toList $ M.fromList l

-- | Binary serialization of values
prop_binaryDbValueSerializeDeserialize :: DbValue -> Bool
prop_binaryDbValueSerializeDeserialize v =
    v == (decode . encode) v

-- | Binary serialization and deserialization of [(DbKey, DbHash)]
prop_binaryListSerializeDeserialize :: [(DbKey, UiDb)] -> Bool
prop_binaryListSerializeDeserialize l =
    l2 == (decode . encode) l2
    where
        l2 = (\(k, db) -> (k, dbHash db)) <$> l

-- | Set db key value
prop_setDbValue :: Gen Bool
prop_setDbValue = do
    tdb <- gentreedb 
    vdb <- genleafdb
    k <- (arbitrary :: Gen DbKey)
    let ndb = set k vdb tdb
    let v = MDb.lookup k (fromRight tdb ndb) :: Maybe UiDb
    return $ v == Just vdb

-- | Set adb key value on a leaf
prop_setDbAtLeaf :: Gen Bool
prop_setDbAtLeaf = do
    tdb <- genleafdb :: Gen UiDb
    vdb <- genleafdb :: Gen UiDb
    k <- (arbitrary :: Gen DbKey)
    let ndb = set k vdb tdb
    return $ isLeft ndb
