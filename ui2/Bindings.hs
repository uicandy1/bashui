{-|
Module      : Bindings
Description : Binding mechanism
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Binding between data and ui elements (how ui elements
are update when their data change)
-}

module Bindings (
    Binding (..),
    adaptBinding,
    adaptIOBinding,
    buttonStringBinding,
    containerListBinding,
    drawingAreaCursorBinding,
    drawingAreaRenderBinding,
    entryStringBinding,
    groupedBinding,
    imagePixbufBinding,
    ioBinding,
    labelStringBinding,
    listStoreBinding,
    mappedBinding,
    menuItemLabelBinding,
    nullBinding,
    objectOnBinding,
    onceBinding,
    rangeMinMaxBinding,
    rangeValueBinding,
    spinButtonIncrementsBinding,
    spinButtonMinMaxBinding,
    spinButtonOnValueSpinnedBinding,
    switchActiveBinding,
    textViewStringBinding,
    toggleButtonActiveBinding,
    toolButtonClickedBinding,
    treeViewSingleSelectionBinding,
    valueChangeBinding,
    widgetSensitiveBinding
) where
--import Graphics.UI.Gtk hiding (Action, backspace)
--import Graphics.Rendering.Cairo
import Control.Monad
import Data.Maybe
import Data.Map.Strict as Map hiding (splitAt)
import MDb
import Data.GI.Base
import GI.GdkPixbuf.Objects.Pixbuf
import qualified GI.Gtk as Gtk
import GI.Gtk.Objects.Button
import GI.Gtk.Objects.DrawingArea
import GI.Gtk.Objects.Entry
import GI.Gtk.Objects.Image
import GI.Gtk.Objects.Label
import GI.Gtk.Objects.ListStore
import GI.Gtk.Objects.MenuItem
import GI.Gtk.Objects.Range
import GI.Gtk.Objects.SpinButton
import GI.Gtk.Objects.Switch
import GI.Gtk.Objects.TextView
import GI.Gtk.Objects.ToggleButton
import GI.Gtk.Objects.TreeView
import GI.Gtk.Objects.Widget
import GI.Gtk.Structs.TreePath
import Data.GI.Base.Signals
-- | Deinition of a binding (essentially: a function that
-- when given new data, regenerates the binding)
newtype Binding a = Binding (a -> IO (Binding a))

-- | Creates a Binding b from a Binding a and an a -> b function
-- newtype Binding a = Binding (a -> IO (Binding a))
adaptBinding :: (b -> a) -> Binding a -> Binding b
adaptBinding f (Binding bf) =
    Binding $ \ x -> do
                    ba <- bf (f x)
                    return (adaptBinding f ba) 

--- | Adapts a binding
adaptIOBinding :: (b -> IO a) -> Binding a -> Binding b
adaptIOBinding f (Binding bf) =
    Binding $ \ x -> do
                    fx <- f x
                    ba <- bf fx
                    return (adaptIOBinding f ba) 

-- | Binds a string to button
buttonStringBinding :: String -> Button -> String -> IO (Binding String)
buttonStringBinding prevs b s = do
    when (s /= prevs) $ buttonSetLabel b s 
    return $ Binding (buttonStringBinding s b)

-- | Binds a list to a layout
{- containerListBinding :: (WidgetClass b, ContainerClass c) => [(b, Binding a)] -> (a -> IO (b, Binding a)) -> c -> [a] -> IO (Binding [a]) 
containerListBinding bindings createBoundWidget cnt list = do
    let (_, missing) = splitAt (length bindings) list
    let (common, tbr) = splitAt (length list) bindings
    -- create new bindings
    newBindings <- mapM createBoundWidget missing
    -- these need to be added to the container
    mapM_ (\ (b, _) -> containerAdd cnt b) newBindings
    mapM_ (\ (b, _) -> containerRemove cnt b) tbr
    widgetShowAll cnt
    -- we combine the kept with the new and run them on the list data
    allNew <- zipWithM (\(w, Binding b) v -> sequence (w, b v)) (common ++ newBindings) list
    -- pack the container & whatnot
    return $ Binding (containerListBinding allNew createBoundWidget cnt)
-}

-- | Binds a cursor to a drawing area
{--
drawingAreaCursorBinding :: DrawingArea -> Cursor -> IO (Binding Cursor)
drawingAreaCursorBinding darea cursor = do
    drawwin <- widgetGetWindow darea
    when (isJust drawwin) $ drawWindowSetCursor (fromJust drawwin) (Just cursor)
    return $ Binding (drawingAreaCursorBinding darea)
--}
-- | Binds renderable data to a DrawingArea
{--drawingAreaRenderBinding :: (Eq a) =>  Maybe a -> Maybe (ConnectId DrawingArea) -> (a -> Render ()) -> DrawingArea -> a -> IO (Binding a)
drawingAreaRenderBinding prevVisual prevId renderFcn darea visual
    | isNothing prevVisual || (visual /= fromJust prevVisual) = newBinding
    | otherwise = oldBinding
    where
        newBinding = do
            forM_ prevId signalDisconnect
            newId <- darea `on` draw $ renderFcn visual
            widgetQueueDraw darea
            return $ Binding (drawingAreaRenderBinding (Just visual) (Just newId) renderFcn darea)
        oldBinding = return $ Binding (drawingAreaRenderBinding prevVisual prevId renderFcn darea)
--}
-- | Binds a string to an entry
entryStringBinding :: Entry -> String -> IO (Binding String)
entryStringBinding l s = do
    cur <- entryGetText l
    when (s /= cur) $ entrySetText l s 
    return $ Binding (entryStringBinding l)

-- | Create a binding for a list of bindings
groupedBinding :: [Binding a] -> a -> IO (Binding a)
groupedBinding bindings v = do
    newBindings <- mapM (\(Binding b) -> b v) bindings
    return $ Binding (groupedBinding newBindings)

-- | Binds an Image to a Pixbuf
imagePixbufBinding :: Image -> Pixbuf -> IO (Binding Pixbuf)
imagePixbufBinding i p = do
    imageSetFromPixbuf i p
    return $ Binding (imagePixbufBinding i)
 
-- | Binds and io operation
ioBinding :: (a -> IO b) -> a -> IO (Binding a)
ioBinding action payload = do
    action payload
    return $ Binding (ioBinding action)

-- | Binds a string to a label
labelStringBinding :: String -> Label -> String -> IO (Binding String)
labelStringBinding prevs l s = do
    when (s /= prevs) $ labelSetText l s 
    return $ Binding (labelStringBinding s l)

-- | Binds a ListStore to a db
{-listStoreBinding :: (Ord k, Eq v) => ListStore (Db k v) -> Db k v -> Db k v -> IO (Binding (Db k v))
listStoreBinding ls dbold@(Tree nold mold) dbnew@(Tree nnew mnew)
    | nold == nnew = return $ Binding (listStoreBinding ls dbold)
    | otherwise = result
    where
        result = do
            let listOld = dbAsDbList dbold
            let listNew = dbAsDbList dbnew
            let (_, missing) = splitAt (length listOld) listNew
            let (common, tbr) = splitAt (length listNew) listOld
            -- REMARK: we assume that dbold matches ListStore in length (we should crash if this not the case)
            -- create new bindings
            -- REMOVE STUFF THAT SHOULD NO LONGER BE THERE
            let idxToRemove = reverse [length listNew .. length listOld - 1]
            mapM_ (listStoreRemove ls) idxToRemove
            -- ADD NEW STUFF
            mapM_ (listStoreAppend ls) missing
            -- UPDATE EXISTING STUFF WHEN CHANGED
            -- use when or something?
            --mapM_ (\(db1, db2, pos) -> when (db1 /= db2) $ listStoreSetValue ls pos (tols db2)) $ zip3 common listNew [0..length common - 1]
            mapM_ (\(db1, db2, pos) -> when (db1 /= db2) $ listStoreSetValue ls pos db2) $ zip3 common listNew [0..length common - 1]
            return $ Binding (listStoreBinding ls dbnew)
-}
-- | Create a binding for a map of binding
mappedBinding :: (Ord k, Show k, Show a) => Map k (Binding a) -> (a -> a -> [k]) -> a -> a -> IO (Binding a)
mappedBinding bindings getkeys prev v = do
    let modified = getkeys prev v
    newBindings <- update modified v bindings
    return $ Binding (mappedBinding newBindings getkeys v)
    where
        update :: (Ord k) => [k] -> a -> Map k (Binding a) -> IO (Map k (Binding a))
        update [] _ m = return m
        update (k:r) v m = 
            fromMaybe newm newmf
            where
                newm = update r v m
                -- maybe monad
                newmf = do
                    Binding bnd <- Map.lookup k m
                    -- io monad
                    return $ do
                        nm <- newm
                        newbnd <- bnd v -- this is an io binding
                        return $ insert k newbnd nm


-- | Binds a string to a menu item
menuItemLabelBinding :: MenuItem -> String -> IO (Binding String)
menuItemLabelBinding  l s = do
    cur <- menuItemGetLabel l
    when (s /= cur) $ menuItemSetLabel l s 
    return $ Binding (menuItemLabelBinding l)
   
-- | does nothing
nullBinding :: a -> IO (Binding a)
nullBinding _ = 
    return $ Binding nullBinding

-- | General purpose "on" binding
{-objectOnBinding :: (Eq a, GObject o, SignalInfo info) => 
                Maybe a -> Maybe (SignalHandlerId) -> o -> info -> (a -> c) -> a -> IO (Binding a)
objectOnBinding prevValue prevId object signal function value
    | isNothing prevValue || (value /= fromJust prevValue) = newBinding
    | otherwise = oldBinding
    where
        newBinding = do
            forM_ prevId (disconnectSignalHandler o)
            newId <- object `on` signal $ function value
            return $ Binding (objectOnBinding (Just value) (Just newId) object signal function)
        oldBinding = return $ Binding (objectOnBinding prevValue prevId object signal function)
-}

-- | Binds once a given binding (the binding will run once then it will be passive)
-- this can be used for constant values (literals)
onceBinding :: Maybe (Binding a) -> a -> IO (Binding a)
onceBinding (Just (Binding b)) a = do
    b a
    return $ Binding (onceBinding Nothing)
onceBinding Nothing a = return $ Binding (onceBinding Nothing)

-- | Binds a (min, max) to a range
rangeMinMaxBinding :: (Double, Double) -> Range -> (Double, Double) -> IO (Binding (Double, Double))
rangeMinMaxBinding (pmin, pmax) r (nmin, nmax) = do
    when (nmin /= pmin || nmax /= pmax) $ rangeSetRange r nmin nmax 
    return $ Binding (rangeMinMaxBinding (nmin, nmax) r)

-- | Binds a double to a range value
rangeValueBinding :: Double -> Range -> Double -> IO (Binding Double)
rangeValueBinding prev r new = do
    when (new /= prev) $ rangeSetValue r new
    return $ Binding (rangeValueBinding new r)

-- | Binds increments to a spinbutton
spinButtonIncrementsBinding :: (Double, Double) -> SpinButton -> (Double, Double) -> IO (Binding (Double, Double))
spinButtonIncrementsBinding (pmin, pmax) sb (nmin, nmax) = do
    when (nmin /= pmin || nmax /= pmax) $ spinButtonSetIncrements sb nmin nmax 
    return $ Binding (spinButtonIncrementsBinding (nmin, nmax) sb)

-- | Binds a (min, max) to a range
spinButtonMinMaxBinding :: (Double, Double) -> SpinButton -> (Double, Double) -> IO (Binding (Double, Double))
spinButtonMinMaxBinding (pmin, pmax) sb (nmin, nmax) = do
    when (nmin /= pmin || nmax /= pmax) $ spinButtonSetRange sb nmin nmax 
    return $ Binding (spinButtonMinMaxBinding (nmin, nmax) sb)

-- | SpinButton spinned binding (this is not the new signals but the legacy thing)
{-spinButtonOnValueSpinnedBinding :: (SpinButtonClass o) => 
                Maybe (IO ()) -> Maybe (ConnectId o) -> o -> IO () -> IO (Binding (IO ()))
spinButtonOnValueSpinnedBinding prevAction prevId object action = do
    forM_ prevId signalDisconnect
    newId <- onValueSpinned object action
    return $ Binding (spinButtonOnValueSpinnedBinding (Just action) (Just newId) object)
-}
-- | Binds the actives state of a switch to a Bool
switchActiveBinding :: Switch -> Bool -> IO (Binding Bool)
switchActiveBinding switch s = do
    switchSetActive switch s
    return $ Binding (switchActiveBinding switch)

-- | Binds the content of a text view to a string
textViewStringBinding :: String -> TextView -> String -> IO (Binding String)
textViewStringBinding prevs v s = do
    when (s /= prevs) $ do
        buffer <- textViewGetBuffer v
        textBufferSetText buffer s $ length s
    return $ Binding (textViewStringBinding s v)

-- | Bind the active state of a toggle button to a bool
toggleButtonActiveBinding :: ToggleButton -> Bool -> IO (Binding Bool)
toggleButtonActiveBinding  tb s = do
    toggleButtonSetActive tb s
    return $ Binding (toggleButtonActiveBinding tb)

-- | Tool button clicked binding (this is not the new signals but the legacy thing)
{-toolButtonClickedBinding :: (ToolButtonClass o) => 
                Maybe (IO ()) -> Maybe (ConnectId o) -> o -> IO () -> IO (Binding (IO ()))
toolButtonClickedBinding prevAction prevId object action = do
    forM_ prevId signalDisconnect
    newId <- onToolButtonClicked object action
    return $ Binding (toolButtonClickedBinding  (Just action) (Just newId) object)
-}
-- | Binds a treepath to the selection of a treeview
treeViewSingleSelectionBinding :: TreeView -> TreePath -> IO (Binding TreePath)
treeViewSingleSelectionBinding tv s = do
    selection <- treeViewGetSelection tv
    maybeModel <- treeViewGetModel tv
    -- FIXME: we should probably not change the selection if if is identical
    treeSelectionSelectPath selection s
    return $ Binding (treeViewSingleSelectionBinding tv)

-- | Binds an IO operation to a value change
-- when the value changes the IO operation is performed
valueChangeBinding :: (Eq a) => IO () -> a -> a -> IO (Binding a)
valueChangeBinding f prev new 
    | prev == new = return $ Binding (valueChangeBinding f prev)
    | otherwise = do
        f
        return $ Binding (valueChangeBinding f new)

-- | Binds a boolean to the sensitive flag of a widget
widgetSensitiveBinding :: Widget -> Bool -> IO (Binding Bool)
widgetSensitiveBinding w s = do
    widgetSetSensitive w s
    return $ Binding (widgetSensitiveBinding w)

