{-|
Module      : Lib
Description : Run the main loop of the GTK ui application
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Run the main loop of the GTK ui application
-}
{-# LANGUAGE OverloadedStrings #-}
module Lib
    ( 
    runGui,
    loadSettings,
    Types.Settings(..)
    ) where
--import Bindings
--import Build
--import Control.Concurrent(forkIO)
--import Control.Monad(join)
--import Data.IORef(newIORef, readIORef, writeIORef, IORef)
--import Server.DbIO(server)
--import System.Log.Logger
import Text.Read(readMaybe)

import Data.Maybe(fromJust)
import qualified Data.Text as T (unpack)
import Data.GI.Base.ManagedPtr (unsafeCastTo)
import Data.GI.Base.GError (gerrorMessage, GError(..))
import Control.Exception (catch)
import qualified GI.Gtk as Gtk (main, init)
import GI.Gtk
       (widgetShowAll, mainQuit, onWidgetDestroy, onButtonClicked, Button(..),
        Window(..), builderGetObject, builderNewFromFile, builderNew)

import Types

-- | Read settings from a file
loadSettings :: FilePath -> IO (Maybe Types.Settings)
loadSettings path = do
    text <- readFile path
    return $ readMaybe text

-- | Runs the gui based on some settings
runGui :: Types.Settings -> IO ()
runGui someSettings = do
    --infoM logContext "initGUI"
    Gtk.init Nothing
    builder <- builderNewFromFile "ui2.glade"
    window <- builderGetObject builder "ID_MAIN" >>= unsafeCastTo Window . fromJust
    onWidgetDestroy window mainQuit
    widgetShowAll window
    Gtk.main

{-    infoM logContext "builder"
    builder <- builderNew
    allowEvents <- newIORef True
    fireS <- newIORef (putStrLn "") 
    let bo = BuildOptions {
        allowEvents = allowEvents,
        fireStartup = fireS
    }
    Binding binding <- build bo builder (root someSettings)
    infoM logContext "binding"
    nb <- binding (db someSettings)

    infoM logContext "launch server"
    ioRef <- newIORef (db someSettings, nb)
    threadId <- forkIO (server (socketPath someSettings) (db someSettings) (process ioRef allowEvents))

    join $ readIORef fireS

    infoM logContext "run mainGUI"
    Gtk.main
    infoM logContext "exit"

    where
        logContext = "runGui"
        process :: IORef (UiDb, Bindings.Binding UiDb) -> IORef Bool -> UiDb -> UiDb -> IO ()
        process ioRef allowEvents _ db = do
            idleAdd (updateBindings ioRef allowEvents db) priorityDefaultIdle
            return ()

        updateBindings :: IORef (UiDb, Bindings.Binding UiDb) -> IORef Bool -> UiDb -> IO Bool
        updateBindings ioRef allowEvents ndb = do
            (db, Binding bindings) <- readIORef ioRef
            -- we don't want events to be triggered when WE change things
            -- so we disable events
            writeIORef allowEvents False
            newBindings <- bindings ndb
            -- now, the update is complete so we re enable them
            writeIORef allowEvents True
            writeIORef ioRef (ndb, newBindings)
            return False
 -}        

