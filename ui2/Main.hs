{-|
Module      : Main
Description : The main ui app (ui server)
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The main ui application runs the user interface
and waits for queries from external processes
(to update the view model)
-}
module Main where

import Lib
import Options.Applicative
import System.Exit(ExitCode(..), exitWith)
import Server.Logger
import System.Log.Logger

-- | Exit code
invalidSettings = ExitFailure 1

-- | App settings
data AppSettings = AppSettings
  { 
    logLevel :: Priority,
    settingsPath :: String
  }
  deriving Show

-- | Generate an option parser that returns AppSettings
optParser :: Parser AppSettings
optParser = AppSettings
    <$> option auto ( 
        ( long "loglevel"
        <> short 'l'
        <> value WARNING
        <> help "Log level (DEBUG, INFO, NOTICE, WARNING, ERROR, CRITICAL, ALERT, EMERGENCY" ))
    <*> argument str (metavar "SETTINGSFILE")       

-- | Parse the options
opts :: ParserInfo AppSettings
opts = info (optParser <**> helper)
  ( fullDesc
  <> progDesc "uiCandy"
  <> header "uiCandy - user interface server" )

-- | Run the ui server
main :: IO ()
main = do
    options <- execParser opts
    setupLogger "candy" $ logLevel options
    infoM logContext $ "options = " ++ show options
    maybeSettings <- loadSettings $ settingsPath options
    maybe 
        (do
            errorM logContext $ "could not parse settings file " ++ settingsPath options
            exitWith invalidSettings
            )
        runGui
        maybeSettings
    where
        logContext = "main"

