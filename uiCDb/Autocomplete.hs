{-|
Module      : Autocomplete
Description : Autocomlete for the candy repl
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Autocomplete for the interactive candy tool
-}
module Autocomplete
    (
    autocomplete
    ) where

import Data.List(find)
import Data.Maybe(isNothing)
import Help(commandNames)
import System.Console.Haskeline(Completion(..))

-- | Last reversed word
lastReversedWord :: String -> String
lastReversedWord s = reverse $ head $ words s

-- | checks if one of the two strings is the prefix of the other
isPrefix :: String -> String -> Bool
isPrefix s1 s2 =
    isNothing $ find (uncurry (/=)) $ zip s1 s2

-- | suggestions with an empty stem (all possible suggestions)
suggestions :: String -> [Completion]
suggestions stem =
    comp <$> commandNames
    where
        comp a = Completion a a True

-- | filter a list of words by a prefix
startingBy :: [String] -> String -> [String]
startingBy words stem =
    drop (length stem) <$> filter (isPrefix stem) words

-- | returns suggestions starting with the provided prefix
suggestionsStartingWith :: String -> [Completion]
suggestionsStartingWith stem = 
    comp <$> startingBy commandNames stem
    where
        comp a = Completion a a True

-- | Autocomplete a string with some possible completions
autocomplete :: (String, String) -> (String, [Completion])  
autocomplete ("", after) =
    ("", suggestions "")
autocomplete (before, after) =
    case head before of
        ' ' -> (before, suggestions $ reverse before)
        _ -> (before, suggestionsStartingWith $ lastReversedWord before)

