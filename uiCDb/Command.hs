{-|
Module      : Command
Description : Command mode
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

The command mode of the candy application (there
are 3 modes: repl, command, and forwarder)
-}

module Command (
    command
    ) where
import Control.Monad.IO.Class(liftIO, MonadIO(..))
import Control.Monad.Trans.Class(lift)
import Control.Monad.Trans.Except (ExceptT(..), runExceptT, except, throwE, catchE)
import Data.ByteString.Char8(unpack)
import Data.ByteString.Base64(encode)
import Data.Either.Combinators (mapLeft)
import DbFormatting
import DbParsing
import MDb(dbHash)
import Network.Socket hiding (send, recv)
import Server.QueryBlock
import Server.Types
import System.Log.Logger
import Transaction.Transaction
import Types


-- | Foramt a DbResult as a String for display
showResult :: Formatting -> DbResult -> String
showResult PlainText ResultOK =
    "OK"
showResult PlainText (ResultError e) =
    show e
showResult PlainText (ResultDb Nothing) =
    ""
showResult PlainText (ResultDb (Just db)) =
    dbToPlainText db
showResult PlainText (ResultCompute values) =
    show values
showResult PlainText (ResultJournal entries) = 
    unlines (format <$> entries)
    where
        format :: JournalEntry DbKey DbValue -> String
        format (JournalEntry db s) = unpack (encode (dbHash db)) ++ " " ++ s

showResult Haskell res = show res 

showResult Json (ResultDb (Just db)) = dbToJson db
showResult Json _ = "???" 

-- | Receive a transaction result with a command error
receiveTransactionResult :: Socket -> ExceptT CommandError IO TransactionResult
receiveTransactionResult socket =  do
    res <- liftIO $ recvQb socket :: ExceptT CommandError IO (Either QueryBlockError TransactionResult) 
    except $ mapLeft (const ProtocolError) res

-- | Execute an io operation as a transaction (sending the request and obtaining the result)
runTransaction :: String -> Socket -> ExceptT CommandError IO () -> ExceptT CommandError IO TransactionResult
runTransaction comment socket operation = do
    liftIO $ infoM logContext $ "send transaction request, comment = " ++ comment
    liftIO $ sendQb socket $ TransactionRequest comment Nothing
    liftIO $ infoM logContext "run operations"
    operation
    liftIO $ infoM logContext "receive transaction result"
    receiveTransactionResult socket
    where
        logContext = "runTransaction"

-- | Equivalent of the tryFinally construct (the finally block is always executed)
tryFinally :: (Monad m) => ExceptT e m a -> m b -> ExceptT e m a
tryFinally code finally = do
    ret <- catchE code 
        (\e -> do 
            lift finally
            throwE e)
    lift finally
    return ret

-- | Handle whatever transaction result
handleTransactionResult :: ExceptT CommandError IO TransactionResult -> IO ()
handleTransactionResult res =
    runExceptT res >>= either 
        exitError
        (\res -> case res of
            -- here we could definitely display the hash
            Error _ -> (do
                infoM "transactionResult" "transaction error"
                exitError TransactionError)
            _ -> (do
                infoM "transactionResult" "transaction successful"
                return ())
            )

-- | toExcept
toExcept :: IO (Either QueryBlockError a) -> ExceptT CommandError IO a
toExcept operation = do
    res <- liftIO operation
    except $ mapLeft (const ProtocolError) res

-- | Non interactive command handling
processCommands :: String -> Socket -> ExceptT CommandError IO ()
processCommands buffer socket = do
    liftIO $ infoM logContext ("processCommands buffer = " ++ buffer)
    let maybeQueries = parseManyCommands buffer
    liftIO $ infoM logContext "commands parsed"
    tryFinally 
        (case maybeQueries of
            Left e -> do 
                liftIO $ errorM logContext ("parsing error " ++ show e)
                throwE ParsingError
            Right queries -> do
                mapM_ (\cmd -> 
                    case cmd of 
                        Query fmt q -> do
                            liftIO $ infoM logContext (show q)
                            liftIO $ sendQb socket q 
                            er <- toExcept (recvQb socket) :: ExceptT CommandError IO DbResult
                            (liftIO . putStrLn . showResult fmt) er
                        _ -> do
                            liftIO $ errorM logContext "repl commands not supported"
                            throwE ParsingError
                    ) queries 
                liftIO $ infoM logContext "Done processing requests, ShutdownSend")
            (shutdown socket ShutdownSend)
    where
        logContext = "command.processCommands"
    
-- | command client
-- this handles the case where commands are sent by shell scripts
-- (not the repl case)
command :: String -> Bool -> String -> String -> IO ()
command address transacted comment firstline = withSocketsDo $ do
    infoM logContext "starting command (non interactive mode)"
    sock <- socket AF_UNIX Stream 0 
    connect sock $ SockAddrUnix address
    infoM logContext "connected, readding input from stdin"
    fullBuffer <- getContents
    let msg = firstline ++ "\n" ++ fullBuffer
    infoM logContext "content read, processing commands"
    if transacted then do
        -- if it is in a transaction, we don't wrap it as a transaction
        eitherError <- runExceptT $ processCommands msg sock
        either exitError return eitherError 
    else handleTransactionResult $ runTransaction comment sock (processCommands msg sock)

    liftIO $ infoM logContext "closing"
    close sock
    where
        logContext = "command"
