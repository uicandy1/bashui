{-|
Module      : DbFormatting
Description : Formatting of db values
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Formatting of db values for display and shell consumption
-}
module DbFormatting
    (
    dbToPlainTextF,
    dbToPlainText,
    dbToJson 
    ) where

import MDb
import Server.KeyType(isIndex)
import Server.Types(DbValue(..), DbKey(..))
import Data.Map.Strict (toList)
import Data.List (intercalate)

-- | DbValue formatting for easy reading
dbVToPlainText :: DbValue -> String
dbVToPlainText (StringValue s) = s
dbVToPlainText (IntValue s) = show s
dbVToPlainText (FloatValue s) = show s

-- | Db formatting for easy reading
dbToPlainTextCell :: (String -> String) -> Db k DbValue -> String
dbToPlainTextCell fmt (Leaf n v) = fmt $ dbVToPlainText v
dbToPlainTextCell fmt (Tree n v) = fmt $ "nested"

-- | Db formatting for easy reading
dbToPlainTextRow ::  (String -> String) -> Db k DbValue -> String
dbToPlainTextRow fmt (Leaf n v) = fmt $ dbVToPlainText v
dbToPlainTextRow fmt (Tree n m) = 
    unwords $ (dbToPlainTextCell fmt . snd) <$> kv
    where
        kv = toList m

-- | Db formatting for easy reading
dbToPlainTextList :: (String -> String) -> Db k DbValue -> String
dbToPlainTextList fmt (Leaf n v) = fmt $ dbVToPlainText v
dbToPlainTextList fmt (Tree n m) =
    intercalate "\n" $ (dbToPlainTextRow fmt . snd) <$> kv
    where
        kv = toList m

-- | Format a db as a string that is human readable (i.e. unix ish)
dbToPlainTextF :: (String -> String) -> Db k DbValue -> String
dbToPlainTextF = dbToPlainTextList 

-- | Db formatting for easy reading
dbToPlainText :: Db k DbValue -> String
dbToPlainText = dbToPlainTextList id

-- | Db to json
dbToJson :: Db DbKey DbValue -> String
dbToJson (Leaf _ (StringValue str)) = show str
dbToJson (Leaf _ (IntValue i)) = show i
dbToJson (Leaf _ (FloatValue f)) = show f
dbToJson v =
    tji "" v
    where
        ind = "  "
        tji :: String -> Db DbKey DbValue -> String
        tji _ v@(Leaf _ _) = dbToJson v
        tji indent (Tree _ m) 
            | length kvs == 0 || isIndex (fst $ head  kvs) =
                "[" ++ "\n" ++
                (intercalate ",\n" $ map ((++) deepIndent . tji deepIndent . snd) kvs) ++
                "\n" ++ indent ++  "]"
            | otherwise = 
                "{" ++ "\n" ++
                (intercalate ",\n" $ map (kvpair deepIndent) kvs) ++
                "\n" ++ indent ++ "}"
            where
                kvs = toList m
                deepIndent = indent ++ ind
        kvpair :: String -> (DbKey, Db DbKey DbValue) -> String
        kvpair indent (k, v) = indent ++ (show k) ++ ": " ++ (tji (indent ++ ind) v)

