{-|
Module      : DbParsing
Description : Parse commands for the candy tool
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

Parse commands for the candy tool (commands for interacting
with the servers)
-}
module DbParsing
    (
    parseManyCommands,
    parseFirstCommand,
    ParseError,
    errorMessages,
    Message(..),
    messageString
    ) where

import Data.ByteString.Base64(decode)
import Data.ByteString.Char8(pack)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Token
import Text.ParserCombinators.Parsec.Language
import Text.Parsec.Error
import Text.Read (readMaybe)
import Types
import Server.Types
import MDb (fromList, fromDbList)
import Data.String (unlines)
import qualified Text.Parsec.Token as P
import Control.Arrow (first)

tp = makeTokenParser javaStyle
integer = P.integer tp

-- | Parsing of an index key (numeral key for lists)
indexKeyString :: GenParser Char st String 
indexKeyString = do
    first <- char '#' 
    other <- many digit
    return (first: other)

-- | Parsing of a (positive) int number
intNumber :: GenParser Char st Int
intNumber = do
    num <- many1 digit
    readOrFail "Could not parse int" num 

-- | Parsing of a string for a string key
stringKeyString :: GenParser Char st String
stringKeyString = do
    first <- letter
    other <- many alphaNum <|> string "_"
    return (first: other)

-- | Parsing of an index key that can fail
indexKey :: GenParser Char st DbKey
indexKey = indexKeyString >>= readOrFail "Invalid key"

-- | Parsing of a string key
stringKey :: GenParser Char st DbKey
stringKey = fmap Key stringKeyString

-- | Parsing of any key string or numeral
key :: GenParser Char st DbKey
key = indexKey <|> stringKey

-- | Parsing of a non quoted string
unquotedString :: GenParser Char st String
unquotedString = many1 $ noneOf " \n\t\r\""

-- | Escape
escapedChar :: GenParser Char st Char
escapedChar = do
    char '\\'
    anyChar

-- | Quoted string
quotedString :: GenParser Char st String
quotedString = do
    char '"'
    other <- many $ choice [
        try $ noneOf ['"', '\\' ], 
        try escapedChar]
    char '"'
    return other

-- | Parsing of any string quoted or not
anyString :: GenParser Char st String
anyString = 
    quotedString
    <|> unquotedString

-- | Parsing of everything until a newline
line :: GenParser Char st String
line = do
    l <- many $ noneOf ['\n']
    newline
    return l

-- | root path
rootpath :: GenParser Char st [DbKey]
rootpath = do
    try $ string "."  
    return []

-- | Parsing of a key path (deeply nested key)
-- . or aa.bb.
path :: GenParser Char st [DbKey]
path = choice [
    rootpath,
    sepBy key (char '.')]

-- | Parsing of the help command
helpCommand :: GenParser Char st ReplCommand
helpCommand = do
    try $ string "help"
    newline
    return Help
    
-- | Parsing of the quit command
quitCommand :: GenParser Char st ReplCommand
quitCommand = do
    try $ string "quit"
    newline
    return Quit

-- | Parsing of the commit command
commitCommand :: GenParser Char st ReplCommand
commitCommand = do
    try $ string "commit"
    newline
    return $ Query PlainText OOTCommit

-- | Parsing of the rollback command
rollbackCommand :: GenParser Char st ReplCommand
rollbackCommand = do
    try $ string "rollback"
    newline
    return $ Query PlainText OOTRollback

-- | Parsing of the transact command
transactCommand :: GenParser Char st ReplCommand
transactCommand = do
    try $ string "transact"
    skipMany1 space
    comment <- line
    return $ TransactionAt Tip comment

-- | Parsing of the at command
atCommand :: GenParser Char st ReplCommand
atCommand = do
    try $ string "at"
    skipMany1 space
    sha <- anyString
    skipMany1 space
    comment <- line
    return $ TransactionAt (Branch sha) comment

-- | Parsing of the get command
getStringCommand :: GenParser Char st ReplCommand
getStringCommand = do
    try $ string "get"
    skipMany1 space
    p <- path
    newline
    return $ Query PlainText $ Get p

-- | Parsing oof the get command: haskell Read formatting
getHaskellCommand :: GenParser Char st ReplCommand
getHaskellCommand = do
    try $ string "geth"
    skipMany1 space
    p <- path
    newline
    return $ Query Haskell $ Get p

-- | Parsing oof the get command: json formatting
getJsonCommand :: GenParser Char st ReplCommand
getJsonCommand = do
    try $ string "getj"
    skipMany1 space
    p <- path
    newline
    return $ Query Json $ Get p

-- | Parsing of the string command
setStringCommand :: GenParser Char st ReplCommand
setStringCommand = do
    try $ string "string"
    skipMany1 space
    p <- path
    skipMany $ char ' ' -- space eats the newline
    s <- many $ noneOf ['\n']
    newline
    return $ Query PlainText $ Set p (StringValue s)

-- | Parsing of the int command
setIntCommand :: GenParser Char st ReplCommand
setIntCommand = do
    try $ string "int"
    skipMany1 space
    p <- path
    skipMany $ char ' ' -- space eats the newline
    i <- intNumber
    newline
    return $ Query PlainText $ Set p (IntValue i)

-- | Column definition
data ColumnDef = StringCol String | IntCol String | FloatCol String

-- | Conversion of string to various column definitions
stringToDbValue :: String -> ColumnDef -> (DbKey, DbValue)
stringToDbValue s (StringCol k) = (Key k, StringValue s)
stringToDbValue s (IntCol k) = (Key k, IntValue (read s))
stringToDbValue s (FloatCol k) = (Key k, FloatValue (read s))

-- | Conversion of columns to a db, given some column definitions
columnsToDb :: [ColumnDef] -> [String] -> UiDb
columnsToDb cols s =
    fromList kv
    where
        kv = zipWith stringToDbValue s cols  

-- | Transformation of a list of db to a numerally indexed db
fromDbList :: [UiDb] -> UiDb
fromDbList lines =
    MDb.fromDbList kvList
    where
        -- The arrow version of this:
        -- kvList = (\ (n, l) -> (Index n, l)) <$> zip [0..] lines
        -- is here and blows my mind: 
        kvList = first Index <$> zip [0..] lines

-- | Parse column definitions
columnDefinitions :: GenParser Char st [ColumnDef]
columnDefinitions = sepBy column (char ',')

-- | Convert string lines to a db
linesToDb :: [ColumnDef] -> [[String]] -> UiDb
linesToDb cols lines = 
    DbParsing.fromDbList lineDbs
    where
        lineDbs = columnsToDb cols <$> lines

-- | Parse a columndef
column :: GenParser Char st ColumnDef
column = do
    title <- stringKeyString
    option (StringCol title) $ choice [
        do 
            try $ string "%d"
            return (IntCol title),
        do
            try $ string "%f"
            return (FloatCol title)
        ]

-- | Parse column data
columnData :: Int -> GenParser Char st [String]
columnData ncol = do
    first <- count ncol $ do
        spaces
        anyString
    last <- many $ noneOf ['\n']
    newline
    return $ first ++ [last]

-- | parse the lines command
setLinesCommand :: GenParser Char st ReplCommand
setLinesCommand = do
    try $ string "lines"
    skipMany1 space
    p <- path
    spaces
    columns <- columnDefinitions
    spaces
    delimiter <- many $ noneOf ['\n']
    newline
    lines <- manyTill  (columnData (length columns)) (try $ string delimiter)
    newline
    return $ Query PlainText $ SetDb p (linesToDb columns lines)

-- | parse the appendlines command (for appending to a numerally indexed db)
appendLinesCommand :: GenParser Char st ReplCommand
appendLinesCommand = do
    try $ string "appendlines"
    skipMany1 space
    p <- path
    spaces
    columns <- columnDefinitions
    spaces
    delimiter <- many $ noneOf ['\n']
    newline
    lines <- manyTill  (columnData (length columns)) (try $ string delimiter)
    newline
    return $ Query PlainText $ AppendDb p $ columnsToDb columns <$> lines

-- | parse the prependlines command (for prepending to a numerally indexed db)
prependLinesCommand :: GenParser Char st ReplCommand
prependLinesCommand = do
    try $ string "prependlines"
    skipMany1 space
    p <- path
    spaces
    columns <- columnDefinitions
    spaces
    delimiter <- many $ noneOf ['\n']
    newline
    lines <- manyTill  (columnData (length columns)) (try $ string delimiter)
    newline
    return $ Query PlainText $ PrependDb p $ columnsToDb columns <$> lines

-- | Parse the delete line command (only works on numerally indexed dbs,
-- does not really makes sense in another context)
deleteLineCommand :: GenParser Char st ReplCommand
deleteLineCommand = do
    try $ string "del"
    skipMany1 space
    p <- path 
    newline
    return $ Query PlainText $ Delete p

-- | Parses the text command
setTextCommand :: GenParser Char st ReplCommand
setTextCommand = do
    try $ string "text"
    skipMany1 space
    p <- path
    skipMany1 space
    delimiter <- many $ noneOf ['\n']
    newline
    lines <- manyTill line (try $ string delimiter)
    newline
    return $ Query PlainText $ Set p (StringValue $ unlines lines)

-- | Tries to parse something with Read and fails the parsec
-- parser if this does not fly
readOrFail :: (Read t) => String -> String -> GenParser Char st t
readOrFail msg s =
    maybe (unexpected msg) return (readMaybe s)

-- | Parse a command to get the diff between to shas
getDiffCommand :: GenParser Char st ReplCommand
getDiffCommand = do
    try $ string "diff"
    skipMany space
    hashFrom <- anyString 
    skipMany space
    hashTo <- anyString 
    let eitherParsed = do
            ub64From <- decode $ pack hashFrom
            ub64To <- decode $ pack hashTo
            return $ Query PlainText $ OOTDiff ub64From ub64To
    either unexpected return eitherParsed
    
-- | sunc some haskell (show encoded) notificaiton objects
-- fixme: we should have 2 more things: the path of the patch and the current sha
-- IN FACT: the sha should be in the diff. So you know on top of what you should apply that
-- FIXME: add the SHA to the DIf
patchDiffsCommand :: GenParser Char st ReplCommand
patchDiffsCommand = do
    try $ string "patch"
    skipMany1 space
    p <- path
    skipMany space
    delimiter <- many $ noneOf ['\n']
    newline
    lines <- manyTill line (try $ string delimiter)
    newline
    --let diffs = read (concat lines) :: [ UiDbDiff ]
    diffs <- readOrFail "Could not parse diffs" (concat lines)

    return $ Query PlainText $ Patch p diffs 

-- | Parse the history command
getHistoryCommand :: GenParser Char st ReplCommand
getHistoryCommand = do
    try $ string "history"
    newline
    return $ Query PlainText OOTHistory

-- | Parse the undo command
getUndoCommand :: GenParser Char st ReplCommand
getUndoCommand = do
    try $ string "undo"
    newline
    return $ Query PlainText OOTUndo

-- | Parse the redo command
getRedoCommand :: GenParser Char st ReplCommand
getRedoCommand = do
    try $ string "redo"
    newline
    return $ Query PlainText OOTRedo

-- | Parse the count command (number of items in numerally indexed tree)
getCountCommand :: GenParser Char st ReplCommand
getCountCommand = do
    try $ string "count"
    skipMany1 space
    p <- path
    newline
    return $ Query PlainText $ Compute (Count p)

-- | Parse the select command
selectCommand :: GenParser Char st ReplCommand
selectCommand = do
    try $ string "select"
    skipMany1 space
    fields <- sepBy path (char ',')
    skipMany1 space
    string "from"
    skipMany1 space
    p <- path
    newline
    return $ Query PlainText $ Compute (Select fields p)

-- | Parse any command.
-- Top level parser for everything that is supported
anyCommand :: GenParser Char st ReplCommand
anyCommand = do
        skipMany newline
        helpCommand
            <|> quitCommand
            <|> setLinesCommand
            <|> appendLinesCommand 
            <|> prependLinesCommand 
            <|> setStringCommand
            <|> setIntCommand 
            <|> setTextCommand
            <|> deleteLineCommand 
            <|> patchDiffsCommand 
            <|> getDiffCommand 
            <|> getJsonCommand 
            <|> getHaskellCommand 
            <|> getStringCommand 
            <|> getHistoryCommand
            <|> getCountCommand
            <|> getRedoCommand 
            <|> getUndoCommand 
            <|> selectCommand
            <|> commitCommand
            <|> rollbackCommand
            <|> transactCommand 
            <|> atCommand 

-- | Parse many commands
manyCommands :: GenParser Char st [ReplCommand]
manyCommands = sepBy anyCommand $ many newline

-- | Parse the first available command
firstCommand :: GenParser Char st (ReplCommand, String)
firstCommand = do
    cmd <- anyCommand    
    s <- many anyChar
    return (cmd, s)

-- | Parse all available commands
parseManyCommands :: String -> Either ParseError [ReplCommand]
parseManyCommands input = parse manyCommands ("could not parse sequence " ++ show input) input

-- | Parse the first command
parseFirstCommand :: String -> Either ParseError (ReplCommand, String)
parseFirstCommand input = parse firstCommand ("could not parse command " ++ show input) input
