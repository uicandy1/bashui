{-|
Module      : Forwarder
Description : Command forwarder to another server
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}

module Forwarder (
    forwardConnection,
    forwarder
    ) where

import Control.Concurrent.MVar (newEmptyMVar)
import Control.Monad (when)
import Control.Monad.IO.Class(liftIO, MonadIO(..))
import Control.Monad.Trans.Except (ExceptT(..), runExceptT, catchE, throwE, except)
import Data.Either (isLeft, fromLeft)
import Network.Socket hiding (send, recv)
import Server.Directory
import System.Exit(exitWith)
import Server.QueryBlock
import Server.Types
import System.Log.Logger
import Types(transactionError)


-- | While loop returning the last value checked
while :: (Monad m) => m (Bool, a) -> m a
while fcn = do
    (b, r) <- fcn
    if b then while fcn else return r

-- | While loop with no result
while_ :: (Monad m) => m Bool -> m ()
while_ fcn = do
    b <- fcn
    when b $ while_ fcn

-- | forward a single connection . if it ends the session return false otherwise true
forwardConnection :: Socket -> Socket -> ExceptT QueryBlockError IO Bool
forwardConnection fromSocket toSocket = 
    while $ do
        liftIO $ infoM logContext "read request"
        -- here it is expected that the socket may have been shutdown
        maybeRequest <- catchE
            (fmap Just (liftIO (recvQb fromSocket) >>= except))
            (\e -> case e of
                ConnectionClosed -> return Nothing
                e -> throwE e)
        let typeRequest = maybeRequest :: Maybe DbQuery
        -- request <- ((liftIO $ recvQb fromSocket) >>= except)
        case maybeRequest of
            Just request -> do
                liftIO $ infoM logContext $ "received " ++ show request
                liftIO $ infoM logContext "send request"
                liftIO $ sendQb toSocket request
                liftIO $ infoM logContext "read response"
                response <- liftIO (recvQb toSocket) >>= except
                let typeResponse = response :: DbResult
                liftIO $ infoM logContext "write response"
                liftIO $ sendQb fromSocket response
                liftIO $ infoM logContext "interpret end"
                case request of
                    OOTCommit -> do
                        liftIO $ infoM logContext "at end (commit)"
                        return (False, False)
                    OOTRollback -> do
                        liftIO $ infoM logContext "at end (rollback)"
                        return (False, False)
                    _ -> do
                        liftIO $ infoM logContext "not at end"
                        return (True, True)
            Nothing -> return (False, True)
    where
        logContext = "forwardConnection"

-- | Forwarder: receives parsed commands and forwards them
--   (allows to maintain a lengthy connection to the db spawning
--   multiple individual requests)
forwarder :: String -> FilePath -> FilePath -> IO () 
forwarder comment toPath socketPath = withSocketsDo $ do
    -- connecct to the destination socket
    fwtoSocket <- socket AF_UNIX Stream 0 
    connect fwtoSocket $ SockAddrUnix toPath
    -- open the reception socket
    removeIfExists socketPath 
    sock <- socket AF_UNIX Stream 0 -- and try UDP?
    bind sock (SockAddrUnix socketPath)
    listen sock maxListenQueue 
    mvarConnected <- newEmptyMVar 

    -- send the tranaction request
    --trequest <- recvQb socket :: IO (Either QueryBlockError TransactionRequest)
    eitherThing <- runExceptT $ do
        liftIO $ sendQb fwtoSocket $ TransactionRequest comment Nothing
        while_ $ do 
            liftIO $ infoM logContext "wait for connection"
            (conn, _) <- liftIO $ accept sock
            liftIO $ infoM logContext "connection accepted"
            b <- forwardConnection conn fwtoSocket
            liftIO $ infoM logContext $ "connection processing complete, keep going: " ++ show b
            return b
        -- read the result of the transactionj
        liftIO $ infoM logContext "waiting for transaction result"
        trResponse <- liftIO (recvQb fwtoSocket) >>= except
        let typeTrResponse = trResponse :: TransactionResult
        liftIO $ infoM logContext $ "transaction result received " ++ show trResponse
        return ()
    when (isLeft eitherThing) $ do
            liftIO $ errorM logContext (show (fromLeft Server.QueryBlock.UnexpectedError eitherThing))
            exitWith transactionError

    return () 
    where
        logContext = "forwarder"
