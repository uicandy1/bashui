{-|
Module      : Help
Description : Help for repl commands
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
module Help
    (
    splashString,
    helpAll,
    commandNames
    ) where

import Data.List(intercalate, sortOn)
import Data.Tuple.HT(uncurry3, fst3, snd3)
import System.Console.ANSI
import Text.Printf

-- | Commands
commands :: [(String, [String], String)]
commands = sortOn fst3 [
        ("appendlines", ["path", "cols", "sep", "..."], "append the provided lines at path"),
        ("at", ["partialhash", "comment"], "start a transaction at a partialhash"),
        ("commit", [], "commit the current transaction"),
        ("count", ["path"], "count the children at path"),
        ("del", ["path"], "remove the selected key from the database at path"),
        ("get", ["path"], "return the value of a given key as a string"),
        ("geth", ["path"], "return the value of a given key as Haskell read format"),
        ("getj", ["path"], "return the value of a given key as json"),
        ("help", [], "show help information"),
        ("history", [], "display the change history of the db"),
        ("lines", ["path", "columns", "sep"], "set lines at path"),
        ("patch", ["path", "sep", "..."], "sync under path with the provided changes"),
        ("prependlines", ["path", "columns", "sep"], "add some lines before the existing line at path"),
        ("quit", [], "quit the application"),
        ("redo", [], "redo (the whole history) one step"),
        ("rollback", [], "rollback the current transaction"),
        ("select", ["fields", "from", "path"], "select fields from path"),  
        ("transact", ["comment"], "start a transaction at the tip"),  
        ("undo", [], "undo (the whole history) one step")
    ]

-- | Commands with second arguments as a string
textCommands :: [(String, String, String)]
textCommands = (\(a, b, c) -> (a, unwords b, c)) <$> commands

-- | Command names
commandNames :: [String]
commandNames = fst3 <$> commands

-- | A splash string
splashString :: String
splashString = setSGRCode [SetColor Foreground Dull Magenta] ++
    "uiCDb uiCandy Database Interface\n" ++
    "commands: " ++
    setSGRCode [SetColor Foreground Dull White] ++
    helpAllItems ++
    setSGRCode [Reset]

-- | Max length of a list of strings
maxLength :: [String] -> Int
maxLength strings = maximum (length <$> strings)

-- | Max length of all command names
maxKeyLength :: Int
maxKeyLength = maxLength commandNames

-- | Max length of formatted commands
maxArgsLength :: Int
maxArgsLength = maxLength $ snd3 <$> textCommands


-- | String showing a list of alla commands for which help is available
helpAllItems :: String
helpAllItems = intercalate ", " commandNames

-- | Show all help
helpAll :: String
helpAll = 
    setSGRCode [SetColor Foreground Dull White] ++
        unlines (uncurry3 (printf format)  <$> textCommands) ++
        setSGRCode [Reset]
    where
        format = "%-" ++ show maxKeyLength ++ "s %-" ++ show maxArgsLength ++ "s %s"

