{-|
Module      : Main
Description : Main loop for the candy program
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

THe main application can operate in 3 different modes:
forwarder: all commands are only forwarded to another process
command: normal mode
repl: interactive mode
-}
{-# LANGUAGE OverloadedStrings #-}
module Main where
import Command
import Control.Arrow (second)
import Data.List (elemIndex) -- , find)
import Data.Semigroup ((<>))
import Forwarder
import Help
import Options.Applicative
import Options.Applicative.Help.Pretty
import Repl
import Server.Logger
import System.Environment(getArgs)
import System.Log.Logger

-- | server settings
data ServerSettings = ServerSettings
  { 
    socketPath :: String,
    transactionSocket :: Maybe String,
    update   :: [String],
    logLevel :: Priority,
    transactionComment :: String,
    transacted :: Bool
  }
  deriving Show

-- | Separate what's before and after "--" in the args
partitionArgs :: [String] -> ([String], Maybe String)
partitionArgs args = 
    maybe (args, Nothing) (second Just) $ do
        idx <- elemIndex "--" args
        let (b, a) = splitAt (idx + 1) args
        return (b, unwords a)

-- | Generate an option parser
optParser :: Parser ServerSettings
optParser = ServerSettings
      <$> strOption ( 
        long "socketPath"
         <> short 's'
         <> metavar "SOCKET"
         <> help "Unix domain socket path to SOCKET" )
      <*> optional (strOption ( 
        long "transactionSocket"
         <> short 't'
         <> metavar "TSOCKET"
         <> help "Unix domain socket path for transactions to TSOCKET" ) )
      <*> many (strOption
          ( long "update"
         <> short 'n'
         <> help "Command to run on update" ))
      <*> option auto  
          ( long "loglevel"
         <> short 'l'
         <> value WARNING
         <> help "Log level (DEBUG, INFO, NOTICE, WARNING, ERROR, CRITICAL, ALERT, EMERGENCY" )
      <*> strOption (
        long "transactionComment"
         <> short 'c'
         <> metavar "COMMENT"
         <> value "Transaction"
         <> help "Transaction comment as COMMENT")
      <*> switch
          ( long "transacted"
         <> short 'x'
         <> help "transacted" )

-- | Parse the options
opts :: ParserInfo ServerSettings
opts = info (optParser <**> helper)
  ( fullDesc
  <> progDesc "execute queries on ui or state database in interactive (REPL) or non iteractive mode"
  <> header "candy - a connection to a ui or state database"
  <> (footerDoc . Just) (
    text "To connect interactively (REPL mode) to a ui server \
    \and interactively inspect the history and state of the ui:\n" </>
    indent 4 (text "candy -s /tmp/tmp.wlhbvCTDPr/ui.ipc") </>
    text "\n\n" </>
    text "To pipe a request to a ui server (non interactive request): \n" </>
    indent 4 (text "echo 'getj .' | candy -s /tmp/tmp.wlhbvCTDPr/ui.ipc --") </>
    text "\n\n" </>
    text "Supported Queries:\n" </>
    text helpAll))

-- | Run as a command processor
commandProcessor :: String -> Bool -> String -> Maybe String -> IO ()
commandProcessor address transacted comment = 
    maybe (repl address transacted comment) (Command.command address transacted comment) 

-- | Run as a transaction context (forwarding parsed commands to a db)
commandForwarder :: String -> String -> String -> IO ()
commandForwarder = forwarder

-- | Top level candy app
main :: IO ()
main = do
    args <- getArgs
    let (normalArgs, c) = partitionArgs args
    options <- handleParseResult $ execParserPure Options.Applicative.defaultPrefs opts normalArgs
    setupLogger "uiCDb" $ logLevel options
    infoM "main" $ "options = " ++ show options
    maybe
        (commandProcessor 
            (socketPath options) 
            (transacted options) 
            (transactionComment options) 
            c)
        (commandForwarder (transactionComment options) (socketPath options))
        $ transactionSocket options         
