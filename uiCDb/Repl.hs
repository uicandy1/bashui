{-|
Module      : Repl
Description : Repl mode for the candy app 
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}
module Repl
    (
    getCommand,
    transactionTalk,
    innertalk,
    repl
    ) where

import Autocomplete
import Control.Monad.IO.Class(liftIO, MonadIO(..))
import Data.ByteString.Base64(encode)
import Data.ByteString.Char8(unpack)
import Data.List.Split (splitOn)
import DbFormatting
import DbParsing
import Help(helpAll, splashString)
import MDb(dbHash)
import Network.Socket hiding (send, recv)
import Server.QueryBlock
import Server.Types
import System.Console.ANSI
import System.Console.Haskeline
import System.Log.Logger
import Text.Parsec(errorPos, sourceColumn, sourceLine)
import Transaction.Transaction
import Types

-- | Normal prompt
nPrompt :: String
nPrompt = setSGRCode [SetColor Foreground Vivid Green] ++
    "> " ++
    setSGRCode [Reset]

-- | Normal prompt
cPrompt :: String
cPrompt = setSGRCode [SetColor Foreground Dull Green] ++
    "... " ++
    setSGRCode [Reset]

-- | Transaction prompt
tnPrompt :: String -> String
tnPrompt _ = "* " ++ nPrompt

-- | Transaction continuation prompt
tcPrompt :: String -> String
tcPrompt _ = "* " ++ cPrompt

-- | Message to string
showMsg :: Message -> String
showMsg (UnExpect s) = "UnExpect " ++ s
showMsg (Expect s) = "Expect " ++ s
showMsg (Message s) = "Message " ++ s
showMsg (SysUnExpect s) = "SysUnExpect " ++ s

-- | Number of lines and columns in a multiline string
linesColumns :: String -> (Int, Int)
linesColumns s =
    (length l, length . last $ l)
    where
        l = splitOn "\n" s

-- | Determine if a command is continued based on the parsing error
-- (error because missing stuff??)
isContinued :: DbParsing.ParseError -> String -> Bool
isContinued e src =
    l == line && c == (col - 1)
    where
        pos = errorPos e
        col = sourceColumn pos
        line = sourceLine pos
        (l, c) = linesColumns src

-- Transform DbResult to String for display
showResult :: Formatting -> DbResult -> String
showResult PlainText ResultOK =
    "OK"

showResult PlainText (ResultError e) =
    setSGRCode [SetColor Foreground Vivid White, SetColor Background Vivid Red] ++
    show e ++
    setSGRCode [Reset]

showResult PlainText (ResultDb Nothing) =
    ""
showResult PlainText (ResultDb (Just db)) =
    dbToPlainTextF delimit db
showResult PlainText (ResultCompute values) =
    show values
showResult PlainText (ResultJournal entries) = 
    unlines (format <$> entries)
    where
        format :: JournalEntry DbKey DbValue -> String
        format (JournalEntry db s) = unpack (encode (dbHash db)) ++ " " ++ delimit s

showResult PlainText r = show r

showResult Haskell res = delimit (show res) 

showResult Json (ResultDb (Just db)) = delimit (dbToJson db)
showResult Json _ = "???" 

-- | make displayed results more readable (delimiting white space)
delimit :: String -> String
delimit s =
    setSGRCode [SetColor Foreground Vivid White, SetColor Background Vivid Black] ++
    s ++
    setSGRCode [Reset]

-- | Returns the next command
getCommand :: String -> String -> String -> InputT IO ReplCommand
getCommand normalPrompt continuationPrompt prefix = do
    let prompt = if length (words prefix) == 0 then normalPrompt else continuationPrompt
    maybeLine <- getInputLine prompt
    maybe 
        (do
            liftIO $ errorM logContext "getInputLine error" 
            getCommand normalPrompt continuationPrompt ""
        )
        (\line -> do
            liftIO $ infoM logContext ("line: " ++ line)
            let fullLine = prefix ++ line ++ "\n"
            let parsed = parseFirstCommand fullLine
            liftIO $ infoM logContext ("parsed: '" ++ show parsed ++ "'")

            case parsed of
                Left err -> do
                        liftIO $ debugM logContext (show (showMsg <$> errorMessages err))
                        if isContinued err fullLine
                            then 
                                getCommand normalPrompt continuationPrompt fullLine 
                            else do
                                liftIO $ errorM logContext ("parsing error '" ++ show err ++ "'")
                                getCommand normalPrompt continuationPrompt ""
                Right (q, remainder) -> 
                    -- FIXME: Here we should immediately deal with stuff like
                    -- help, quit etc... (>)
                            return q
        )
        maybeLine
    where
        logContext = "getCommnad"

-- | Process commands in a transaction
transactionTalk :: Socket -> Root -> String -> InputT IO ()
transactionTalk socket root comment = do
    cmd <- getCommand (tnPrompt comment) (tcPrompt comment) ""
    case cmd of 
        Help -> do
            liftIO $ putStrLn helpAll
            transactionTalk socket root comment
        Quit -> do
            liftIO $ errorM logContext "quit in transaction"
            transactionTalk socket root comment
        TransactionAt _ _ -> do
            liftIO $ errorM logContext "nested transaction"
            transactionTalk socket root comment
        Query fmt q -> do
            liftIO $ sendQb socket q
            er <- liftIO $ recvQb socket :: InputT IO (Either QueryBlockError DbResult)
            either
                (\err -> liftIO $ errorM logContext $ "error " ++ show err)
                (liftIO . putStrLn . showResult fmt)
                er
            case q of
                OOTCommit -> return ()
                OOTRollback -> return ()
                _ -> transactionTalk socket root comment
    where
        logContext = "transactionTalk"

-- | Processcommands as one command per transaction
innertalk :: String -> InputT IO ()
innertalk address = do
    cmd <- getCommand nPrompt cPrompt ""
    case cmd of 
        Help -> do 
            liftIO $ putStrLn helpAll
            innertalk address

        Quit ->
            liftIO $ infoM logContext "exiting"
        TransactionAt root comment -> do
            sock <- liftIO connectSock 
            liftIO $ sendQb sock $ TransactionRequest comment
                (case root of
                    Tip -> Nothing
                    Branch str -> Just str
                        )
            transactionTalk sock root comment
            tr <- liftIO $ recvQb sock :: InputT IO (Either QueryBlockError TransactionResult)
            liftIO $ close sock
            innertalk address

        Query fmt q -> do
            sock <- liftIO connectSock
            -- do the transaction
            liftIO $ infoM logContext (show q)
            liftIO $ infoM logContext "send transaction request"
            liftIO $ sendQb sock $ TransactionRequest (head . lines $ "REPL: " ++ show cmd) Nothing
            liftIO $ infoM logContext "send query"
            liftIO $ sendQb sock q
            liftIO $ infoM logContext "read result"
            er <- liftIO $ recvQb sock :: InputT IO (Either QueryBlockError DbResult)
            either
                (\err -> liftIO $ errorM logContext $ "error " ++ (show err))
                (liftIO . putStrLn . showResult fmt)
                er
            liftIO $ infoM logContext "oot end"
            liftIO $ sendQb sock OOTCommit
            liftIO $ infoM logContext "read transaction result"
            tr <- liftIO $ recvQb sock :: InputT IO (Either QueryBlockError TransactionResult)
            --
            liftIO $ close sock
            innertalk address
    where
        connectSock :: IO Socket
        connectSock = do
            sock <- socket AF_UNIX Stream 0 
            liftIO $ infoM logContext ("connecting to " ++ address)
            connect sock $ SockAddrUnix address
            liftIO $ infoM logContext "connected"
            return sock
        logContext = "innerTalk"

-- | Provide all possible complettions for a string
ac :: (String, String) -> IO (String, [Completion])  
ac input = return $ autocomplete input

-- | Autocomplete loop
talk :: String -> IO ()
talk address = runInputT settings (innertalk address)
    where
        settings = Settings {
            System.Console.Haskeline.complete = ac,
            historyFile = Nothing,
            autoAddHistory = True
            } 

-- | repl client
repl :: String -> Bool -> String -> IO ()
repl address transacted comment = do
    putStrLn splashString
    withSocketsDo $ do
        liftIO $ infoM logContext "starting repl (interactive mode)"
        talk address
        liftIO $ infoM logContext "exiting repl"
        return ()
        where
            logContext = "repl"
