{-|
Module      : Types 
Description : Repl types
Copyright   : (c) Hugo Windisch, 2018
License     : BSD3
Stability   : experimental

-}

module Types (
    Root(..),
    ReplCommand(..),
    transactionError,
    CommandError(..),
    parsingError,
    commandError,
    exitError,
    Formatting(..)
    ) where

import Server.Types
import System.Exit(exitWith, ExitCode(..))

-- | Root of a transaction (tip or specific partial hash)
data Root = Tip | Branch String
    deriving (Read, Show)

-- | Formatting for displaying query results
data Formatting = PlainText | Json | Haskell 
    deriving (Read, Show)

-- | Repl commands
data ReplCommand = 
    Help |
    Quit |
    TransactionAt Root String |
    Query Formatting DbQuery
    deriving (Read, Show)

-- | Error command error
data CommandError = 
    ProtocolError |
    TransactionError |
    ParsingError |
    ExecutionError

-- | Convert a CommandError to an IO operation (that sets the exitCode)
exitError :: CommandError -> IO ()
exitError ProtocolError = exitWith $ ExitFailure 1
exitError TransactionError = exitWith $ ExitFailure 2
exitError ParsingError = exitWith $ ExitFailure 3
exitError ExecutionError = exitWith $ ExitFailure 4

-- | Exit code for transaction error
transactionError = ExitFailure 2
-- | Exit code for command error (when out of transaction)
commandError = ExitFailure 3
-- | Exit code for command parsing error 
parsingError = ExitFailure 4

